/*******************************************************************************
 * Package: com.song.common.work
 * Type:    WorkUtil
 * Date:    2023-01-02 20:17
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.work;

import java.lang.reflect.Field;

/**
 * 功能描述：工作中好的代码写法
 *
 * @author Songxianyang
 * @date 2023-01-02 20:17
 */
public class UpdateTimeAutoUtil {

    /**
     * 如何调用：
     *      获取创建时间
     *         setFiledValue("createTime", obj, Calendar.getInstance().getTime());
     * @param filedName
     * @param obj
     * @param value
     */

    private void setFiledValue(String filedName, Object obj, Object value) {
        Class<?> objcetClass = obj.getClass();
        try {
            //用于返回一个Field对象，该对象指示给定的类的声明字段或由此Class对象表示的接口。（获取当前字段）
            Field field = objcetClass.getDeclaredField(filedName);

            //让我们在用反射时访问私有变量 （跳过权限）
            field.setAccessible(true);
            field.set(obj, value);
            field.setAccessible(false);
            // 如果是父类中的字段
        } catch (NoSuchFieldException e) {

            try {
                // 获取父类
                if (objcetClass.getSuperclass() != null) {
                    //获取父类中的 privact 的字段
                    Field field = objcetClass.getSuperclass().getDeclaredField(filedName);
                    field.setAccessible(true);
                    field.set(obj, value);
                    field.setAccessible(false);
                } else {
                    throw new RuntimeException("异常！");
                }


            } catch (Exception e1) {
                throw new RuntimeException("异常！");
            }


        } catch (Exception e) {
            throw new RuntimeException("异常！");
        }
    }
}

