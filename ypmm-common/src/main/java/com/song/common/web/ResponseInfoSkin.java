/*******************************************************************************
 * Package: com.song.common.web
 * Type:    ResponseInfoMapper
 * Date:    2022-10-19 21:50
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.common.web;

import java.lang.annotation.*;

/**
 * 功能描述： 请求的前置处理 注解
 *
 * @author Songxianyang
 * @date 2022-10-19 21:50
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseInfoSkin {
}
