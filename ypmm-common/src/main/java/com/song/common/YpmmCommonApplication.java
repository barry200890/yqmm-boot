package com.song.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YpmmCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(YpmmCommonApplication.class, args);
    }

}
