/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    RedisWeb
 * Date:    2022-07-29 23:16
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import io.swagger.annotations.Api;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-07-29 23:16
 */
@Api(tags = "测试Redisson")
@RestController
public class RedisWeb {
    @Resource
    private RedissonClient redissonClient;
    
    /**
     * 可重入锁
     * @return
     */
    @GetMapping("lock-kcr")
    public String testLock() throws InterruptedException {
        RLock lock = redissonClient.getLock("test-Lock");
        lock.lock(11,TimeUnit.SECONDS);
        //boolean b = lock.tryLock(5,10,TimeUnit.SECONDS);
        try {
    
            System.out.println("上锁拉拉"+Thread.currentThread().getName());
            Thread.sleep(10000);
            // 执行业务代码
                return "执行业务代码！";
                
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("解锁锁拉拉"+Thread.currentThread().getName());
            lock.unlock();
        }
        return "s";
    }
    
}
