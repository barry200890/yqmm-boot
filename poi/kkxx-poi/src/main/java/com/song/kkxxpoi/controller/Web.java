/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    Web
 * Date:    2022-05-02 16:08
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.service.IIntEndScaleForecastService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-02 16:08
 */
@Api(tags = "事务注解失效测试")
@RestController
@RequestMapping("transactional-web")
@Slf4j
public class Web {
    @Resource
    private IIntEndScaleForecastService iIntEndScaleForecastService;

    
    @ApiOperation("public修饰方法同一个类中")
    @PostMapping("test-public")
    public int insert(@RequestBody IntEndScaleForecastEntity entity) {
        int insert = iIntEndScaleForecastService.insert(entity);
        return insert;
    }

    @ApiOperation("2个try能回滚码？")
    @PostMapping("test-try")
    public String testTry(@RequestBody IntEndScaleForecastEntity entity) {
       return iIntEndScaleForecastService.insertOrUpdate(entity);
    }
}
