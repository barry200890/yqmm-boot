/*******************************************************************************
 * Package: com.song.kkxxpoi
 * Type:    Da
 * Date:    2022-03-27 21:42
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi;

import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-27 21:42
 */
@Data
public class Da {
    private Date startDate;
    
    public Da(Date startDate) {
        this.startDate = startDate;
    }
}
