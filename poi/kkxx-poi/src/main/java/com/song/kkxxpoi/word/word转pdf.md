在某些业务场景下，可能需要将Word文件转换为Pdf文件的需求，Word文件编辑方便，而Pdf文件查看更方便，并且格式基本不会变化，同时可以避免被误编辑。

如果使用的是Java语言，那么可以借助于Jacob开源库实现将Word文件转换为Pdf文件，Jacob是什么呢？先看看官方定义

> Jacob 是一个 Java 库，可让 Java 应用程序与 Microsoft Windows DLL 或 COM 库进行通信，它使用 Jacob Java 类通过 JNI 与之通信的自定义 DLL 来实现这一点。

也就是说 Jacob 的实现方式是通过 jacob.dll 调用系统本地安装的 Office 来实现转换，因此使用 Jacob 也有一定的限制：

+   **由于是 dll 库，所以必须是在 Windows 系统环境；**
+   **系统需要先安装Office Word应用，比如 MicroSoft Office 或者 WPS Office；**

下面通过具体的实践操作和代码来演示。

### 1、引入Jacob依赖

jacob在github上的官方地址为：[https://github.com/freemansoft/jacob-project](https://github.com/freemansoft/jacob-project)，目前最新版本为1.20，如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/a31c08e367084dd29154b44887e0cca3.png#pic_center)  
如果是首次使用 jacob，建议先直接从 github上下载 zip包，因为在实现Word转Pdf的时候，不仅需要用到 jacob.jar，还需要 jacob.dll 库，而zip包中都有。

![Jacob](https://img-blog.csdnimg.cn/60bd3aad43ba495790467302eb8d6b81.png#pic_center)  
为了方便后期的使用，可以把下载的jar安装到本地的maven库中，具体使用的命令如下：

```shell
mvn install:install-file    
    -Dfile=C:\Users\admin\Downloads\jacob-1.20\jacob.jar  # 配置 jar 包文件所在的位置
    -DgroupId=com.jacob  # 配置生成 jar 包对应的 groupId
    -DartifactId=jacob   # 配置生成 jar 包对应的 artifactId
    -Dpackaging=jar    # 配置文件的打包方式, 此处为 jar
    -Dversion=1.20    # 配置版本号, 只要符合 Maven 的版本命名规范即可
```

简写成一行的话，如下：

```shell
mvn install:install-file -Dfile=C:\Users\admin\Downloads\jacob-1.20\jacob.jar -DgroupId=com.jacob -DartifactId=jacob -Dpackaging=jar -Dversion=1.20
```

命令执行成功后，就可以在本地仓库看到类似的文件

![在这里插入图片描述](https://img-blog.csdnimg.cn/7832ba0cbad84dd1b4cc5342908abfad.png#pic_center)  
至此，以后就可以直接在pom.xml 文件加入以下依赖进行引用

```xml
<dependency>
    <groupId>com.jacob</groupId>
    <artifactId>jacob</artifactId>
    <version>1.20</version>
</dependency>
```

### 2、拷贝 jacob.dll 文件

`jacob.dll` 文件分为x86和x64的，根据自身系统来进行选择，这里拷贝 `jacob-1.20-x64.dll` 文件到 `jdk/bin` 或者 `jdk/jre/bin` 目录下都可以，这里选择将其拷贝至 `jdk/jre/bin` 目录中，如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/f81382b0699446da9b130ca3124f6e2a.png#pic_center)  
官方使用文档可以参考：[https://github.com/freemansoft/jacob-project/blob/main/docs/UsingJacob.md](https://github.com/freemansoft/jacob-project/blob/main/docs/UsingJacob.md)

### 3、代码实现

```java
package com.magic.jacob;

import java.io.File;
import java.nio.file.Files;
import java.util.Objects;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComThread;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

/**
 * Word转PDF工具类
 */
public class WordToPdfUtils {

    /** word 转换为 pdf 的格式宏，值为 17 */
    private static final int WORD_FORMAT_PDF = 17;
    private static final String MS_OFFICE_APPLICATION = "Word.Application";
    private static final String WPS_OFFICE_APPLICATION = "KWPS.Application";

    /**
     * 微软Office Word转PDF
     * 如果无法转换，可能需要下载 SaveAsPDFandXPS.exe 插件并安装
     * @param wordFile Word文件
     * @param pdfFile Pdf文件
     */
    public static void msOfficeToPdf(String wordFile, String pdfFile) {
        wordToPdf(wordFile, pdfFile, MS_OFFICE_APPLICATION);
    }

    /**
     * WPS Office Word转PDF
     * @param wordFile Word文件
     * @param pdfFile Pdf文件
     */
    public static void wpsOfficeToPdf(String wordFile, String pdfFile) {
        wordToPdf(wordFile, pdfFile, WPS_OFFICE_APPLICATION);
    }

    /**
     * Word 转 PDF
     * @param wordFile Word文件
     * @param pdfFile Pdf文件
     * @param application Office 应用
     */
    private static void wordToPdf(String wordFile, String pdfFile, String application) {
        Objects.requireNonNull(wordFile);
        Objects.requireNonNull(pdfFile);
        Objects.requireNonNull(application);

        ActiveXComponent app = null;
        Dispatch document = null;
        try {
            File outFile = new File(pdfFile);
            // 如果目标路径不存在, 则新建该路径，否则会报错
            if (!outFile.getParentFile().exists()) {
                Files.createDirectories(outFile.getParentFile().toPath());
            }

            // 如果目标文件存在，则先删除
            if (outFile.exists()) {
                outFile.delete();
            }

            // 这里需要根据当前环境安装的是 MicroSoft Office还是WPS来选择
            // 如果安装的是WPS，则需要使用 KWPS.Application
            // 如果安装的是微软的 Office，需要使用 Word.Application
            app = new ActiveXComponent(application);
            app.setProperty("Visible", new Variant(false));
            app.setProperty("AutomationSecurity", new Variant(3));

            Dispatch documents = app.getProperty("Documents").toDispatch();
            document = Dispatch.call(documents, "Open", wordFile, false, true).toDispatch();

            Dispatch.call(document, "ExportAsFixedFormat", pdfFile, WORD_FORMAT_PDF);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (document != null) {
                Dispatch.call(document, "Close", false);
            }

            if (app != null) {
                app.invoke("Quit", 0);
            }

            ComThread.Release();
        }
    }
}
```

如果找不到 dll 文件，则会报错，具体错误信息如下：

```java
Exception in thread "main" java.lang.UnsatisfiedLinkError: no jacob-1.20-x64 in java.library.path
	at java.lang.ClassLoader.loadLibrary(ClassLoader.java:1867)
	at java.lang.Runtime.loadLibrary0(Runtime.java:870)
	at java.lang.System.loadLibrary(System.java:1122)
	at com.jacob.com.LibraryLoader.loadJacobLibrary(LibraryLoader.java:184)
	at com.jacob.com.JacobObject.<clinit>(JacobObject.java:110)
	at com.magic.springlearning.jacob.WordToPdfUtils.officeToPdf(WordToPdfUtils.java:31)
	at com.magic.springlearning.jacob.WordToPdfUtils.main(WordToPdfUtils.java:17)
```

### 4、测试验证

```java
public static void main(String[] args) {
    wpsOfficeToPdf("D:\\Test\\test_word.docx", "D:\\Test\\test_word.pdf");
}
```

由于本地安装的是WPS Office，所以选择 `wpsOfficeToPdf()` 方法，经过测试，如果选择 `msOfficeToPdf()` 方法也是可以的，只是转换后的文件大小相差了一倍，转换后的文件效果如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/6ad526d70f99475dbf195f3778b9b766.png#pic_center)