/*******************************************************************************
 * Package: com.song.kkxxpoi.entity
 * Type:    User
 * Date:    2022-08-12 22:20
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.entity;

import com.song.kkxxpoi.common.CreateTime;
import com.song.kkxxpoi.common.CreateUser;
import com.song.kkxxpoi.common.UpdateTime;
import com.song.kkxxpoi.common.UpdateUser;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-08-12 22:20
 */

public class User {
    
    /**
     * 名字
     */
    private String name;
    /** 创建人 */
    @CreateUser
    private String createUser;
    
    /** 创建时间 */
    @CreateTime
    private Date createTime;
    
    /** 更新人 */
    @UpdateUser
    private String updateUser;
    
    /** 更新时间 */
    @UpdateTime
    private Date updateTime;
    
    public User(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCreateUser() {
        return createUser;
    }
    
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }
    
    public Date getCreateTime() {
        return createTime;
    }
    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    
    public String getUpdateUser() {
        return updateUser;
    }
    
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    
    public Date getUpdateTime() {
        return updateTime;
    }
    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
