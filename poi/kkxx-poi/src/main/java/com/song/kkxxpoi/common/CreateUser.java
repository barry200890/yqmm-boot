/*******************************************************************************
 * Package: com.song.kkxxpoi.common
 * Type:    CreateUser
 * Date:    2022-08-12 21:51
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-08-12 21:51
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface CreateUser {
}
