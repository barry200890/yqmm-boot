/*******************************************************************************
 * Package: com.song.kkxxpoi.enums
 * Type:    Roles
 * Date:    2022-03-30 9:43
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.enums;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-03-30 9:43
 */
public enum Roles {
    ADMIN(1,"超级管理员"),
    GROUP_ADMIN(2,"集团管理员"),
    SCHOOL_ADMIN(3,"分校管理员"),
    BUSINESS_ADMIN(4,"业务管理员"),
    SCHOOL_OPERATION(5,"分校运营员"),
    BUSINESS_OPERATION(6,"业务运营员");
    
    private Integer role;
    
    private String name;
    
    Roles(Integer role, String name) {
        this.role = role;
        this.name = name;
    }
    
    public Integer getRole() {
        return role;
    }
    
    public String getName() {
        return name;
    }
    
    /**
     * 获取角色名称
     * @param role
     * @return
     */
    public static String getNameByRole(Integer role){
        for (Roles value : Roles.values()) {
            if(value.getRole().equals(role)){
                return value.getName();
            }
        }
        return null;
    }
}
