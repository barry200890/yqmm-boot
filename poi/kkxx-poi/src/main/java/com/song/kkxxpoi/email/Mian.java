/*******************************************************************************
 * Package: com.DocSystem.email
 * Type:    Mian
 * Date:    2023-10-08 9:10
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.email;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-10-08 9:10
 */
public class Mian {
    public static void main(String[] args) throws Exception {
//163邮箱测试
//        String userName = "xx@163.com"; // 发件人邮箱
//        String password = "xx"; // 发件人密码，其实不一定是邮箱的登录密码，对于QQ邮箱来说是SMTP服务的授权文本
//        String smtpHost = "smtp.163.com"; // 邮件服务器

        String userName = "xx@xx.com"; // 发件人邮箱
        String password = "xx@Q"; // 发件人密码，其实不一定是邮箱的登录密码，对于QQ邮箱来说是SMTP服务的授权文本
        String smtpHost = "ss.qq.com"; // 邮件服务器

        String to = "1965569785@qq.com"; // 收件人，多个收件人以半角逗号分隔
        //String to = "huaqiang.wang@rsglzx.com"; // 收件人，多个收件人以半角逗号分隔
        String cc = null; // 抄送，多个抄送以半角逗号分隔
        String subject = "这是邮件的主题"; // 主题
        String body = "这是邮件的正文163"; // 正文，可以用html格式的哟
        //List<String> attachments = Arrays.asList("D:\\安装包\\【繁星课堂】DataX3.0离线同步课件.pdf", "D:\\安装包\\权限控制优化需求.docx"); // 附件的路径，多个附件也不怕

        EmailUtils emailUtils = EmailUtils.entity(smtpHost, userName, password, to, cc, subject, body, null);

        emailUtils.send(); // 发送！
    }
}
