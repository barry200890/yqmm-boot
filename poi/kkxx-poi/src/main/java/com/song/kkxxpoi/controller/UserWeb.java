/*******************************************************************************
 * Package: com.song.kkxxpoi.controller
 * Type:    UserWeb
 * Date:    2022-08-12 22:33
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.controller;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.service.IIntEndScaleForecastService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

/**
 * 功能描述：mybatis-拦截器
 *
 * @author Songxianyang
 * @date 2022-08-12 22:33
 */
@Api(tags = "测试-拦截器")
@RestController
@RequestMapping("user-web")
@Slf4j
public class UserWeb {
    @Resource
    private IIntEndScaleForecastService iIntEndScaleForecastService;
    
    @GetMapping("insert/{subProjectName}")
    public String insert(@PathVariable("subProjectName") String subProjectName) {
        IntEndScaleForecastEntity entity = new IntEndScaleForecastEntity();
        entity.setId(UUID.randomUUID().toString());
        entity.setSubProjectName(subProjectName);
        
        //entity.setCreateBy("sss");
        //entity.setCreateTime(new Date());
        //entity.setUpdateBy("ssss");
        //entity.setUpdateTime(new Date());
        
        iIntEndScaleForecastService.insert(entity);
        return "成功";
    }
    
}
