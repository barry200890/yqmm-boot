/*******************************************************************************
 * Package: com.song.kkxxpoi.service.impl
 * Type:    IIntEndScaleForecastServiceImpl
 * Date:    2022-05-02 15:55
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.kkxxpoi.service.impl;

import com.song.kkxxpoi.entity.IntEndScaleForecastEntity;
import com.song.kkxxpoi.mapper.IIntEndScaleForecastMapper;
import com.song.kkxxpoi.service.IIntEndScaleForecastService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-02 15:55
 */
@Service
public class IIntEndScaleForecastServiceImpl implements IIntEndScaleForecastService {
    
    @Resource
    private IIntEndScaleForecastMapper iIntEndScaleForecastMapper;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(IntEndScaleForecastEntity entity) {
       return iIntEndScaleForecastMapper.insert(entity);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public int update(IntEndScaleForecastEntity entity) {
        int result = iIntEndScaleForecastMapper.updateIgnoreNull(entity);
        return result;
    }

    @Override
    public String insertOrUpdate(IntEndScaleForecastEntity entity) {
        try {
            try {
                int update = update(entity);
            } finally {
                throw new RuntimeException("error！");
            }
        } catch (RuntimeException e) {
            System.out.println(">>>"+e.getMessage());
            e.printStackTrace();
        }
        return "成功";
    }

    /**
     * 事务是不是失效了 ？为什么会失效
     * @param entity
     * @return
     */
    public void publicTestInsert(IntEndScaleForecastEntity entity) {
        int result = iIntEndScaleForecastMapper.insertIgnoreNull(entity);
        System.out.println(result);
    }


}
