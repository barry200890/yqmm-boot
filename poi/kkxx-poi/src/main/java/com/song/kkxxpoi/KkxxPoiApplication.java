package com.song.kkxxpoi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.song.kkxxpoi.mapper")
public class KkxxPoiApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(KkxxPoiApplication.class, args);
    }
    
}
