package com.song.bigdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SteveBigDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(SteveBigDataApplication.class, args);
    }

}
