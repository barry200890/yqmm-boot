/*******************************************************************************
 * Package: com.song.boot.springstudy.jdk11
 * Type:    StudyNew
 * Date:    2023-02-28 22:27
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jdk11;


import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 功能描述：11 新特性学习
 *
 * @author Songxianyang
 * @date 2023-02-28 22:27
 */
public class StudyNew {
    public static void main(String[] args) throws IOException, InterruptedException {
        //var动态获取变量类型
        var s = "song";
        System.out.println(s);
        var a = "";
        // 字符串是不是空白
        System.out.println(a.isBlank());// true
        //System.out.println(b.isBlank());
        System.out.println("\\n、\\r 分割后成为一个成为流");
        var newStr = "Java 11\npk\rJava 8";
        //System.out.println(newStr.intern());
        Stream<String> lines = newStr.lines();
        lines.forEach(s1 -> System.out.println(s1));
        System.out.println("字符串重复计数次数的串联");
        //字符串重复计数次数的串联
        String s2 = "song-";
        System.out.println(s2.repeat(0));
        System.out.println(s2.repeat(1));
        System.out.println(s2.repeat(2));
        System.out.println("java8--集合转数组");
        List<String> sList = Arrays.asList("song", "xian", "yang");
        String[] ss = new String[sList.size()];
        for (int i = 0; i < sList.size(); i++) {
            ss[i] = sList.get(i);
        }
        System.out.println(Arrays.asList(ss));
        System.out.println("java11--集合转数组");
        String[] toArray = sList.toArray(String[]::new);
        System.out.println(Arrays.asList(toArray));
        System.out.println("断言的使用、已什么开头的字符串、已什么结尾的字符串");
        // 已s开头的字符串
        System.out.println(sList.stream().filter(s1 -> s1.startsWith("s"))
                //已g结尾的字符串
                .filter(s1 -> s1.endsWith("g"))
                // 并且还不包含x
                .filter(Predicate.not(s1 -> s1.contains("x")))
                .collect(Collectors.toList()));

        String dir= "C:\\Users\\SongXianYang\\Desktop\\sssss.txt";
// 写入文件
        Path path = Files.writeString(Path.of(dir), "hello java 11");
// 读取文件
        String fileContent = Files.readString(path);
        // hello java 11
        System.out.println(fileContent);

        var request = HttpRequest.newBuilder()
                .uri(URI.create("https://blog.csdn.net/zyj1051574045/article/details/123618412"))
                .GET()
                .build();
        var client = HttpClient.newHttpClient();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        System.out.println(response.body());
        System.out.println("------");
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body).thenAccept(System.out::println);


        List<Integer> list = List.of(1, 2, 3).stream()
                .filter(i -> i > 2)
                .filter(i -> i < 10)
                .filter(i -> i % 2 == 0)
                .collect(Collectors.toList());

        list.stream().forEachOrdered(l -> System.out.println(l));
    }
}
