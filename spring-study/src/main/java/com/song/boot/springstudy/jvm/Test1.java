/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    Test1
 * Date:    2022-11-13 17:43
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-11-13 17:43
 */
public class Test1 {
    private static int i = 11;
    static {
        i = 999;
    }
    public static void main(String[] args) {
        System.out.println(i);
        int i = 9;
        int j = 6;
        int z = i+j;
    }
}
