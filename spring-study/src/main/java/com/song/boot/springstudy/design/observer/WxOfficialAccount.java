/*******************************************************************************
 * Package: com.song.boot.springstudy.design.observer
 * Type:    WxOfficialAccount
 * Date:    2023-01-05 21:49
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.observer;

import lombok.Data;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 功能描述：
 *  微信公众号
 * @author Songxianyang
 * @date 2023-01-05 21:49
 */
@Data
public class WxOfficialAccount implements MyObservable {
    // 公众号名称
    private String wxOfficialAccountName;

    // 具体文章
    private String text;

    // 你拥有的粉丝列表
    private List<MyObserver> list= new CopyOnWriteArrayList<>();

    // 选择你要发送的 用户
    @Override
    public void add(MyObserver user) {
        list.add(user);
    }
    // 移除要发送的 用户
    @Override
    public void delete(MyObserver user) {
        list.remove(user);
    }
    // 群发用户 文章信息
    @Override
    public void send() {
        list.forEach(observer -> observer.accept("公众号："+wxOfficialAccountName+"--发布了一篇："+text));
    }
}
