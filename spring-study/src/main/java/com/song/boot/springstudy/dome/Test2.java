/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test2
 * Date:    2022-12-23 21:01
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.dome;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-12-23 21:01
 */
public class Test2 implements Call {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(2);
        list.add("song");
        list.add("xian");
        list.add("yang");
    }
}
