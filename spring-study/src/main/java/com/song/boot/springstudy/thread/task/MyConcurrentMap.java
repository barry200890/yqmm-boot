/*******************************************************************************
 * Package: com.song.boot.springstudy.thread.task
 * Type:    MyConcurrenthashmap
 * Date:    2022-07-09 23:37
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread.task;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-07-09 23:37
 */
public class MyConcurrentMap {
    public static void main(String[] args) {
        Map<String, Integer> concurrentHashMap = new ConcurrentHashMap<>(10);
        concurrentHashMap.put("song", 10);
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 11);
    }
}
