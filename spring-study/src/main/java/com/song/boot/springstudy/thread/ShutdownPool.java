/*******************************************************************************
 * Package: com.song.boot.springstudy.thread
 * Type:    ShutdownPool
 * Date:    2022-05-28 15:08
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.thread;

import lombok.SneakyThrows;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 功能描述： 停止线程方式  3种
 *
 * @author Songxianyang
 * @date 2022-05-28 15:08
 */
public class ShutdownPool {
    @SneakyThrows
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0, size = 1000; i < size; i++) {
            executorService.execute(new Task());
        }
        //Thread.sleep(1000);
        //executorService.shutdown();
        executorService.shutdownNow();
        
        System.out.println(executorService.awaitTermination(2L, TimeUnit.MINUTES));
    
        //System.out.println(executorService.isShutdown());
        //System.out.println(executorService.isTerminated());
        //Thread.sleep(1000);
        executorService.execute(new Task());
    }
}

class Task implements Runnable {
    
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"业务代码哈哈");
    }
}

class Task1 implements Callable {
    
    @Override
    public String call() throws Exception {
        return "null";
    }
}
