/*******************************************************************************
 * Package: com.song.boot.springstudy.test
 * Type:    Test9
 * Date:    2023-12-03 15:24
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

/**
 * 功能描述：
 *
 *
 * @author Songxianyang
 * @date 2023-12-03 15:24
 */
public class Test9 {
    // 正则校验邮箱
    public static void main(String[] args) {
        String email = "123456789@qq.com";
        String regex = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
        //使用正则表达式判断email是否符合格式
        boolean flag = email.matches(regex);
        System.out.println(flag);
    }
}
