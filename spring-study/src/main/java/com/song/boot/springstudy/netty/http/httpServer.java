/*******************************************************************************
 * Package: com.song.boot.springstudy.netty.http
 * Type:    httpServer
 * Date:    2023-06-20 21:10
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 功能描述： http服务器
 *浏览器为客户端
 * http://localhost:1111
 * http协议不是一个长连接。每次刷新都会出现新的管道与新的处理器
 * @author Songxianyang
 * @date 2023-06-20 21:10
 */
public class httpServer {
    public static void main(String[] args) throws Exception{
        EventLoopGroup bossEvent = new NioEventLoopGroup();
        EventLoopGroup workerEvent = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossEvent, workerEvent)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new HttpServerChannelInitializer());
            // 构建连接6668 端口
            ChannelFuture sync = serverBootstrap.bind(1111).sync();
            // 对关闭通道进行见监听
            sync.channel().closeFuture().sync();
        }finally {
            bossEvent.shutdownGracefully();
            workerEvent.shutdownGracefully();
        }
    }
}
