/*******************************************************************************
 * Package: com.song.boot.springstudy.entity
 * Type:    ListUserRole
 * Date:    2022-01-20 19:22
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-01-20 19:22
 */
@Data
public class ListUserRole {
    private Integer id;
}
