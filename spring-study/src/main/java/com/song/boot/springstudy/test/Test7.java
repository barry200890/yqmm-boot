/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test7
 * Date:    2022-11-12 23:10
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.RewardCountReportBase;
import com.song.boot.springstudy.entity.RewardCountSale;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-11-12 23:10
 */
public class Test7 {
    public static void main(String[] args) {
        //RewardCountReportBase song = new RewardCountReportBase("123", "song");
        //RewardCountReportBase xian = new RewardCountReportBase("234", "xian");
        //List<RewardCountReportBase> rewardCountReportBases = new ArrayList<>();
        //rewardCountReportBases.add(song);
        //rewardCountReportBases.add(xian);
        //rewardCountReportBases.add(rewardCountSale);

        RewardCountSale rewardCountSale = new RewardCountSale();
        rewardCountSale.setSaleOrg("sssss");
        rewardCountSale.setProjectId("1111");
        rewardCountSale.setProjectName("song");
        List<RewardCountSale> rewardCountSales = new ArrayList<>();
        rewardCountSales.add(rewardCountSale);
        ssss(rewardCountSales,rewardCountSale);

    }

    public static void ssss(List<? extends RewardCountReportBase> rewardCountReportBases,RewardCountReportBase rewardCountReportBase) {
        for (RewardCountReportBase base : rewardCountReportBases) {
            System.out.println(base.toString());
        }
    }
}
