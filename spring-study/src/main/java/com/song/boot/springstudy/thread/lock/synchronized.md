# [视频链接](https://www.bilibili.com/video/BV1aJ411V763)
# 在多线程环境下去访问一个共享变量会出现什么问题
> 原子性、有序性、可见性
> 
# [Java内存模型（JMM模型）](https://blog.csdn.net/m0_51431003/article/details/127702972)
> Java内存模型不是jvm运行时数据区。线程-工作内存-主存 将主存里面的数据复制到工作内存里面实现多线程的可见性。要分为8大操作
![](https://img-blog.csdnimg.cn/779c290541d0430babc9c3764019a375.png#pic_center)
>lock（锁定）: 作用于主内存中的变量，将他标记为一个线程独享变量。
> 
> unlock（解锁）: 作用于主内存中的变量，解除变量的锁定状态，被解除锁定状态的变量才能被其他线程锁定。
> 
> read（读取）：作用于主内存的变量，它把一个变量的值从主内存传输到线程的工作内存中，以便随后的 load 动作使用。
> 
> load（载入）：把 read 操作从主内存中得到的变量值放入工作内存的变量的副本中。
> 
> use（使用）：把工作内存中的一个变量的值传给执行引擎，每当虚拟机遇到一个使用到变量的指令时都会使用该指令。
> 
> assign（赋值）：作用于工作内存的变量，它把一个从执行引擎接收到的值赋给工作内存的变量，每当虚拟机遇到一个给变量赋值的字节码指令时执行这个操作。
> 
> store（存储）：作用于工作内存的变量，它把工作内存中一个变量的值传送到主内存中，以便随后的 write 操作使用。
> 
> write（写入）：作用于主内存的变量，它把 store 操作从工作内存中得到的变量的值放入主内存的变量中。
> 
# synchronized 如何保证 原子性、有序性、可见性
> 原子性操作：只有一个线程去访问共享变量，从而保证原子性操作
> 
> 可见性 synchronized关键字他会将主存数据刷新到当前线程的工作内存当中，从而实现可见性操作
> 
> 在单线程下的指令重排序  不会影响当前线程的执行（需要使用同一把锁）
> 
# synchronized 可重入 ，如何实现的可重入
> 里面有一个计数器 来记录当前线程 当前线程重入的次数+1，直到计数器为0才能释放锁对象。

# synchronized 是不可中断的锁
> 线程1获取到锁后，一直没有释放锁，线程2在阻塞等待，线程2是不可中断的！

# 反汇编指令
> 找到目录class文件目录  javap -p -v .\MySynchronized.class
# synchronized 加锁原理
>  monitorenter  其内部维护了monitor对象，里面有两个属性：owner 持有当前线程的名字、recursions 记录重入的次数
> 
> 当线程执行时owner获取当前线程名称，recursions次数加一，重入时++
> 
> monitorexit 其内部维护了monitor对象
> 
> recursions次数为0时释放锁
> 
# 为什么 编译后有两个monitorexit 
> 正常代码执行结果释放 和报异常后释放锁
> 
```xml
        4: monitorenter
         5: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
         8: ldc           #4                  // String 1112
        10: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
        13: aload_1
        14: monitorexit
        15: goto          23
        18: astore_2
        19: aload_1
        20: monitorexit
        21: aload_2
        22: athrow
        23: return

```