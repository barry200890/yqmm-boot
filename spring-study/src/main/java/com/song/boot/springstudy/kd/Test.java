/*******************************************************************************
 * Package: com.song.boot.springstudy.kd
 * Type:    Test
 * Date:    2023-06-18 18:35
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.kd;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-06-18 18:35
 */
public class Test {
    public static void main(String[] args) {
        QzapiImpl qzapi = new QzapiImpl();
        System.out.println(qzapi.getXnxq("....."));
    }
}
