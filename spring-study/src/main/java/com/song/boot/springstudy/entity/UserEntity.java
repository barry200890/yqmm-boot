/*******************************************************************************
 * Package: com.song.boot.springstudy.entity
 * Type:    UserEntity
 * Date:    2022-01-20 18:17
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 功能描述：
 *com.song.boot.springstudy.entity.UserEntity
 * @author Songxianyang
 * @date 2022-01-20 18:17
 */
@Data
@ToString
@EqualsAndHashCode
public class UserEntity {
    private String name;
    private String type;
    
    private Integer id;
}
