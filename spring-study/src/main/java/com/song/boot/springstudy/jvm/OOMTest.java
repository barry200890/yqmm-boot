/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    OOMTest
 * Date:    2022-11-26 21:44
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-11-26 21:44
 */
public class OOMTest {
    public static void main(String[] args) throws InterruptedException {
        // 这种写法 太直观了，我要使用循环中的对象，放到List中
        List<Object> objects = new ArrayList<>();
        while (true) {
            Thread.sleep(1);
            String s = new String();
            objects.add(s);
        }

        /*
        这种写法  GC回收对象。因为我只是在创建对象，他们的引用地址没有得到使用   GC 就会将其我在while中的创建好了的对象回收掉。
         */
        //while (true) {
        //    String dsfds = new String();
        //    List<Object> fdsds = new ArrayList<>();
        //    OOMTest oomTest = new OOMTest();
        //
        //}
    }

}
