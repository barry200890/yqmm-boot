/*******************************************************************************
 * Package: com.song.boot.springstudy
 * Type:    Test2
 * Date:    2022-06-23 16:32
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.UserEntity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-06-23 16:32
 */
public class Test2 {
    public static void main(String[] args) {
        String s = "eee";
        String ss = "eee";
        Integer i = 100;
        Integer ii = 100;
        Integer integer = new Integer(100);
        Integer integer1 = new Integer(100);
        System.out.println(integer.toString());
        System.out.println(integer1.hashCode());
        System.out.println(i.hashCode());
        System.out.println(ii.hashCode());
        System.out.println(ss.hashCode());
        System.out.println(s.hashCode());
        System.out.println(s==ss);
    }
}
