/*******************************************************************************
 * Package: com.song.boot.springstudy.design.duty
 * Type:    AbstractCheck
 * Date:    2022-12-29 23:12
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.design.duty;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * 功能描述：  责任链模式
 *
 * @author Songxianyang
 * @date 2022-12-29 23:12
 */
@Component
public abstract class AbstractCheck {
    @Getter
    @Setter
    private AbstractCheck abstractCheckNext;


    protected abstract void check(LeaveVO vo);

    public void next(LeaveVO vo) {
        // 防止重复执行
        //if (Objects.isNull(abstractCheckNext)) {
        //    this.check(vo);
        //}
        abstractCheckNext.check(vo);
    }

}
