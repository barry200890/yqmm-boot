/*******************************************************************************
 * Package: com.song.boot.springstudy.netty
 * Type:    C
 * Date:    2023-06-04 17:38
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.netty;

/**
 * 功能描述：
 *  学习连接：https://blog.csdn.net/qq_28742063/article/details/126545899
 *          https://gitee.com/xiany_u/netty-learning/tree/master
 * @author Songxianyang
 * @date 2023-06-04 17:38
 */
public class C {
    /**
     * Bootstrap、ServerBootstrap
     * 1Bootstrap 意思是引导，一个 Netty 应用通常由一个 Bootstrap开始，主要作用是配置整个 Netty 程序，串联各个组件，Netty 中 Bootstrap 类是客户端程序的启动引导类，ServerBootstrap 是服务端启动引导类
     * 常见的方法有public ServerBootstrap group(EventLoopGroup parentGroup, EventLoopGroup childGroup)，该方法用于服务器端，用来设置两个 EventLooppublic B group(EventLoopGroup group)，该方法用于客户端，用来设置一个 EventLoopGrouppublic B channel(Class<? extends C> channelClass)，该方法用来设置一个服务器端的通道实现public<T> B option(ChannelOption<T> option,Tvalue)，用来给 ServerChannel 添加配置public <T> ServerBootstrap childOption(ChannelOption<T> childOption, Tvalue)，用来给接收到的通道添加配置
     * public ServerBootstrap childHandler(ChannelHandler childHandler)，该方法用来设置业务处理类(自定义的 handler)
     * public ChannelFuture bind(int inetPort)，该方法用于服务器端，用来设置占用的端口号public ChannelFuture connect(String inetHost,int inetPort)，该方法用于客户端，用来连接服务器
     *
     * Future、ChannelFuture
     * 1) Netty 中所有的 10 操作都是异步的，不能立刻得知消息是否被正确处理。但是可以过一会等它执行完成或者直接注册一个监听，具体的实现就是通过 Future 和ChannelFutures，他们可以注册一个监听，当操作执行成功或失败时监听会自动触发注册的监听事件
     * 常见的方法有2)
     * Channelchannel)，返回当前正在进行 10 操作的通道ChannelFuture sync()，等待异步操作执行完毕
     *
     * Channel
     * yNetty 网络通信的组件，能够用于执行网络 I/0 操作。2通过Channel可获得当前网络连接的通道的状态3) 通过Channel可获得 网络连接的配置参数 (例如接收缓冲区大小)
     * 4) Channel提供异步的网络 I/0 操作(如建立连接，读写，绑定端口)，异步调用意味着任何 I/0 调用都将立即返回，并且不保证在调用结束时所请求的 I/0 操作已完成5) 调用立即返回一个 ChannelFuture 实例，通过注册监听器到 ChannelFuture上，可以l/O 操作成功、失败或取消时回调通知调用方
     */
}
