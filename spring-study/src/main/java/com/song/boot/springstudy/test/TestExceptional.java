/*******************************************************************************
 * Package: com.song.boot.springstudy.test
 * Type:    TestExceptional
 * Date:    2023-08-23 22:37
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

/**
 * 功能描述： 测试异常
 *
 * @author Songxianyang
 * @date 2023-08-23 22:37
 */
public class TestExceptional {

    public static void main(String[] args) {
        try {
            System.out.println("hhhhhhhh");
            int i = 1 / 0;
        } catch (Exception e) {
            throw e;

        }finally {
            System.out.println("肯定执行");
        }
    }
}
