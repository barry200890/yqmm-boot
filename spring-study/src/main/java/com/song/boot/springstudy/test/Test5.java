/*******************************************************************************
 * Package: com.song.boot.springstudy.thread
 * Type:    Test5
 * Date:    2022-10-31 10:12
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.test;

import com.song.boot.springstudy.entity.One;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-10-31 10:12
 */
@Slf4j
public class Test5 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Instant stime1 = Instant.now();
        List<One> ones = new ArrayList<>(20433098);
        One one= new One();

        for (int i = 0, size = 20433098; i < size; i++) {
            One one1 =(One) one.clone();
            one1.setCount("song" + i);
            //one = nnnn(i);
            ones.add(one1);
        }
        System.out.println(ones);
        Instant stime2 = Instant.now();
        log.info(">>>转换耗时={}...", Duration.between(stime1, stime2).toMillis());
    }
}
