/*******************************************************************************
 * Package: com.song.boot.springstudy.jvm
 * Type:    StringTest
 * Date:    2023-04-11 18:05
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jvm;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-04-11 18:05
 */
public class StringTest {
    public static void main(String[] args) {
        //String str1 = "a";
        String str2 = "aa";
        if ("aa" == str2) {
            System.out.println("ssss");
        } else {
            System.out.println("else");
        }
        //String a = new String("a").intern();
        //String a1 = new String("a").intern();
        //System.out.println(a == a1);

        //AtomicInteger atomicMaxSortNo = new AtomicInteger(99);
        //System.out.println(atomicMaxSortNo.addAndGet(1));


        LocalDate date = LocalDate.parse("2017-03-03");
        System.out.println(date);
        LocalDate date1 = LocalDate.parse("2017-03-03");
        System.out.println(date1);
        System.out.println(date1.compareTo(date));

    }
}
