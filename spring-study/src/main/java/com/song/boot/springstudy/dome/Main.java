/*******************************************************************************
 * Package: com.song.boot.springstudy.dome
 * Type:    Main
 * Date:    2022-12-23 21:02
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.dome;

import java.util.HashSet;
import java.util.Set;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-12-23 21:02
 */
public class Main {

    public static void main(String[] args) {
        Call test1 = new Test1();

        if (test1 instanceof Call) {
            System.out.println("成功了");
        }

        Set<String> strings = new HashSet<>();

        System.out.println(strings.add("1"));
        System.out.println(strings.add("1"));

    }
}
