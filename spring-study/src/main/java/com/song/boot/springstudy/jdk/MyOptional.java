/*******************************************************************************
 * Package: com.song.boot.springstudy.jdk
 * Type:    D
 * Date:    2023-03-04 20:48
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.boot.springstudy.jdk;

import java.util.*;
import java.util.function.Supplier;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-03-04 20:48
 */
public class MyOptional {

    public static void main(String[] args) throws Throwable {
        //https://blog.csdn.net/xiao_yu_gan/article/details/125661440
        List<String> list = null;
         list = Arrays.asList("song", "xian", "yang");
        // 如果值为null报异常
        Optional<List<String>> optionalList = Optional.of(list);
        // 如果为null的时候 输出 你想输出的异常
        //System.out.println(optionalList.orElse(Collections.EMPTY_LIST));
        // 为 null的时候自动抛出错选异常
        //Optional.ofNullable(list).orElseThrow((Supplier<Throwable>) () -> new RuntimeException("为null报错！"));
        // 为null的时候我便输出 orElseGet(() -> Arrays.asList("song", "xian", "yang") 定义好的数据
        System.out.println(Optional.ofNullable(list).orElseGet(() -> Arrays.asList("song", "xian", "yang")));
        // 判断 Optional 中的值是否为null  不等于null返回真 等于null返回假
        System.out.println(optionalList.isPresent());

    }
}
