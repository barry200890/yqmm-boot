# [【已满足】企业级开发的flowable工作流平台-学习借鉴-集成分布式项目都非常方便SpringBoot+swagger+mybatis plus+mysql](https://blog.csdn.net/weixin_48278764/article/details/122800320)
# [Flowable多实例会签功能来袭](https://blog.csdn.net/weixin_48278764/article/details/126920927)
# [Flowable多实例加签减签功能正式上线](https://blog.csdn.net/weixin_48278764/article/details/126920496)
# [退回](https://gitee.com/SongXianYang/yqmm-boot/blob/master/yqmm-flowable/src/main/resources/%E9%80%80%E5%9B%9E/Return.md)
- [X] 通过或者拒绝
- [X] (唤酲)被挂起的流程实例
- [X] 多实例加签
- [X] 流程部署
- [X] 驳回流程
- [X] 多实例减签
- [X] 终止流程
- [X] 用户已办
- [X] 查询所有启动的流程列表
- [X] 流程图
- [X] 我发起的流程实例列表
- [X] 流程部署列表
- [X] 挂起流程实例
- [X] 查看历史流程记录
- [X] 判断传入流程实例在运行中是否存在
- [X] 查询会签节点的审批人任务
- [X] 启动流程获取指定用户组流程任务列表
- [X] 用户待办
- [X] 或签
- [X] 退回
- [X] 加签
- [X] 减签
- [X] 邮件任务（到节点后自动发送邮件）
- [X] 定时器边界事件（过期自动跳过当前节点）
- [X] Flowable系统升级（6.8.0）为了解决act_ru_timer_job这张表没有实时更新问题
- [ ] 监听实现异步通知
- [ ] 流程表单功能
##### 如果觉得对您有帮助，希望能请SteveCode喝杯奶茶丫！嘿嘿嘿，谢谢！_没关系您白嫖我也行（大哭）_
![img_3.png](img_3.png)
# 持续优化~~~~
![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)