/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : flowable

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 19/04/2022 11:45:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for act_adm_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_adm_databasechangelog`;
CREATE TABLE `act_adm_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_adm_databasechangelog
-- ----------------------------
INSERT INTO `act_adm_databasechangelog` VALUES ('1', 'flowable', 'META-INF/liquibase/flowable-admin-app-db-changelog.xml', '2022-02-01 23:21:23', 1, 'EXECUTED', '8:655e3bb142f7d051dfc2d641ee0eeebd', 'createTable tableName=ACT_ADM_SERVER_CONFIG', '', NULL, '3.8.0', NULL, NULL, '3728883703');

-- ----------------------------
-- Table structure for act_adm_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_adm_databasechangeloglock`;
CREATE TABLE `act_adm_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_adm_databasechangeloglock
-- ----------------------------
INSERT INTO `act_adm_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_adm_server_config
-- ----------------------------
DROP TABLE IF EXISTS `act_adm_server_config`;
CREATE TABLE `act_adm_server_config`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SERVER_ADDRESS_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PORT_` int(0) NULL DEFAULT NULL,
  `CONTEXT_ROOT_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REST_ROOT_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `USER_NAME_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PASSWORD_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ENDPOINT_TYPE_` int(0) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_adm_server_config
-- ----------------------------
INSERT INTO `act_adm_server_config` VALUES ('9eaa8bde-8372-11ec-ba42-dc41a90b0909', 'Flowable Process app', 'Flowable Process REST config', 'http://localhost', 8080, '/flowable-ui', 'process-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 1, NULL);
INSERT INTO `act_adm_server_config` VALUES ('9eaad9ff-8372-11ec-ba42-dc41a90b0909', 'Flowable CMMN app', 'Flowable CMMN REST config', 'http://localhost', 8080, '/flowable-ui', 'cmmn-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 5, NULL);
INSERT INTO `act_adm_server_config` VALUES ('9eab0110-8372-11ec-ba42-dc41a90b0909', 'Flowable App app', 'Flowable App REST config', 'http://localhost', 8080, '/flowable-ui', 'app-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 6, NULL);
INSERT INTO `act_adm_server_config` VALUES ('9eab2821-8372-11ec-ba42-dc41a90b0909', 'Flowable DMN app', 'Flowable DMN REST config', 'http://localhost', 8080, '/flowable-ui', 'dmn-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 2, NULL);
INSERT INTO `act_adm_server_config` VALUES ('9eab4f32-8372-11ec-ba42-dc41a90b0909', 'Flowable Form app', 'Flowable Form REST config', 'http://localhost', 8080, '/flowable-ui', 'form-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 3, NULL);
INSERT INTO `act_adm_server_config` VALUES ('9eab7643-8372-11ec-ba42-dc41a90b0909', 'Flowable Content app', 'Flowable Content REST config', 'http://localhost', 8080, '/flowable-ui', 'content-api', 'admin', 'wf088DItMLLPiQIoM5rajQ==', 4, NULL);

-- ----------------------------
-- Table structure for act_app_appdef
-- ----------------------------
DROP TABLE IF EXISTS `act_app_appdef`;
CREATE TABLE `act_app_appdef`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `VERSION_` int(0) NOT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_APP_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE,
  INDEX `ACT_IDX_APP_DEF_DPLY`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_APP_DEF_DPLY` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_app_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_app_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_app_databasechangelog`;
CREATE TABLE `act_app_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_databasechangelog
-- ----------------------------
INSERT INTO `act_app_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-02-01 23:21:22', 1, 'EXECUTED', '8:496fc778bdf2ab13f2e1926d0e63e0a2', 'createTable tableName=ACT_APP_DEPLOYMENT; createTable tableName=ACT_APP_DEPLOYMENT_RESOURCE; addForeignKeyConstraint baseTableName=ACT_APP_DEPLOYMENT_RESOURCE, constraintName=ACT_FK_APP_RSRC_DPL, referencedTableName=ACT_APP_DEPLOYMENT; createIndex...', '', NULL, '3.8.0', NULL, NULL, '3728882043');
INSERT INTO `act_app_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-02-01 23:21:22', 2, 'EXECUTED', '8:ccea9ebfb6c1f8367ca4dd473fcbb7db', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_APP_DEPLOYMENT', '', NULL, '3.8.0', NULL, NULL, '3728882043');
INSERT INTO `act_app_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-02-01 23:21:22', 3, 'EXECUTED', '8:f1f8aff320aade831944ebad24355f3d', 'createIndex indexName=ACT_IDX_APP_DEF_UNIQ, tableName=ACT_APP_APPDEF', '', NULL, '3.8.0', NULL, NULL, '3728882043');

-- ----------------------------
-- Table structure for act_app_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_app_databasechangeloglock`;
CREATE TABLE `act_app_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_databasechangeloglock
-- ----------------------------
INSERT INTO `act_app_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_app_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_app_deployment`;
CREATE TABLE `act_app_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_app_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_app_deployment_resource`;
CREATE TABLE `act_app_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_APP_RSRC_DPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_APP_RSRC_DPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_app_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_casedef
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_casedef`;
CREATE TABLE `act_cmmn_casedef`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `VERSION_` int(0) NOT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` bit(1) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_CASE_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_DEF_DPLY`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CASE_DEF_DPLY` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_cmmn_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_databasechangelog`;
CREATE TABLE `act_cmmn_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_databasechangelog
-- ----------------------------
INSERT INTO `act_cmmn_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:20', 1, 'EXECUTED', '8:8b4b922d90b05ff27483abefc9597aa6', 'createTable tableName=ACT_CMMN_DEPLOYMENT; createTable tableName=ACT_CMMN_DEPLOYMENT_RESOURCE; addForeignKeyConstraint baseTableName=ACT_CMMN_DEPLOYMENT_RESOURCE, constraintName=ACT_FK_CMMN_RSRC_DPL, referencedTableName=ACT_CMMN_DEPLOYMENT; create...', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:20', 2, 'EXECUTED', '8:65e39b3d385706bb261cbeffe7533cbe', 'addColumn tableName=ACT_CMMN_CASEDEF; addColumn tableName=ACT_CMMN_DEPLOYMENT_RESOURCE; addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:20', 3, 'EXECUTED', '8:c01f6e802b49436b4489040da3012359', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_CASE_INST; createIndex indexName=ACT_IDX_PLAN_ITEM_STAGE_INST, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableNam...', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:20', 4, 'EXECUTED', '8:e40d29cb79345b7fb5afd38a7f0ba8fc', 'createTable tableName=ACT_CMMN_HI_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_MIL_INST; addColumn tableName=ACT_CMMN_HI_MIL_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 5, 'EXECUTED', '8:70349de472f87368dcdec971a10311a0', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_CMMN_DEPLOYMENT; modifyDataType columnName=START_TIME_, tableName=ACT_CMMN_RU_CASE_INST; modifyDataType columnName=START_TIME_, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; modifyDataType columnName=T...', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('6', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 6, 'EXECUTED', '8:10e82e26a7fee94c32a92099c059c18c', 'createIndex indexName=ACT_IDX_CASE_DEF_UNIQ, tableName=ACT_CMMN_CASEDEF', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('7', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 7, 'EXECUTED', '8:530bc81a1e30618ccf4a2da1f7c6c043', 'renameColumn newColumnName=CREATE_TIME_, oldColumnName=START_TIME_, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; renameColumn newColumnName=CREATE_TIME_, oldColumnName=CREATED_TIME_, tableName=ACT_CMMN_HI_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_P...', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('8', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 8, 'EXECUTED', '8:e8c2eb1ce28bc301efe07e0e29757781', 'addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('9', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 9, 'EXECUTED', '8:4cb4782b9bdec5ced2a64c525aa7b3a0', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('10', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 10, 'EXECUTED', '8:341c16be247f5d17badc9809da8691f9', 'addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_RU_CASE_INST; createIndex indexName=ACT_IDX_CASE_INST_REF_ID_, tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE...', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('11', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 11, 'EXECUTED', '8:d7c4da9276bcfffbfb0ebfb25e3f7b05', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('12', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 12, 'EXECUTED', '8:adf4ecc45f2aa9a44a5626b02e1d6f98', 'addColumn tableName=ACT_CMMN_RU_CASE_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('13', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-02-01 23:21:21', 13, 'EXECUTED', '8:7550626f964ab5518464709408333ec1', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '3.8.0', NULL, NULL, '3728880083');

-- ----------------------------
-- Table structure for act_cmmn_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_databasechangeloglock`;
CREATE TABLE `act_cmmn_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_databasechangeloglock
-- ----------------------------
INSERT INTO `act_cmmn_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_cmmn_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_deployment`;
CREATE TABLE `act_cmmn_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_deployment_resource`;
CREATE TABLE `act_cmmn_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  `GENERATED_` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_CMMN_RSRC_DPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CMMN_RSRC_DPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_cmmn_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_hi_case_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_case_inst`;
CREATE TABLE `act_cmmn_hi_case_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_hi_mil_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_mil_inst`;
CREATE TABLE `act_cmmn_hi_mil_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_hi_plan_item_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_plan_item_inst`;
CREATE TABLE `act_cmmn_hi_plan_item_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STAGE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IS_STAGE_` bit(1) NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_AVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_ENABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_DISABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_STARTED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_SUSPENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `COMPLETED_TIME_` datetime(3) NULL DEFAULT NULL,
  `OCCURRED_TIME_` datetime(3) NULL DEFAULT NULL,
  `TERMINATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `EXIT_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `ENTRY_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EXIT_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SHOW_IN_OVERVIEW_` bit(1) NULL DEFAULT NULL,
  `EXTRA_VALUE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DERIVED_CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LAST_UNAVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_ru_case_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_case_inst`;
CREATE TABLE `act_cmmn_ru_case_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `LOCK_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COMPLETEABLE_` bit(1) NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_REF_ID_`(`REFERENCE_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CASE_INST_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_ru_mil_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_mil_inst`;
CREATE TABLE `act_cmmn_ru_mil_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_MIL_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_MIL_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MIL_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MIL_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_ru_plan_item_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_plan_item_inst`;
CREATE TABLE `act_cmmn_ru_plan_item_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STAGE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IS_STAGE_` bit(1) NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `ITEM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IS_COMPLETEABLE_` bit(1) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` bit(1) NULL DEFAULT NULL,
  `VAR_COUNT_` int(0) NULL DEFAULT NULL,
  `SENTRY_PART_INST_COUNT_` int(0) NULL DEFAULT NULL,
  `LAST_AVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_ENABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_DISABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_STARTED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_SUSPENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `COMPLETED_TIME_` datetime(3) NULL DEFAULT NULL,
  `OCCURRED_TIME_` datetime(3) NULL DEFAULT NULL,
  `TERMINATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `EXIT_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENTRY_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EXIT_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EXTRA_VALUE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DERIVED_CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LAST_UNAVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_STAGE_INST`(`STAGE_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PLAN_ITEM_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_PLAN_ITEM_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_cmmn_ru_sentry_part_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_sentry_part_inst`;
CREATE TABLE `act_cmmn_ru_sentry_part_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `REV_` int(0) NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PLAN_ITEM_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ON_PART_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `IF_PART_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_PLAN_ITEM`(`PLAN_ITEM_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SENTRY_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SENTRY_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SENTRY_PLAN_ITEM` FOREIGN KEY (`PLAN_ITEM_INST_ID_`) REFERENCES `act_cmmn_ru_plan_item_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_co_content_item
-- ----------------------------
DROP TABLE IF EXISTS `act_co_content_item`;
CREATE TABLE `act_co_content_item`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MIME_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TASK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTENT_STORE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTENT_STORE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `FIELD_` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTENT_AVAILABLE_` bit(1) NULL DEFAULT b'0',
  `CREATED_` timestamp(6) NULL DEFAULT NULL,
  `CREATED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LAST_MODIFIED_` timestamp(6) NULL DEFAULT NULL,
  `LAST_MODIFIED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTENT_SIZE_` bigint(0) NULL DEFAULT 0,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `idx_contitem_taskid`(`TASK_ID_`) USING BTREE,
  INDEX `idx_contitem_procid`(`PROC_INST_ID_`) USING BTREE,
  INDEX `idx_contitem_scope`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_co_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_co_databasechangelog`;
CREATE TABLE `act_co_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_co_databasechangelog
-- ----------------------------
INSERT INTO `act_co_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/content/db/liquibase/flowable-content-db-changelog.xml', '2022-02-01 23:21:19', 1, 'EXECUTED', '8:7644d7165cfe799200a2abdd3419e8b6', 'createTable tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_taskid, tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_procid, tableName=ACT_CO_CONTENT_ITEM', '', NULL, '3.8.0', NULL, NULL, '3728879796');
INSERT INTO `act_co_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/content/db/liquibase/flowable-content-db-changelog.xml', '2022-02-01 23:21:19', 2, 'EXECUTED', '8:fe7b11ac7dbbf9c43006b23bbab60bab', 'addColumn tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_scope, tableName=ACT_CO_CONTENT_ITEM', '', NULL, '3.8.0', NULL, NULL, '3728879796');

-- ----------------------------
-- Table structure for act_co_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_co_databasechangeloglock`;
CREATE TABLE `act_co_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_co_databasechangeloglock
-- ----------------------------
INSERT INTO `act_co_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_de_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_de_databasechangelog`;
CREATE TABLE `act_de_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_databasechangelog
-- ----------------------------
INSERT INTO `act_de_databasechangelog` VALUES ('1', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2022-02-01 23:21:24', 1, 'EXECUTED', '8:e70d1d9d3899a734296b2514ccc71501', 'createTable tableName=ACT_DE_MODEL; createIndex indexName=idx_proc_mod_created, tableName=ACT_DE_MODEL; createTable tableName=ACT_DE_MODEL_HISTORY; createIndex indexName=idx_proc_mod_history_proc, tableName=ACT_DE_MODEL_HISTORY; createTable tableN...', '', NULL, '3.8.0', NULL, NULL, '3728884111');
INSERT INTO `act_de_databasechangelog` VALUES ('3', 'flowable', 'META-INF/liquibase/flowable-modeler-app-db-changelog.xml', '2022-02-01 23:21:24', 2, 'EXECUTED', '8:3a9143bef2e45f2316231cc1369138b6', 'addColumn tableName=ACT_DE_MODEL; addColumn tableName=ACT_DE_MODEL_HISTORY', '', NULL, '3.8.0', NULL, NULL, '3728884111');

-- ----------------------------
-- Table structure for act_de_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_de_databasechangeloglock`;
CREATE TABLE `act_de_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_databasechangeloglock
-- ----------------------------
INSERT INTO `act_de_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_de_model
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model`;
CREATE TABLE `act_de_model`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `version` int(0) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `thumbnail` longblob NULL,
  `model_type` int(0) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_created`(`created_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_model
-- ----------------------------
INSERT INTO `act_de_model` VALUES ('121c7c59-868d-11ec-8361-dc41a90b0909', '请假流程', 'holidayRequest', '员工请假流程', NULL, '2022-02-05 22:08:18.868000', 'admin', '2022-02-05 22:08:18.868000', 'admin', 1, '{\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"},\"properties\":{\"process_id\":\"holidayRequest\",\"name\":\"请假流程\",\"documentation\":\"员工请假流程\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"iseagerexecutionfetch\":false,\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[],\"escalationdefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130.0,\"y\":205.0},\"upperLeft\":{\"x\":100.0,\"y\":175.0}},\"resourceId\":\"startEvent\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"startEvent\",\"name\":\"启动事件\",\"interrupting\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000012\"}]},{\"bounds\":{\"lowerRight\":{\"x\":315.4,\"y\":230.0},\"upperLeft\":{\"x\":215.39999999999998,\"y\":150.0}},\"resourceId\":\"approveTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"approveTask\",\"name\":\"ApproveRequest\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"managers\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\"}]},{\"bounds\":{\"lowerRight\":{\"x\":400.4,\"y\":210.0},\"upperLeft\":{\"x\":360.4,\"y\":170.0}},\"resourceId\":\"decision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"decision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\"},{\"resourceId\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\"}]},{\"bounds\":{\"lowerRight\":{\"x\":520.0,\"y\":140.0},\"upperLeft\":{\"x\":420.0,\"y\":60.0}},\"resourceId\":\"externalSystemCall\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"externalSystemCall\",\"name\":\"SuccessService\",\"servicetaskclass\":\"com.example.demo.delegate.CallExternalSystemDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000011\"}]},{\"bounds\":{\"lowerRight\":{\"x\":520.0,\"y\":305.0},\"upperLeft\":{\"x\":420.0,\"y\":225.0}},\"resourceId\":\"sendRejectionMail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendRejectionMail\",\"name\":\"faileService\",\"servicetaskclass\":\"com.example.demo.delegate.SendRejectionMail\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00009\"}]},{\"bounds\":{\"lowerRight\":{\"x\":665.0,\"y\":140.0},\"upperLeft\":{\"x\":565.0,\"y\":60.0}},\"resourceId\":\"firstApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"firstApprovedTask\",\"name\":\"firstApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"first\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00004\"}]},{\"bounds\":{\"lowerRight\":{\"x\":750.0,\"y\":120.0},\"upperLeft\":{\"x\":710.0,\"y\":80.0}},\"resourceId\":\"firstDecision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"firstDecision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00005\"},{\"resourceId\":\"sequenceFlow-000013\"}]},{\"bounds\":{\"lowerRight\":{\"x\":895.0,\"y\":140.0},\"upperLeft\":{\"x\":795.0,\"y\":60.0}},\"resourceId\":\"secenedApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"secenedApprovedTask\",\"name\":\"secenedApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"secened\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00006\"}]},{\"bounds\":{\"lowerRight\":{\"x\":980.0,\"y\":120.0},\"upperLeft\":{\"x\":940.0,\"y\":80.0}},\"resourceId\":\"secenedDecision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"secenedDecision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00008\"},{\"resourceId\":\"sequenceFlow-00007\"}]},{\"bounds\":{\"lowerRight\":{\"x\":1120.0,\"y\":140.0},\"upperLeft\":{\"x\":1020.0,\"y\":60.0}},\"resourceId\":\"bossApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"bossApprovedTask\",\"name\":\"bossApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"boss\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000010\"}]},{\"bounds\":{\"lowerRight\":{\"x\":593.0,\"y\":279.0},\"upperLeft\":{\"x\":565.0,\"y\":251.0}},\"resourceId\":\"rejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"rejectEnd\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":1193.0,\"y\":114.0},\"upperLeft\":{\"x\":1165.0,\"y\":86.0}},\"resourceId\":\"approveEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"approveEnd\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000011\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"firstApprovedTask\"}],\"target\":{\"resourceId\":\"firstApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-000011\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00004\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"firstDecision\"}],\"target\":{\"resourceId\":\"firstDecision\"},\"properties\":{\"overrideid\":\"sequenceFlow-00004\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00005\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"secenedApprovedTask\"}],\"target\":{\"resourceId\":\"secenedApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-00005\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00008\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-00008\",\"name\":\"faile\",\"conditionsequenceflow\":\"${!approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00006\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"secenedDecision\"}],\"target\":{\"resourceId\":\"secenedDecision\"},\"properties\":{\"overrideid\":\"sequenceFlow-00006\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000010\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"approveEnd\"}],\"target\":{\"resourceId\":\"approveEnd\"},\"properties\":{\"overrideid\":\"sequenceFlow-000010\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00009\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"rejectEnd\"}],\"target\":{\"resourceId\":\"rejectEnd\"},\"properties\":{\"overrideid\":\"sequenceFlow-00009\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":380.0,\"y\":265.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\",\"name\":\"faile\",\"conditionsequenceflow\":\"${!approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":380.0,\"y\":100.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"externalSystemCall\"}],\"target\":{\"resourceId\":\"externalSystemCall\"},\"properties\":{\"overrideid\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"decision\"}],\"target\":{\"resourceId\":\"decision\"},\"properties\":{\"overrideid\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00007\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"bossApprovedTask\"}],\"target\":{\"resourceId\":\"bossApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-00007\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000012\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":15.0,\"y\":15.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"approveTask\"}],\"target\":{\"resourceId\":\"approveTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-000012\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000013\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-000013\",\"name\":\"faile\",\"conditionsequenceflow\":\"${approved}\"}}]}', 0x89504E470D0A1A0A0000000D49484452000001220000004C0806000000C937BA2800000C334944415478DAED9D0970946719C7C31194AA88B5A3D5F10051A9ADF7D4A3C8A9A83043A040B321176C02A1380848800402139771004B558220560986580471990A2A8423440896C3C8B480925614539196B3402009E198D7FFB3F32E13B681DDCD7EDFEEFB7DFBFFCD3CB3C9E4CBEE7BFEF779AFE74D49218410420821841042082184104288FB504A753D79F2A4FFE0C183B777EFDEAD76EDDA6589555757ABDADADA0BB01C37A42919CA9D792409431A4AD9963A357FD351B5F3D8EBEACAB566D5DADA6A899D3F7F5EEDDDBBF70A1ACE53D1A669DFBE7DEAC8D63901B32A3DB1A4C98E72973C9E3B77CEF27C9A964737D723B108F9B6121192CA5DBFBF41D5D69FB1BAC1B4E25BAC3EDA34D9D1416349931DE52E7994F4D8D4518DC9A39BEB915884B8CC7B8E9F0988D09C8D472C6F2C62682C37A34D53B0839ED8FF7323D26447B907D362573E4DC9A39BEB9158848CB99B9AAF5BEE09853416156D9A82DFA2767D9B469BA676E8196BB9B7F586ECC8A705798C299F0EA947628A10D92540B1089169690A6198CCC5EA57D794BBD5F934358F5EAFB7E7B871E39E82F932333373D2D3D31FA61250889CD64987C22EC196E9D7112E15A298F369621E213C4F4280CEC3541BBB9A9191318E6A4021724A271D1AD22987BAA9935A9D4FD3F2A845E8AA884F7171B1AAACAC54252525770489624421724227BD5767744527B5239F26E5510FC7029E9008D0AD5BB75410F93DE8197198663D9DC23D8082FFB3498D45D2031B6C5A9AFAF4E95314A613063BE9B048DFD3C472D7E98F399F86D6E3DF456C66CF9E7DA3AD0805993B77EE4DF9BBCC19513A2C14A19031F03D2D5E8D25D2F4C42B4D91A665CC9831D2D055DFBE7DC33578994B514E2DF7603E251FC3870F7FDBFDF2E9E4B6555151A1DA63DDBA752DFA191FE5C342A0EC8DD9D9D93D3834B3244D014FA17BF7EE63ADF0884CCCE3C89123DFD5AF5FBF67060E1C7803C393CBF710A3A8F269984724AB646AD6AC5917C15D22D4D8D8A88A8A8ADEA447640328D45310A20F5188624F93CFE7EB3C64C890E7468D1A75BB1D3172EC1C1104E79D32410B5E805DC6CF7FE8DFBFFF92D4D4D4CB6E9B231A3C787057E4EF25119BBCBCBCDBE219F9FDFE80873479F2E45B7AB2FA25AFD7FB76AA87B51ED13FD0D01EA510C59626294334D28328CF9ADEBD7BE73A7DD52C2D2DED01E4C5834EB7498BCF56FC3E01F97C7712AC9A3D843C3F0FBB193A64C3DF3644F2C54DA2F7880EA0917D9542D4B134E15B321565B800760E36B9CDE4BFE3F6114164BAA3A38D85FD4EC4075685B691979595F59E0856CF5CB58F48F78D5E30AFAEDF29288B4F5331EC13A21D6868DFA610459F2634CC2FA2FC5E86FDE91EDF92C6EFACBE76ED9A2A2F2F0F7CD3233F9790979DF8791244E9C128E7C65CB7B39AC45788FC6880E914A2C8D32413B4E8AC4B5076672298B4B4E4AC99D5E273E0C001B57CF972959F9FAF0A0B0B65D8315986243124B5A7A9624B2172861095C326867BAEED29709BACA5A3A7EFE3992694553F58BD0C5F727373DF6777FD5895C7A6A62675E8D021B562C50A3571E244B570E142555555252132AE9B1461C094B645E22F443F81CD0CF75C6D6DED1BC1B83876D8E9D3A77F1B6DCC9878A609A2F30E94D372D869D8E878D54F2C796C6E6E567575756AE5CA956AD2A449CAE7F3A96DDBB6058285C552EE26E5D1AEB645E22F443ED8F7C33D575353336ACF9E3D97CF9E3DDB6CF5B7953494EAEAEAD7A28DA217AF34A17CBE013B095B1B66E2D672A2CDA388CFE1C387D5AA55AB02E2535A5AAAB66EDDAADAE9E81D2EF744E7311E6D8BC4998C8C8C42F18A227916953902DF2C75E2E65A155758BF577D471B8A9D69F2FBFDE3513EAB513E0D914CE8DB45B83CEED8B143AD5DBB36203A5EAF574D9F3E5D959595A9CD9B37DB56EEF1CE6322DA1689AF473451E68958127793999999269B3D21443F93DDC4A6A52F3D3DBD8BF6D47EA1B70E1CD45F2A1F66ED91BB70C2ED04B2622613AFACAD3B02249BD9D6C3FEE9F178069A9436D9B92D874461AB6067617F85CD86B7F651D61CB9274EB89D40861C10A3EDACAD807728E718DE803D2B9BFB4C111F1144A469A54EDB615831D2D79B354622C209B713C0037842765727733DE5E4E47C0065B019760CA2FC250392D409F5D21F69F9A9ACD2C9D926FC3E0FAF7DD8AB48D438E1760234F0C7E4BC59127B41F97A98E38397D12DC1E2F304C446C267FC0FAF4761F3E1B17E823D89C484136E27183060801CD63C95840224E78876C2EAB2B3B33F93C0747C0502F463BCFE578272417C4AF1FB23EC3DC4522132FD9689D4D454E5F1789A92A85A3AA1B37F57C281A2C317C9EA5302C4E7717CF652D916003BAEBDB147D96348320A51E0D474E7CE9DCB2044F21E696EAF0F08D027D1E9F789C9CF71FE6C3920FB43BD31F255D80F789A3B099180473366CC28CFCBCB6B0D063D2A2D2D3DB560C18231492844A17163BE991243A80AD3D17B6E8AC50B126F28258238DD56909595F5797CE662D8BF6027608BF0F99F636F4C62A64D9BF64B88CE0D59C5125A5A5A544D4D8D9A3A75EACDE2E2E2A79348882CBD65C2743004FA2C04E06F3ADC89EDFB6D64BE49BC1DEDF5FC5B9FD2FF027B2009E0F57A5B8222D416894B5B505070314984C892DB171CE2057583082C94153109EE65B3D83DA63FAB1EF61F3DFFF3387B1D790B321C6B0FB93244FE66F16719774DCAE8D1A3F7A5E8DB17C23CBA4C3FD7D3A9750DCFE7CB7A05EAF7B247C826F179440E0BCBD607BCBE86CFFA917C2E7B1AB92FF9F9F937240A7F280D0D0D6ACA94298DD15C35E2C4EB6EC4060D1AA4BA75EBA6DCEA11C96E681104BD0339C3EAF7D793DD0B648F8F3E87B64C87D9EDC41E4622A2A8A8E86599130AF586162D5AA4162F5EECE71C91B3E788E0990CD213C2BF8931D260A8B87D1CEF59A243C24A2CA2E51E8FE76B141FD2D186DABFA0A0A051E6894480C4135AB264899A3973E609BBAE064986BBD9138D9C8CD78740C543B124FD18627D4CAFB21DD6DED50AD80039F7C59E44AC70AD474C9830415633245CE6B9C2C2C21576DE4FE4847D442931DE3291E02F97617A7E6675C835371D99D7EB85F799233BAD2516B516B7C1141FE2789CB0B31AA6BA74E932DC49E52A5112214295B23C2E31796218767D04EF330BEF714862FA40889EC3EF5F4FC46E6B42925988527AF4E8F1203A61AB53BEF921166364AE0682512671A4A3FD7FB9F607FFFB3DBCC77EBDC171B5C7E3194AF121AEC509B713E84060171C300C7BBF5C7DA4F7EAF48B72D8F541D874D85F24AFB03512874976DBB39512D7E384DB09643F0CEC1593CB11C291ABE76D16CB9D62110EBB1EC6F35361B5B08BB00A783FC3E56656B64C925438E17602598A46277D31DC7389087B2BC328B9735D028345724C42EE19C3B3DFD11B4ADF84FD5A1628121C678890C463FAED04E8A8A3D061B7847B4E44A86C4B9D9ABFE9A8DA79EC7575E55AB32D616FC593912319F0D29ED613C8F3EF378492A1A57E76B75C998C9FD7E1E791917A4E841033863D728BC79A70CF8927242224C2B17E7F83AAAD3F634BD85BA4E58F109426084A1D5E3FD55E5AC68F1FFF5EB99F5D029B69F1D9007BD2CEAD1884101B91E060E8CCCF847B4E86637B8E9F0988D09C8D476C99F3AAAAAABA29FB82E4E6D9D0553C59B297B0AE12E81FAF97611B91F6B1A604B92784C426444B458CC23D2743C2A6E6EB967B42A15B11D2D2D21ED0E7B72462424FA46D0244679B161FBF5C7F24CFB0E608719710FD4A3C8D4884281E7BA2E49E2E588BC40FD2E2F302C4675C47F60C11429C3347B44526774D10A2EDDBB78B109D97552F99038AE4865599DCC6B33912F7193685E1570971A610BDA84F931BE11145B3DA253BA2B3B2B29A42C39DE8C9EB8758BB843847885E8557D1D714218A34DDE209054568E9D2A5CAEFF7AB8A8A0A89B6191023D97BC45534429C3347744196C3EFF78CECE3314D882036DEA008B54582DF496C72ED19E5B08609311C9FCFD7550F67CEEAA88372E1E0F312E55087C4C8C5EBB760AF9824447AE7F41A49BB7842A16CD8B021384CF3B19609710072F64A0E94CA6D17223AFA5C97AC5C3D2BA204AB96E312715C358BCACACBCBDF2244F0886ED32322C485183834EB05A1B9959F9FAF2E5DBA7447842A2B2B83227555E69158738450886C8DAD2427EA45744A4A4A6EC8706CDEBC79B7DBAC9CD11B22844264BF10C912BD3E99DF76B876952244088528AED12665454F765ECBC4B408108763845088121AF69610E2729C10F69610E2729C10F69610E2729C10F696109204981EF6961042082184104208218410420821841042082184104208218410D286FF0346BBDE27F351DDA00000000049454E44AE426082, 0, '');
INSERT INTO `act_de_model` VALUES ('3d05e78d-842b-11ec-88f2-dc41a90b0909', 'Request Resource Approval ', 'requestResourceApprovalProcess', NULL, NULL, '2022-02-02 21:22:58.266000', 'admin', '2022-02-02 21:22:58.266000', 'admin', 1, '{\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"},\"properties\":{\"process_id\":\"requestResourceApprovalProcess\",\"name\":\"Request Resource Approval \",\"process_namespace\":\"http://www.activiti.org/test\",\"iseagerexecutionfetch\":false,\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[],\"escalationdefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":80.0,\"y\":153.0},\"upperLeft\":{\"x\":45.0,\"y\":118.0}},\"resourceId\":\"starter\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"starter\",\"name\":\"Starter\",\"interrupting\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow12\"}]},{\"bounds\":{\"lowerRight\":{\"x\":511.0,\"y\":301.0},\"upperLeft\":{\"x\":340.0,\"y\":230.0}},\"resourceId\":\"sendJuniorRejectEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendJuniorRejectEmail\",\"name\":\"发送初级审批拒绝邮件\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendJuniorRejectionMailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow5\"}]},{\"bounds\":{\"lowerRight\":{\"x\":443.0,\"y\":420.0},\"upperLeft\":{\"x\":408.0,\"y\":385.0}},\"resourceId\":\"juniorRejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"juniorRejectEnd\",\"name\":\"Junior Reject End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow5\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":85.5,\"y\":35.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"juniorRejectEnd\"}],\"target\":{\"resourceId\":\"juniorRejectEnd\"},\"properties\":{\"overrideid\":\"flow5\"}},{\"bounds\":{\"lowerRight\":{\"x\":696.0,\"y\":173.0},\"upperLeft\":{\"x\":575.0,\"y\":95.0}},\"resourceId\":\"seniorApproval\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"seniorApproval\",\"name\":\"高级审批\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${seniorAdmin}\"}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow7\"}]},{\"bounds\":{\"lowerRight\":{\"x\":285.0,\"y\":176.0},\"upperLeft\":{\"x\":170.0,\"y\":95.0}},\"resourceId\":\"juniorApproval\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"juniorApproval\",\"name\":\"初级审批\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${juniorAdmin}\"}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow11\"}]},{\"bounds\":{\"lowerRight\":{\"x\":445.0,\"y\":153.0},\"upperLeft\":{\"x\":405.0,\"y\":113.0}},\"resourceId\":\"exclusivegateway1\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway1\",\"name\":\"Exclusive Gateway1\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"juniorSuccessFlow\"},{\"resourceId\":\"juniorRejectFlow\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"juniorSuccessFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":60.5,\"y\":39.0}],\"outgoing\":[{\"resourceId\":\"seniorApproval\"}],\"target\":{\"resourceId\":\"seniorApproval\"},\"properties\":{\"overrideid\":\"juniorSuccessFlow\",\"name\":\"同意\",\"conditionsequenceflow\":\"${approved==\'Y\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"juniorRejectFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":85.5,\"y\":35.5}],\"outgoing\":[{\"resourceId\":\"sendJuniorRejectEmail\"}],\"target\":{\"resourceId\":\"sendJuniorRejectEmail\"},\"properties\":{\"overrideid\":\"juniorRejectFlow\",\"name\":\"拒绝\",\"conditionsequenceflow\":\"${approved==\'N\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":805.0,\"y\":155.0},\"upperLeft\":{\"x\":765.0,\"y\":115.0}},\"resourceId\":\"exclusivegateway2\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway2\",\"name\":\"Exclusive Gateway2\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"seniorSuccessFlow\"},{\"resourceId\":\"seniorRejectFlow\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow7\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":60.5,\"y\":39.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway2\"}],\"target\":{\"resourceId\":\"exclusivegateway2\"},\"properties\":{\"overrideid\":\"flow7\"}},{\"bounds\":{\"lowerRight\":{\"x\":1175.0,\"y\":152.0},\"upperLeft\":{\"x\":1140.0,\"y\":117.0}},\"resourceId\":\"approvalSuccessEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"approvalSuccessEnd\",\"name\":\"Approval Success End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"seniorSuccessFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":70.5,\"y\":37.5}],\"outgoing\":[{\"resourceId\":\"sendApprovalSuccessEmail\"}],\"target\":{\"resourceId\":\"sendApprovalSuccessEmail\"},\"properties\":{\"overrideid\":\"seniorSuccessFlow\",\"name\":\"同意\",\"conditionsequenceflow\":\"${approved==\'Y\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":882.0,\"y\":301.0},\"upperLeft\":{\"x\":690.0,\"y\":230.0}},\"resourceId\":\"sendSeniorRejectEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendSeniorRejectEmail\",\"name\":\"发送高级审批拒绝邮件\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendSeniorRejectionMailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow9\"}]},{\"bounds\":{\"lowerRight\":{\"x\":803.0,\"y\":420.0},\"upperLeft\":{\"x\":768.0,\"y\":385.0}},\"resourceId\":\"seniorRejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"seniorRejectEnd\",\"name\":\"Senior Reject End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"seniorRejectFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":96.0,\"y\":35.5}],\"outgoing\":[{\"resourceId\":\"sendSeniorRejectEmail\"}],\"target\":{\"resourceId\":\"sendSeniorRejectEmail\"},\"properties\":{\"overrideid\":\"seniorRejectFlow\",\"name\":\"拒绝\",\"conditionsequenceflow\":\"${approved==\'N\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow9\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":96.0,\"y\":35.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"seniorRejectEnd\"}],\"target\":{\"resourceId\":\"seniorRejectEnd\"},\"properties\":{\"overrideid\":\"flow9\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow11\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":57.5,\"y\":40.5},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway1\"}],\"target\":{\"resourceId\":\"exclusivegateway1\"},\"properties\":{\"overrideid\":\"flow11\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow12\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":17.5,\"y\":17.5},{\"x\":57.5,\"y\":40.5}],\"outgoing\":[{\"resourceId\":\"juniorApproval\"}],\"target\":{\"resourceId\":\"juniorApproval\"},\"properties\":{\"overrideid\":\"flow12\"}},{\"bounds\":{\"lowerRight\":{\"x\":1061.0,\"y\":171.0},\"upperLeft\":{\"x\":920.0,\"y\":96.0}},\"resourceId\":\"sendApprovalSuccessEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendApprovalSuccessEmail\",\"name\":\"发送审批通过邮件\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendApprovalSuccessEmailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow13\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow13\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":70.5,\"y\":37.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"approvalSuccessEnd\"}],\"target\":{\"resourceId\":\"approvalSuccessEnd\"},\"properties\":{\"overrideid\":\"flow13\"}}]}', 0x89504E470D0A1A0A0000000D4948445200000130000000620806000000D7CA8FBB000007CD4944415478DAED9DCD6B535918C65DF40FE862162E5CB870E99F20D885888B8220A6316DD5EAA2A45067A7B8112A161C281869C12E069C41066B8C1411A60B4DBF524602CE2045B1D8A1357688FD72A6566D3A4D53CF9CB7DC486D8DD38FDC9B734F7E3F3804ED25F7F679FB3CE7E3DE9CECDA050000000000000000000000B059945215E3E3E3B16432B9D2DBDBAB1E3D7AE44A8BC7E32A9148BCD5AD1ED5CDAA4DB9D6C74B7DD1DD25A4804343436AF8D7F3AB6D6969C9B5363B3BAB060707E775D18EA3FCE66B333333437D5CD4F766FF4B75A9FBB97AF8EC8D9AFF98715563742F32D2FB786510A7584BBAE71941F9CDD74674A33EEEE92BE125BFFBEDC729951899F23CC0F0C50E90A173DE207F3EEEF4A458BA50CB86C95069726DA88FBBFA0EBC985A0DAFF3D1E1928457B9E95E54641E9EEFDDBDEAE5E59C0649704496439C57E36AB376F465517D2A4DD27721F36FC9465E5EE91E0804769F3871A241B716DDC2BAEDB526C06C2AD41639A4DB9C6E11E7B5BADC6BE3417D8CE9304AA5AFD7BAEBB03A1E0C06E7F4AB5AD396756BABAAAAAAD8F10942A1D077E170F8A7B367CFBE97376F6C6C5CBE70E1C2ABCB972F371160AE875775817F1360967718E510603A5B0E3861A5AE5FBF9E8946A3AAB5B535575B5BFBC909B2D88E424CC2EBD4A9537FDDBB776F696E6E4EE57239353939A97A7A7A545353D38A6E3D45494902EC5BE1B5CBC410B32CC08CEB30CA24C07E97A08AC5626A2D6363634A0F9A969D106BDBF609EAEBEB3BEEDEBDFB417D85C5C54575F1E2C58C4ECBEF09B0A24E61BE659CBCB18E1060767718B607582010D82701A5834A65B3D90DF92221A6B365C519A16D6F4DECE4C993B332F22AC4E8E8A8D2C7BC21C076BC0ED07FF8F0E16A67FD25F23F87479CE34AB6D82CD77BEBD62DDF1A49AE5FB72A533B0CB93E9B03CCD1FF0709B0482452305FAE5CB9B2E88CC2C2DB3DD157D3318FFC4C8E71A380F20756AA005BB7A0E8593B78F0A0DAACA14A758DEB5BA98CB4D3EB3E76EC98DA6C87510A5D4D0C30377ECFAEAEAE82F9226B62CE712DDB0A92D3A74FBF9B9898287802F9996D45640DACACA690C64ED96D9F423A8F4DA8F6F6F682F9A247677FEF6804A6A787BD0F1E3C582E7482CECECE257D4C2B5348EE42B20646806D31C0F64A38D5D5D5A9542AF545AEC8CDC2FBF7EFE7D7C03EE863F66CF72EC191868686F70B0B0B1BC22B994CAA3367CEBCD53FAF24C0ECBFADCF5D4802CC85A5A2CE7C88C9484CA68CF22A0BFBF95998CEA0FA1D9FA4B1B1717C787878349BCD2EA7D36939C93F3AB866DC7E62B6CC032C3FC531FA497C9E0323C0B68B3C82150C067FFCDAF291FEFFA7353535878A7222FD66CDFA4D5FC91BEB61DD842CAAB939F222C0BEA0D28D1B250498D91D46B93C89BF663AF9F9A3443A6FF6BBF97CA9D5453430C0769918606B3FCCED615B74FB43C5A6685D227D4BA6BB9594A0884616CAC4004B241293F9ED74BC6AE974FA8EDBDBBA98A27529F42DA5EE56E275114D2D948901D6D7D777746060E0DDF4F474C68B8E456A138FC75FBBBDB19E295A7BAC6FC975B7120F8B6874A14C0C30416B55AD03FF898C5A5DDEDE58DE7FC48BDA98A4B587FA965C776BF1A8884617CAD400B311B406C054680D00980AAD013015A03500A6426B00C054680D80A900AD013015A03500A6426B00C054680D80A900AD0130155A0300A6426B80F231543FA6426B0057514A558C8F8FC792C9E48A6CC458AC9D32E40B64F35F31B793168FC755229178AB5B3D3A7BDF6CD11F2C454C353434A4DCD880B1585F203B3B3BAB060707E7FDBCBF939B3ABBDD6CD01F2C45460462AA5F7E7BA52E753F570F9FBD51F31F33269A68C9CFDB038BCE37FB5F1AADB1CDFA83A5E4F7EF1763C9EBEDC72995189932D2447EFE8206D1D90F1ADBAA3F584AFE1B94065E4CAD1AEB7C74D86403293FEBEC078D6DD51F2C0FB085CCBFC68F0AFC1E607ED09800035F0618064267020C30160622C00030160186FE0004183AA33F106018880023C0006311600418000186CEE80F0418064267020C30160622C0003C25FF59481FB445BF7F16D2E701B6C86721C1381289C4A41FB67849A7D377FCBC1B825F74B6557FB094BEBEBEA3030303EFA6A7A733A6F6FC629E783CFEDACFFB51F94067ABF5078BD17F98D5BA777D225304037705956B1AB1C13C86EB6CBDFE005B827DDAD11B0043017A036028F406000C85DE00180AD01B0043017A036028F406000C85DE00180AD01B0043A137006028F406C05080DE00180AD01B0043A137006028F406C05080DE00180ABD010043A137008602F40630DD4CFD180ABD01FC66A4BDBA35E8F6B3182A180CEEAFAAAAAA4019F4063016318D3650A798687DD3A67A5A5353730895D01BC0D491C0AA99EAEAEA547B7BBB8A46A32A1289A87038FCD958A150A81EA5D01BC0C469CCAA9952A9945A4B2E9753DDDDDDAAB6B676451FF3411FB307C5D01BC02443C91ACCEA48A0101D1D1D196764104631F7F5BE76EDDA2C7A036CCE502D6296AEAEAE828692298E63A81614436F00E34604B2065388AB57AF7E6244E09DDEADADAD4BE80DB0090281C03E318B2C2067B3D90D661A1B1B93351909B06559BF4131F406300A6D983FC454B1586C8399CE9D3B979FCEB4A194FB7A373737AFA037C016088542079C1E5FDDB8716345D660F4B4313F121033C578C0B2B87AEB96436F8022A14D733C180CCEAD7BB05242AD0D33A13780F1040281DDCE22B3DC290BB30683DE00000000000000000000000000000000000000B6F21FE0CC2E76923202150000000049454E44AE426082, 0, '');
INSERT INTO `act_de_model` VALUES ('9ed1f7c5-8377-11ec-ba42-dc41a90b0909', '审批表单', 'key-sp', '', NULL, '2022-02-01 23:57:13.055000', 'admin', '2022-02-02 00:01:34.922000', 'admin', 1, '{\"name\":\"审批表单\",\"key\":\"key-sp\",\"version\":0,\"fields\":[{\"fieldType\":\"FormField\",\"id\":\"意见\",\"name\":\"意见\",\"type\":\"text\",\"value\":null,\"required\":true,\"readOnly\":false,\"overrideId\":false,\"placeholder\":\"\",\"layout\":null}],\"outcomes\":[]}', 0x89504E470D0A1A0A0000000D494844520000012C0000006C0806000000D0528FAD000000017352474200AECE1CE9000004E149444154785EEDDDBB4A5C7D14C6E1FF288A073C2136166261A35720D80982F72236168257E00DD87B0176626725D88897216861A1280A1ED0B0877C1F1631827B66F0CD7E0684348E6B9EB5F8914C8C693D3D3DBD170F0204080408B4042B604B462440A02D20580E8100811801C18A5995410910102C374080408C8060C5ACCAA0040808961B20402046E0CB60BDBFBF97EAC383000102BD1068B55AA5FAF8D3E3CB605D5F5F979999995ECCE96B102040A0DCDCDC94A9A929C1720B0408FC7C01C1FAF93B32210102BF056A05EBF6F6B64C4E4EC2244080404F046A05EBEEEEAE8C8F8FB7073D393929575757ED37C4E6E6E64AF5FED6C2C242595A5A2A2F2F2F656F6FAF6C6C6C94C1C1C19EBC305F8400817F4FE0FEFEBE8C8D8D7DEF3DAC8FC13A3B3B2B8B8B8BED277A7D7D2DA7A7A7E5E2E2A21D290F0204087442A063C13A3A3A2ACBCBCBFFFF6EABFAC5FAFA7A191D1D2DC7C7C7656D6DAD13F37A0E02041A2CD0B16035D8D04B2740A0470282D523685F860081FA021D0BD6C3C343E9EFEFAF3F91678812F8ECBB8EBBF522FCCB8A6EC9FEECE71D1A1A6A0FD8B16055873B3030F0B35FB5E908108814787E7E16ACC8CD199A40030504AB814BF79209A40A0856EAE6CC4DA081025D0DD6EEEE6ED9D9D929979797EDEF841F191969FF7A7878B84C4F4F3790DB4B2640A08E40D783B5BABA5A0E0E0ECAE6E666D9DFDF2FDBDBDBA57AA7BFAFAFAFCEDC3E970081060A743D585B5B5BE5F0F0B0CCCECE96F9F9F9F6CFCD3A3F3F2F2B2B2B0DE4F6920910A823D091607DFC017EBEADA1CE3A7C2E01027F13F82F58B57E5AC3C7603D3E3E12274080405704AAF7C1AB47C782D595293D290102043E08D40ED6C4C404D04F04FC0583D3F88EC0DBDBDB773EAD119F53FD49EEB31F1AFAE57F42D108212F9200810801C18A5893210910A80404CB1D10201023205831AB3228010282E5060810881110AC985519940001C172030408C4080856CCAA0C4A808060B90102046204042B665506254040B0DC00010231028215B32A83122020586E8000811801C18A5995410910102C374080408C8060C5ACCAA0040808961B2040204640B062566550020404CB0D10201023205831AB3228010282E5060810881110AC985519940001C172030408C4080856CCAA0C4A808060B90102046204042B665506254040B0DC00010231028215B32A83122020586E8000811801C18A5995410910102C374080408C8060C5ACCAA0040808961B2040204640B062566550020404CB0D10201023205831AB3228010282E5060810881110AC985519940001C172030408C4080856CCAA0C4A808060B90102046204042B665506254040B0DC00010231028215B32A83122020586E8000811801C18A5995410910102C374080408C8060C5ACCAA0040808961B2040204640B062566550020404CB0D10201023205831AB3228010282E5060810881110AC985519940001C172030408C4080856CCAA0C4A808060B90102046204042B665506254040B0DC00010231028215B32A83122020586E8000811801C18A5995410910102C374080408C8060C5ACCAA0040808961B2040204640B062566550020404CB0D10201023205831AB3228010282E5060810881110AC985519940001C172030408C4080856CCAA0C4A808060B90102046204042B665506254040B0DC00010231028215B32A83122020586E8000811801C18A5995410910102C374080408CC02F77E68E434994722D0000000049454E44AE426082, 2, '');
INSERT INTO `act_de_model` VALUES ('bcbc96ec-840d-11ec-88f2-dc41a90b0909', '报销', 'key-bx', '', NULL, '2022-02-02 17:51:47.598000', 'admin', '2022-02-02 20:36:57.514000', 'admin', 1, '{\"modelId\":\"bcbc96ec-840d-11ec-88f2-dc41a90b0909\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"key-bx\",\"name\":\"报销\",\"documentation\":\"\",\"process_author\":\"sxy\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"escalationdefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"开始\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formfieldvalidation\":true,\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3\",\"properties\":{\"overrideid\":\"\",\"name\":\"人事\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${hrUserId}\"}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-81DD1ECE-1C16-4E97-A948-0A4F3C0DEBB6\"}],\"bounds\":{\"lowerRight\":{\"x\":275,\"y\":218},\"upperLeft\":{\"x\":175,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\"},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-409E4567-B4ED-47E8-87A5-2135A3F108A3\"},{\"resourceId\":\"sid-9F3C21D4-0160-43D1-9423-9F855EACEE13\"}],\"bounds\":{\"lowerRight\":{\"x\":360,\"y\":198},\"upperLeft\":{\"x\":320,\"y\":158}},\"dockers\":[]},{\"resourceId\":\"sid-81DD1ECE-1C16-4E97-A948-0A4F3C0DEBB6\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A\"}],\"bounds\":{\"lowerRight\":{\"x\":319.64844687000755,\"y\":178.4097335362338},\"upperLeft\":{\"x\":275.62889687999245,\"y\":178.2191727137662}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20.5,\"y\":20.5}],\"target\":{\"resourceId\":\"sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A\"}},{\"resourceId\":\"sid-60D5E385-71A9-4CC5-BFAE-E302EFF04BA9\",\"properties\":{\"overrideid\":\"\",\"name\":\"总经理\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"idm\",\"idm\":{\"type\":\"user\",\"assignee\":{\"id\":\"admin\",\"firstName\":\"Test\",\"lastName\":\"Administrator\",\"email\":\"test-admin@example-domain.tld\",\"fullName\":\"Test Administrator\",\"tenantId\":null,\"groups\":[],\"privileges\":[],\"$$hashKey\":\"object:13067\"}}}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-DF045EE9-B8D6-4593-B1D4-F03F0965B8F0\"}],\"bounds\":{\"lowerRight\":{\"x\":529,\"y\":218},\"upperLeft\":{\"x\":429,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-7DEAB731-9140-4527-A905-3F9002166154\",\"properties\":{\"overrideid\":\"\",\"name\":\"财务\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${financeUserId}\"}},\"formkeydefinition\":\"\",\"formreference\":\"\",\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-F969DB3D-8B6E-4DA4-806A-D6410A413578\"}],\"bounds\":{\"lowerRight\":{\"x\":390,\"y\":395},\"upperLeft\":{\"x\":290,\"y\":315}},\"dockers\":[]},{\"resourceId\":\"sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3\"}],\"bounds\":{\"lowerRight\":{\"x\":174.15625,\"y\":178},\"upperLeft\":{\"x\":130.609375,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3\"}},{\"resourceId\":\"sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6\",\"properties\":{\"overrideid\":\"\",\"name\":\"结束\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":493,\"y\":369},\"upperLeft\":{\"x\":465,\"y\":341}},\"dockers\":[]},{\"resourceId\":\"sid-DF045EE9-B8D6-4593-B1D4-F03F0965B8F0\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6\"}],\"bounds\":{\"lowerRight\":{\"x\":479,\"y\":340.171875},\"upperLeft\":{\"x\":479,\"y\":218.41015625}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6\"}},{\"resourceId\":\"sid-F969DB3D-8B6E-4DA4-806A-D6410A413578\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\",\"showdiamondmarker\":false},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6\"}],\"bounds\":{\"lowerRight\":{\"x\":464.96875,\"y\":355},\"upperLeft\":{\"x\":390.953125,\"y\":355}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6\"}},{\"resourceId\":\"sid-9F3C21D4-0160-43D1-9423-9F855EACEE13\",\"properties\":{\"overrideid\":\"\",\"name\":\"小于1000\",\"documentation\":\"\",\"conditionsequenceflow\":{\"expression\":{\"type\":\"static\",\"staticValue\":\"${money<1000}\"}},\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-7DEAB731-9140-4527-A905-3F9002166154\"}],\"bounds\":{\"lowerRight\":{\"x\":340.4424796501771,\"y\":314.0117227625271},\"upperLeft\":{\"x\":340.1161140998229,\"y\":198.8046834874729}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-7DEAB731-9140-4527-A905-3F9002166154\"}},{\"resourceId\":\"sid-409E4567-B4ED-47E8-87A5-2135A3F108A3\",\"properties\":{\"overrideid\":\"\",\"name\":\"大于1000\",\"documentation\":\"\",\"conditionsequenceflow\":{\"expression\":{\"type\":\"static\",\"staticValue\":\"${money>1000}\"}},\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-60D5E385-71A9-4CC5-BFAE-E302EFF04BA9\"}],\"bounds\":{\"lowerRight\":{\"x\":428.2265690163773,\"y\":178.42803054022158},\"upperLeft\":{\"x\":360.4355403586227,\"y\":178.18329758477842}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-60D5E385-71A9-4CC5-BFAE-E302EFF04BA9\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D4948445200000103000000A10806000000C372DA9A000009754944415478DAED9DCD6B545718875DB870E1C245FF04175D14BA71E942D1858B802E1C93CC874C349128A682264AA2A244416A77556877512A1209B8300885B913920969A7041425434431D5EAD466623169AA719289A7E7B57365128C7333998973CE7D1E38A453358DEFFBBBCF9C73EEB9D3356B000000000000000000006C4529B5766C6CAC27994CCEC7E371158BC58C1F8EE3A84422F1528F301D263FE4C723D2C8C1C14195C96454369BB5664C4C4CA881818129DDD8DD5CB2E487FC78408C6E5B230B1A9AD5A61FE592253FE4C70332B5FB5821C686BB54CAE97CFFD5E486EA66CE71C9921FF2E30159232D2EC0B3D42D75EF76DB87F13CD56B723315972CF9213F2536732476764133E535CD04F2E3C366CAF4AEB099F29A6602F9F161331F0D5D5ED04C794D3381FCF8B099320A9B69F8061032203FE4A7D4663E1CBAB4A099F29A6602F9F15133A75FA5173471F1905FA799656103F9213F55DDCC4F35D2D4295F153673931EF233D5901FF263C49ACF965165CDDCACC7841E3FE9F14A8F1DE487FCD04CFF35D315415DFEF58EBC106AC88FCFF2535757B7A9A1A1E1BBC6C6C647E17038AB5F2BFDF56D341A7DA8FFF95C7D7DFD9734D35A196CCA8B60F1432FDB4D1702F9590681406063241219D21278DDDDDD3D77FFFE7D353333A384D9D959954AA5D4952B575E8642A1193DBEDDB265CB5A9A69950C36E72FF8A51E87DD6EF29281FC7844BFDBEFD222F8FBC68D1B5957004B214F7E9D3C797222180CC6575308347355445057E4F719BB64203F1E9705FA9D7E72646424AB3C92CBE5547B7BFBB8CC1068A6F1327045E0F5797823970CE4A708D168749D7E877FE838CEA45A2632430887C36F566B0F816656748FA06E997FCEB82503F929426D6D6DDD912347FE9077FA52B87EFDFA5C5353D3F734D34819B8770D4AFDC82CA3960CE4A7F812A1E7CE9D3B7FA91279F0E0813A70E0C0339A699C0C44042F96B134307E86407E8A6F1C4E15DB30FC14729721128994FD1356F48CE5AE1655732010584F33CB5BC7152C0D8ACD10AA4608E4A7B499815A29F23D2A70D641E5C7B41E5DB2C94933575EC76DDBB66D5DF3FF11E39E32FFA74EE5BF6F553CCB407E4A20180C4EAF7466B077EFDE5C41F12B36FCD0CCD5A8E3D6AD5B5599DFC9DDBD87E86AFCFCE4A77277137E96C3452BD93368696999AAC4344F8FC3FAE7DBC034AFBC752CF3E6DFE6322F39C8CF67DC33683C7FFEFC74A932B879F3A63A73E64C8C0D2023EF26ACF4BCC0524797D9403454065F687BFEF3F8F1E3929608070F1E9C6F6B6BDB41338D3D6750EADD80624797918189F9D9BF7FFF898E8E8E59B9B897436F6FAF3A74E8D0EF34D3F81388CB5D32783DBA8C0C4CCC4F7373F32F7AB930EF5508B257D0D0D0F076DFBE7D5FD34C2B9E4DF0BA6458EED16564605A7EE481232D84219921145B32249349A56713FF6A19D4D34CAB9E5A2CB66428F7F9046450CDF9696C6CBCA8DFEDDF5CBC78F19DDC6570670AF25566039D9D9DAFB5045EECD9B3673BCDB4F2F30C965A32ACF4E8323230313FB2A9A86709DFB4B4B4FC16894466DC0F37696A6AFA35140A1D5A749A8D66DAF749478B6708E53ABA8C0CF8D8339A696033DD19C229939706E40719D0CCF209417EA628F9213F3493666EA8C4B327E40719D04C039B890CC80FCDA499C880FCD04C9A890CC80FCDA499C880FCD04C9A890CC80FCDA499C880FCD04C9A890CC88F61C4E3719B9B39A39B39870CC88F6DF9A9088944E285FC8F5B6C6C663A9DEED6CD1C4506E4C7B6FC5484BEBEBE9DFDFDFD93E3E3E36F6C32BA34D2719CA77AEC4606E4C7B6FC540CFD17AED1061C962991AC912C18F2F718ADE646DA2203F203800CA81D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A0740A0A91D0081A67600049ADA0110686A47ED804003B503020DD40E0834503B20D040ED804003B503020DD40E0834503B20D040ED804003B503020DD40E0834503B00024DED000834B50320D0D40E8040533B00024DED000834B50320D0D40E8040533B00024DED000834B50320D0D40E60F9411E9630CBA8ADADBD4B459001F837C85157067A34531164003E251008ACD733825712E86834BA818A2003F077987F20D0C800AA10A5D4DAB1B1B19E6432391F8FC7552C16337E388EA31289C44B3DC2C800C0232282C1C14195C96454369BB5664C4C4CA8818181292D86DDC800C0033223B04D040542C8EA99C2283200F0802C0D3E76218D0D77A994D3F9FEABC942D032984306001E9035F6E20BE859EA96BA77BBEDC3789EEA3559060A190094288391D8D9053290D7C80019800F6520CB834219C86B64800CC0873278347479810CE435324006E04319C8289481E11B88C800A054193C1CBAB44006F21A192003F0910CA65FA5174860F1905F47069F4D023CF109AB27834F89C0D425834532E0894FF83C7B06B60C5B64C0139F800C9041E1EC80273E0119F855067A16B0AEBEBE7E879640AB9E19FC2832D0AF77C94C81F40232F0810C4402FAC23FABC764C15EC187A18530A5C705A400C8C06219E80B7CA3BEE01FB917FED1A34755575797EAE9E951D7AE5D53274E9C50A150E8FDAF0583C13FF58CE12B920CC8C03219E445302D17BA5CF44F9E3C511F239D4EABD3A74FBBB3841C42006460910CF24B83F73382AB57AFAA5C2EA78A21BFCF9D2170970190812532C8EF11A8E3C78FBF9B9D9D555EE9E8E87067081748342003C36520B302F71CC1524B83A5902583EC21C8A622B303400606CA408E15CB69C2FC81A21A11416B6BEB3B550205B3835DA41A9081613228B85538ED3E7B20770D4A41EE32E4BF572BA90664B0840C3E769FBE5A87DC3E2C05F973F9EF7196540332307099A0C76159E7CB3B3A330340066C20AE71F70C8E1D3B9663CF0090017713567437418E2E733701908145E70CDADADAE6BC1C3872293889C8390340067E3F81A8C7336605800C2C7D3641CF10725E9E4DD0638E67130019F8E0A945D954941980FBD4627B7BFBBB70383CEFCE0810012003FB3FCFE09CBBA9B878C8BF973D029606800C7CF44947F9DB8EADF90DC656B97D88040019F8FCFF9B00800C90010032400600C800190020036400800C90010032400600C800190020036400F0D989C7E336CB6046CB608E2E0378209148BCC8643256CA209D4E776B198CD265000FF4F5F5EDECEFEF9F1C1F1F7F63D38C4044E038CE533D76D365008FE80BA646BF830ECB945AD6D8160CF97B8C220200000000000000000000000000000000000000806AE63F3DA4A14442D9E7490000000049454E44AE426082, 0, '');
INSERT INTO `act_de_model` VALUES ('f73c50b4-8375-11ec-ba42-dc41a90b0909', '请假', 'key-1', '请假流程', NULL, '2022-02-01 23:45:22.026000', 'admin', '2022-02-02 21:40:32.188000', 'admin', 1, '{\"modelId\":\"f73c50b4-8375-11ec-ba42-dc41a90b0909\",\"bounds\":{\"lowerRight\":{\"x\":1200,\"y\":1050},\"upperLeft\":{\"x\":0,\"y\":0}},\"properties\":{\"process_id\":\"key-1\",\"name\":\"请假\",\"documentation\":\"请假流程\",\"process_author\":\"\",\"process_version\":\"\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"process_historylevel\":\"\",\"isexecutable\":true,\"dataproperties\":\"\",\"executionlisteners\":\"\",\"eventlisteners\":\"\",\"signaldefinitions\":\"\",\"messagedefinitions\":\"\",\"escalationdefinitions\":\"\",\"process_potentialstarteruser\":\"\",\"process_potentialstartergroup\":\"\",\"iseagerexecutionfetch\":\"false\"},\"childShapes\":[{\"resourceId\":\"startEvent1\",\"properties\":{\"overrideid\":\"\",\"name\":\"开始\",\"documentation\":\"\",\"executionlisteners\":\"\",\"initiator\":\"\",\"formkeydefinition\":\"\",\"formreference\":\"\",\"formfieldvalidation\":true,\"formproperties\":\"\"},\"stencil\":{\"id\":\"StartNoneEvent\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-3911B50D-991A-4F34-BB64-AC695A38035B\"}],\"bounds\":{\"lowerRight\":{\"x\":130,\"y\":193},\"upperLeft\":{\"x\":100,\"y\":163}},\"dockers\":[]},{\"resourceId\":\"sid-414A1C8A-A7C0-4C6B-96DF-827813026B84\",\"properties\":{\"overrideid\":\"\",\"name\":\"班长\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"idm\",\"idm\":{\"type\":\"user\",\"assignee\":{\"id\":\"yqmm\",\"firstName\":\"元气\",\"lastName\":\"满满\",\"email\":null,\"fullName\":\"元气 满满\",\"tenantId\":null,\"groups\":[],\"privileges\":[],\"$$hashKey\":\"object:2471\"}}}},\"formkeydefinition\":\"\",\"formreference\":{\"id\":\"9ed1f7c5-8377-11ec-ba42-dc41a90b0909\",\"name\":\"审批表单\",\"key\":\"key-sp\"},\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2\"}],\"bounds\":{\"lowerRight\":{\"x\":275,\"y\":218},\"upperLeft\":{\"x\":175,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-3911B50D-991A-4F34-BB64-AC695A38035B\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-414A1C8A-A7C0-4C6B-96DF-827813026B84\"}],\"bounds\":{\"lowerRight\":{\"x\":174.15625,\"y\":178},\"upperLeft\":{\"x\":130.609375,\"y\":178}},\"dockers\":[{\"x\":15,\"y\":15},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-414A1C8A-A7C0-4C6B-96DF-827813026B84\"}},{\"resourceId\":\"sid-369B9425-D4FE-4786-8668-8C3194A0382F\",\"properties\":{\"overrideid\":\"\",\"name\":\"老师\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"idm\",\"initiatorCanCompleteTask\":false,\"idm\":{\"type\":\"user\",\"assignee\":{\"id\":\"songxy\",\"firstName\":\"先阳\",\"lastName\":\"宋\",\"email\":null,\"fullName\":\"先阳 宋\",\"tenantId\":null,\"groups\":[],\"privileges\":[],\"$$hashKey\":\"object:1026\"}}}},\"formkeydefinition\":\"\",\"formreference\":{\"id\":\"9ed1f7c5-8377-11ec-ba42-dc41a90b0909\",\"name\":\"审批表单\",\"key\":\"key-sp\"},\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-F192A1C9-45C0-4847-80DD-9E0A73C69A87\"}],\"bounds\":{\"lowerRight\":{\"x\":420,\"y\":218},\"upperLeft\":{\"x\":320,\"y\":138}},\"dockers\":[]},{\"resourceId\":\"sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-369B9425-D4FE-4786-8668-8C3194A0382F\"}],\"bounds\":{\"lowerRight\":{\"x\":319.15625,\"y\":178},\"upperLeft\":{\"x\":275.84375,\"y\":178}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":50,\"y\":40}],\"target\":{\"resourceId\":\"sid-369B9425-D4FE-4786-8668-8C3194A0382F\"}},{\"resourceId\":\"sid-49C22E46-E64E-4A7A-82C4-236BBCA93544\",\"properties\":{\"overrideid\":\"\",\"name\":\"班主任\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"executionlisteners\":\"\",\"multiinstance_type\":\"None\",\"multiinstance_cardinality\":\"\",\"multiinstance_collection\":\"\",\"multiinstance_variable\":\"\",\"multiinstance_condition\":\"\",\"isforcompensation\":\"false\",\"usertaskassignment\":{\"assignment\":{\"type\":\"idm\",\"idm\":{\"type\":\"user\",\"assignee\":{\"id\":\"admin\",\"firstName\":\"Test\",\"lastName\":\"Administrator\",\"email\":\"test-admin@example-domain.tld\",\"fullName\":\"Test Administrator\",\"tenantId\":null,\"groups\":[],\"privileges\":[],\"$$hashKey\":\"object:810\"}}}},\"formkeydefinition\":\"\",\"formreference\":{\"id\":\"9ed1f7c5-8377-11ec-ba42-dc41a90b0909\",\"name\":\"审批表单\",\"key\":\"key-sp\"},\"formfieldvalidation\":true,\"duedatedefinition\":\"\",\"prioritydefinition\":\"\",\"formproperties\":\"\",\"tasklisteners\":\"\",\"skipexpression\":\"\",\"categorydefinition\":\"\",\"taskidvariablename\":\"\"},\"stencil\":{\"id\":\"UserTask\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-FD2BD278-ED8C-4770-9D5D-54817AF1A672\"}],\"bounds\":{\"lowerRight\":{\"x\":663.9383941831829,\"y\":218.82579600949603},\"upperLeft\":{\"x\":563.9383941831829,\"y\":138.82579600949603}},\"dockers\":[]},{\"resourceId\":\"sid-1584A029-9920-429C-AF71-0BB38D9CE882\",\"properties\":{\"overrideid\":\"\",\"name\":\"结束\",\"documentation\":\"\",\"executionlisteners\":\"\"},\"stencil\":{\"id\":\"EndNoneEvent\"},\"childShapes\":[],\"outgoing\":[],\"bounds\":{\"lowerRight\":{\"x\":762.7584217788329,\"y\":193.65159201899206},\"upperLeft\":{\"x\":734.7584217788329,\"y\":165.65159201899206}},\"dockers\":[]},{\"resourceId\":\"sid-FD2BD278-ED8C-4770-9D5D-54817AF1A672\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-1584A029-9920-429C-AF71-0BB38D9CE882\"}],\"bounds\":{\"lowerRight\":{\"x\":734.0657814844947,\"y\":179.56159705355253},\"upperLeft\":{\"x\":664.4426043076157,\"y\":179.13514303995794}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":14,\"y\":14}],\"target\":{\"resourceId\":\"sid-1584A029-9920-429C-AF71-0BB38D9CE882\"}},{\"resourceId\":\"sid-0F438303-82A4-4FFF-9B4E-8B4E3E68C6CF\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"asynchronousdefinition\":\"false\",\"exclusivedefinition\":\"false\",\"sequencefloworder\":\"\"},\"stencil\":{\"id\":\"ExclusiveGateway\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-3A93B1D6-5001-47EA-869F-080922D8D138\"},{\"resourceId\":\"sid-0EF6E53F-A18A-431C-A619-331AAF3ABDC5\"}],\"bounds\":{\"lowerRight\":{\"x\":505,\"y\":198},\"upperLeft\":{\"x\":465,\"y\":158}},\"dockers\":[]},{\"resourceId\":\"sid-F192A1C9-45C0-4847-80DD-9E0A73C69A87\",\"properties\":{\"overrideid\":\"\",\"name\":\"\",\"documentation\":\"\",\"conditionsequenceflow\":\"\",\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\"},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-0F438303-82A4-4FFF-9B4E-8B4E3E68C6CF\"}],\"bounds\":{\"lowerRight\":{\"x\":464.64844687000755,\"y\":178.4097335362338},\"upperLeft\":{\"x\":420.62889687999245,\"y\":178.2191727137662}},\"dockers\":[{\"x\":50,\"y\":40},{\"x\":20.5,\"y\":20.5}],\"target\":{\"resourceId\":\"sid-0F438303-82A4-4FFF-9B4E-8B4E3E68C6CF\"}},{\"resourceId\":\"sid-0EF6E53F-A18A-431C-A619-331AAF3ABDC5\",\"properties\":{\"overrideid\":\"\",\"name\":\"不同意\",\"documentation\":\"\",\"conditionsequenceflow\":{\"expression\":{\"type\":\"static\",\"staticValue\":\"${opinion==\'N\'}\"}},\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\",\"showdiamondmarker\":false},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-414A1C8A-A7C0-4C6B-96DF-827813026B84\"}],\"bounds\":{\"lowerRight\":{\"x\":485.5,\"y\":356.2464918680689},\"upperLeft\":{\"x\":225.8632415327293,\"y\":198.86825464997514}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":485.5,\"y\":251.41578221167106},{\"x\":311.22032280819457,\"y\":356.2464918680689},{\"x\":50,\"y\":79}],\"target\":{\"resourceId\":\"sid-414A1C8A-A7C0-4C6B-96DF-827813026B84\"}},{\"resourceId\":\"sid-3A93B1D6-5001-47EA-869F-080922D8D138\",\"properties\":{\"overrideid\":\"\",\"name\":\"同意\",\"documentation\":\"\",\"conditionsequenceflow\":{\"expression\":{\"type\":\"static\",\"staticValue\":\"${opinion==\'Y\'}\"}},\"executionlisteners\":\"\",\"defaultflow\":\"false\",\"skipexpression\":\"\",\"showdiamondmarker\":false},\"stencil\":{\"id\":\"SequenceFlow\"},\"childShapes\":[],\"outgoing\":[{\"resourceId\":\"sid-49C22E46-E64E-4A7A-82C4-236BBCA93544\"}],\"bounds\":{\"lowerRight\":{\"x\":563.3177901386101,\"y\":178.81914952153363},\"upperLeft\":{\"x\":505.1183652267002,\"y\":178.5804596463639}},\"dockers\":[{\"x\":20.5,\"y\":20.5},{\"x\":1,\"y\":40}],\"target\":{\"resourceId\":\"sid-49C22E46-E64E-4A7A-82C4-236BBCA93544\"}}],\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"}}', 0x89504E470D0A1A0A0000000D49484452000001130000006508060000006EA507D0000009FB4944415478DAED9D7F6C94771DC7C161D8748B384D9C6E26984C63741A8C66DB1F882423080A75A877C75D4B5B293D6AA0A446584722D218A012B407956870DD5C3699846ECE04570C3DA01C99EBD2758B8EA6088139B46029A302D6DAB5F0F8F9D4E7483DAEBD9FCF73CF3DCFEB957C72E50A3CF7F97CBFDFF77DBEDFE7FB7CBED3A60100000000000000000080BB310C63C6D9B3675B3B3B3BAF1F3E7CD8686F6F2FA845A35123168B5D122BC347FA18F12F22B4918F1F3F6E5CBC78D118191971840D0C0C18C78E1DBB228DFE6D7CA48F11FF2241BF2D2636F29F5EDA306ED706FB0ADDD823F22DD28B8FF431E25F2468DA3931C0F186EE3DBABDE0DF1ED2D0A3F8481F23FE4582CE219335744FF4474E6868031FE963C4BFC81BDA09F35AABC5C44D3ED2C788BFA31A5AE7B0F1863EF5F2CF5C29266EF5913E46FC1DD3D0F1064E343789899B7D4CC10CFA1862627B0AEA24B36A9AE3461FA76091985EE351FA18624243E363B62C101B146B141B10ABA38F79504C9A9A9A966FDAB4E9F4EAD5ABDF5DBE7CB9515151315A5757D72DAF8FD0D0F8986646A242B2C4FCF30362A7C522764F7B1093C991B1FD25B11AB1866030581708041ECEEB05B66DDBD6B176EDDA1B6D6D6DE3BB0695C1C1C171C76B6B6B476A6A6A9E993F7FFE0C1A1A1F5364248B12DEBF47AC4BAC55EC76C4A4706222C251160A854E69A29068F2BB3E79ADCC798C6FD9B225BA75EB56636868C84886BE2FBF1F920CE5291A1A1FD3C84812B953EC80D82B621F464CEC1713118A67E3C2B161C30663CF9E3D466B6BABD1D2D262D4D7D7DF1415C9525ECCFA22F21F2F928CE4FA6442325150AAABABDF11059B4B43E3631A1949B2BB3BBF30A73DF72326F621021151A10887C3635D5D5DC6D8D8D82DE3BBBBBBDB90D9C798292891AC2EB47EFDFA6E9DDAA4432C16BB2A17FB1D0D8D8F696624C978DC5C987D1831B12523992D362AD39BEB67CE9C99727C9F3B77CE58B16285AE978EFA7CBECC057FE5CA9543F1359254E81A8A6426577270AC57177DDCDCD05EF2D1BCFD9BCDB7D8AFCC7F3BCB82C133FED9F6EFDFEFF4F84FB75A482A2B2B6F972C63AD661B8D8D8DFF4A678C4722916BE61A4A5D36C1373221D9E24DA6E6F481868FE9D9BC79F38C993367EAC09893C5B46849BCFF59694E8CFFA14387F4B3E9946258ECB2D8DFC54ECBC0FFB3BCBE2AD6217650ECB732A87F2DEF3F21D62CB65D8552AC5E6C9DFC6E95BC572AF64DB1C5F2DE7CB187E4FDCF6B66213F5F117B45E320E27A3D9DF1ADEB2866EC1A3216938A8A8A2BE96626FDFDFD97E5430F9282E2E3046AC4FE26363B5321F1F83467BA660E32E8EF16BB57077F6969E9E7643AF2A088C157C416992251AAA221562BF6980CF2CD623F36C5E509151BF9F30B626D6247C53A4D513A6D8AD5F8368F1D3B76A435C677EDDA35927566525E5EBE77DFBE7DC3E95CA8A5A5A54D2EF21B061A3E26A01BD4DE48316DB14D485833B975CD44C6F9C8D5AB57A71CDFC3C3C3465555D5B029269FCEF862A288F788420EF6F4F44C79A113274E5C900B5CD20F4743E36312F44BA67D92CD69B60A0962728BA01C5081686A6A1A4B762727CEEEDDBB6F9842D29ECB6696B9AB56AD1A686E6EBEA1A2A20BAD8A2AD9C993278D9D3B775E2E2B2BFBABCEC968687C9C04DD9076D45C5C2DA890202649B393F185D548247223711B8866242224F1B592D1ACB2928495DF59EBD6AD6BDAB871E35BE1707834BE9D7ECD9A35BD3297FBA1663034343EA640A7395A92B0A190428298DC8ADFEF5F1017141DD79AA5E862AB6623D5D5D5EFC685241008D8DE5634343E4EC67D6217C49E2D949020265366280726D94EDF9E7346424323261630C7DC47B2C48EFD14C43F735131F79F8C3FE8579422829878AA33EB9467BAA6CE851014E2EF1110136F74E6929292BB749E4E1F434C68687CCC359DFE98D879FA18624243E3634EC8BCFC5322267FA18F21269691784092836CD8AA43B8DCE8632A42A1D017454C5EA38F1526FE9E20168B5D70D219B071EBEBEBDB97AFA31BBDE0631AD31C7DF0EC287DAC30F1F704478E1CF9464747C73FFBFBFBFFED946F0B6DE46834FA76BE0E95F6828FA90806834B755F037DAC30F1F70C12D025A2D05D9AF2E91CD24A3B78F060AABFA39FA137DF8D6CA78F6998253EA6C84C4262CF79A18F3931FE90677C3EDF1D5A3641BE253F4234EC4584242CB68748809B3AB556DEFE2391B01711F0EF8B90FF8448806B900EFD9288C97F4A4B4BEF231AB68AB85605DB4C24C035E85920D2A95BE8D8B667263FD5EC844880AB306B625E94D73B89866D99C92F75DD8448801B3BF75EADA349246C8BF77312EF209100D71108041E900E7E41EFF0100D5BC4E440D115DB01C840505ED45A0E44C21631E9D04AEB4402DCDAC1F504F8B7C3E1F07B8986E5B1EED6E7738804B8393BF98374F42A2261B9989CD227878904B8B9937F590F29F2F97CB7110D4BE37CBEACACECA34402DCDED163DC69B03C03BCC6AD78703D3297FFAA08CA9BD30A54F0D8CDE87A94DFEFDFA855CF172E5CF87E22025EC84EBA2439799448E4770A2931ED91D7DF4B56F20922029E408544058548E44E7979F987249E4F493CCFE981DB4404BC861EC5F0A674FE858422A718AE14FB8758136B24E0E5B45C0BF8C488445699DD673576629DA150680E11014FA3B787F536B1CEF589467A2C5DBAF47D22248DFAE0A4584D4343C37B880AC0FFB2932ADDC84624D28AD5D7C5DE321F9AA47A1DC044F456A62E1CEA567BA2911C2D2C25F17941CFC1117B8488004C3EFFAFD587008944D269E0F7C406B4B8D4E2C58B67121580A907CD1D5A9E40171589C6CD29CD4322B06F484CDA43A1D027890840FAD9C963BA16E0F538545656CE9238FC5C0B71F3C8014016949494DC659676BCDFC3D988DE2A3FAF622271F800BD0220FBC1B4598B4F7BCD6F9DC688DF51B1D7E5E707E90900B90FAA0FCA80BA24F6712FF8AB0BAAE691140332A3A9A32C03401E090402DBC59ADDEEA7DFEF5F6016307A5E44E45E5A1E20CFE8662C1964EFB8755396E9DF5EB1B392897D8D1607B0363B69D60CC54D3EE9B677F1E9BBE636F86D54E907B0015D33D1B5135D4371893F5F107B551FCC1311F90C2D0C60EF002CFA2345F576B76423112D1120AFDF9946653900FB29F6234583C1E0B7CC678E9ED4E245B4284061B393A23B52543EF36C2D9B2876423EFB5C5A11C00114D391A2FAF4B37CDEC7CD87F2EA39680CC07982E2F82345271672D6CC84560370E64075EC91A214720628BEECC469478A52C819A048B313C71C294A216780E21794821E294A2167009750C8234529E40CE0BEECC4D6234529E40CE052EC3A529442CE00EEC7F2234529E40CE09DA98E25478A52C819C0635871A428859C01BC9B9DE4E548510A3903789C5C8F14A5903300DC24DB234529E40C00FF47A6478A52C81900A6128894478A52C819005292EA48510A390340DA243B529442CE00903189478A52C81900B2460FEC12F1789A42CE0090EB54472BC28F89FD8042CE009013CB962D634A03000000000000000000000000000000000060F25FC805BCF17B02EEA40000000049454E44AE426082, 0, '');

-- ----------------------------
-- Table structure for act_de_model_history
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model_history`;
CREATE TABLE `act_de_model_history`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `model_key` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `model_comment` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `created` datetime(6) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `last_updated` datetime(6) NULL DEFAULT NULL,
  `last_updated_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `removal_date` datetime(6) NULL DEFAULT NULL,
  `version` int(0) NULL DEFAULT NULL,
  `model_editor_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `model_type` int(0) NULL DEFAULT NULL,
  `tenant_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_proc_mod_history_proc`(`model_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_model_history
-- ----------------------------
INSERT INTO `act_de_model_history` VALUES ('43bbdeab-83f3-11ec-88f2-dc41a90b0909', 'Request Resource Approval ', 'requestResourceApprovalProcess', NULL, NULL, '2022-02-02 14:41:53.304000', 'admin', '2022-02-02 14:41:53.304000', 'admin', '2022-02-02 14:42:17.832000', 1, '{\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"},\"properties\":{\"process_id\":\"requestResourceApprovalProcess\",\"name\":\"Request Resource Approval \",\"process_namespace\":\"http://www.activiti.org/test\",\"iseagerexecutionfetch\":false,\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[],\"escalationdefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":80.0,\"y\":153.0},\"upperLeft\":{\"x\":45.0,\"y\":118.0}},\"resourceId\":\"starter\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"starter\",\"name\":\"Starter\",\"interrupting\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow12\"}]},{\"bounds\":{\"lowerRight\":{\"x\":511.0,\"y\":301.0},\"upperLeft\":{\"x\":340.0,\"y\":230.0}},\"resourceId\":\"sendJuniorRejectEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendJuniorRejectEmail\",\"name\":\"���ͳ��������ܾ��ʼ�\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendJuniorRejectionMailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow5\"}]},{\"bounds\":{\"lowerRight\":{\"x\":443.0,\"y\":420.0},\"upperLeft\":{\"x\":408.0,\"y\":385.0}},\"resourceId\":\"juniorRejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"juniorRejectEnd\",\"name\":\"Junior Reject End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow5\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":85.5,\"y\":35.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"juniorRejectEnd\"}],\"target\":{\"resourceId\":\"juniorRejectEnd\"},\"properties\":{\"overrideid\":\"flow5\"}},{\"bounds\":{\"lowerRight\":{\"x\":696.0,\"y\":173.0},\"upperLeft\":{\"x\":575.0,\"y\":95.0}},\"resourceId\":\"seniorApproval\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"seniorApproval\",\"name\":\"�߼�����\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${seniorAdmin}\"}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow7\"}]},{\"bounds\":{\"lowerRight\":{\"x\":285.0,\"y\":176.0},\"upperLeft\":{\"x\":170.0,\"y\":95.0}},\"resourceId\":\"juniorApproval\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"juniorApproval\",\"name\":\"��������\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"assignee\":\"${juniorAdmin}\"}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow11\"}]},{\"bounds\":{\"lowerRight\":{\"x\":445.0,\"y\":153.0},\"upperLeft\":{\"x\":405.0,\"y\":113.0}},\"resourceId\":\"exclusivegateway1\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway1\",\"name\":\"Exclusive Gateway1\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"juniorSuccessFlow\"},{\"resourceId\":\"juniorRejectFlow\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"juniorSuccessFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":60.5,\"y\":39.0}],\"outgoing\":[{\"resourceId\":\"seniorApproval\"}],\"target\":{\"resourceId\":\"seniorApproval\"},\"properties\":{\"overrideid\":\"juniorSuccessFlow\",\"name\":\"ͬ��\",\"conditionsequenceflow\":\"${approved==\'Y\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"juniorRejectFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":85.5,\"y\":35.5}],\"outgoing\":[{\"resourceId\":\"sendJuniorRejectEmail\"}],\"target\":{\"resourceId\":\"sendJuniorRejectEmail\"},\"properties\":{\"overrideid\":\"juniorRejectFlow\",\"name\":\"�ܾ�\",\"conditionsequenceflow\":\"${approved==\'N\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":805.0,\"y\":155.0},\"upperLeft\":{\"x\":765.0,\"y\":115.0}},\"resourceId\":\"exclusivegateway2\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"exclusivegateway2\",\"name\":\"Exclusive Gateway2\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"seniorSuccessFlow\"},{\"resourceId\":\"seniorRejectFlow\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow7\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":60.5,\"y\":39.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway2\"}],\"target\":{\"resourceId\":\"exclusivegateway2\"},\"properties\":{\"overrideid\":\"flow7\"}},{\"bounds\":{\"lowerRight\":{\"x\":1175.0,\"y\":152.0},\"upperLeft\":{\"x\":1140.0,\"y\":117.0}},\"resourceId\":\"approvalSuccessEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"approvalSuccessEnd\",\"name\":\"Approval Success End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"seniorSuccessFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":70.5,\"y\":37.5}],\"outgoing\":[{\"resourceId\":\"sendApprovalSuccessEmail\"}],\"target\":{\"resourceId\":\"sendApprovalSuccessEmail\"},\"properties\":{\"overrideid\":\"seniorSuccessFlow\",\"name\":\"ͬ��\",\"conditionsequenceflow\":\"${approved==\'Y\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":882.0,\"y\":301.0},\"upperLeft\":{\"x\":690.0,\"y\":230.0}},\"resourceId\":\"sendSeniorRejectEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendSeniorRejectEmail\",\"name\":\"���͸߼������ܾ��ʼ�\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendSeniorRejectionMailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow9\"}]},{\"bounds\":{\"lowerRight\":{\"x\":803.0,\"y\":420.0},\"upperLeft\":{\"x\":768.0,\"y\":385.0}},\"resourceId\":\"seniorRejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"seniorRejectEnd\",\"name\":\"Senior Reject End\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"seniorRejectFlow\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":96.0,\"y\":35.5}],\"outgoing\":[{\"resourceId\":\"sendSeniorRejectEmail\"}],\"target\":{\"resourceId\":\"sendSeniorRejectEmail\"},\"properties\":{\"overrideid\":\"seniorRejectFlow\",\"name\":\"�ܾ�\",\"conditionsequenceflow\":\"${approved==\'N\'}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow9\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":96.0,\"y\":35.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"seniorRejectEnd\"}],\"target\":{\"resourceId\":\"seniorRejectEnd\"},\"properties\":{\"overrideid\":\"flow9\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow11\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":57.5,\"y\":40.5},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"exclusivegateway1\"}],\"target\":{\"resourceId\":\"exclusivegateway1\"},\"properties\":{\"overrideid\":\"flow11\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow12\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":17.5,\"y\":17.5},{\"x\":57.5,\"y\":40.5}],\"outgoing\":[{\"resourceId\":\"juniorApproval\"}],\"target\":{\"resourceId\":\"juniorApproval\"},\"properties\":{\"overrideid\":\"flow12\"}},{\"bounds\":{\"lowerRight\":{\"x\":1061.0,\"y\":171.0},\"upperLeft\":{\"x\":920.0,\"y\":96.0}},\"resourceId\":\"sendApprovalSuccessEmail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendApprovalSuccessEmail\",\"name\":\"��������ͨ���ʼ�\",\"servicetaskclass\":\"com.gitee.approval.delegate.SendApprovalSuccessEmailDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"flow13\"}]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"flow13\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":70.5,\"y\":37.5},{\"x\":17.5,\"y\":17.5}],\"outgoing\":[{\"resourceId\":\"approvalSuccessEnd\"}],\"target\":{\"resourceId\":\"approvalSuccessEnd\"},\"properties\":{\"overrideid\":\"flow13\"}}]}', '3577388a-83f3-11ec-88f2-dc41a90b0909', 0, '');
INSERT INTO `act_de_model_history` VALUES ('a59a4d3b-8693-11ec-8361-dc41a90b0909', '请假流程', 'holidayRequest', '员工请假流程', NULL, '2022-02-05 22:54:53.770000', 'admin', '2022-02-05 22:54:53.770000', 'admin', '2022-02-05 22:55:23.830000', 1, '{\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"stencil\":{\"id\":\"BPMNDiagram\"},\"stencilset\":{\"namespace\":\"http://b3mn.org/stencilset/bpmn2.0#\",\"url\":\"../editor/stencilsets/bpmn2.0/bpmn2.0.json\"},\"properties\":{\"process_id\":\"holidayRequest\",\"name\":\"请假流程\",\"documentation\":\"员工请假流程\",\"process_namespace\":\"http://www.flowable.org/processdef\",\"iseagerexecutionfetch\":false,\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[],\"escalationdefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":130.0,\"y\":205.0},\"upperLeft\":{\"x\":100.0,\"y\":175.0}},\"resourceId\":\"startEvent\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"startEvent\",\"name\":\"启动事件\",\"interrupting\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000012\"}]},{\"bounds\":{\"lowerRight\":{\"x\":315.4,\"y\":230.0},\"upperLeft\":{\"x\":215.39999999999998,\"y\":150.0}},\"resourceId\":\"approveTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"approveTask\",\"name\":\"ApproveRequest\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"managers\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\"}]},{\"bounds\":{\"lowerRight\":{\"x\":400.4,\"y\":210.0},\"upperLeft\":{\"x\":360.4,\"y\":170.0}},\"resourceId\":\"decision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"decision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\"},{\"resourceId\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\"}]},{\"bounds\":{\"lowerRight\":{\"x\":520.0,\"y\":140.0},\"upperLeft\":{\"x\":420.0,\"y\":60.0}},\"resourceId\":\"externalSystemCall\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"externalSystemCall\",\"name\":\"SuccessService\",\"servicetaskclass\":\"com.example.demo.delegate.CallExternalSystemDelegate\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000011\"}]},{\"bounds\":{\"lowerRight\":{\"x\":520.0,\"y\":305.0},\"upperLeft\":{\"x\":420.0,\"y\":225.0}},\"resourceId\":\"sendRejectionMail\",\"childShapes\":[],\"stencil\":{\"id\":\"ServiceTask\"},\"properties\":{\"overrideid\":\"sendRejectionMail\",\"name\":\"faileService\",\"servicetaskclass\":\"com.example.demo.delegate.SendRejectionMail\",\"servicetaskfields\":{\"fields\":[]},\"servicetaskexceptions\":{\"exceptions\":[]},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00009\"}]},{\"bounds\":{\"lowerRight\":{\"x\":665.0,\"y\":140.0},\"upperLeft\":{\"x\":565.0,\"y\":60.0}},\"resourceId\":\"firstApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"firstApprovedTask\",\"name\":\"firstApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"first\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00004\"}]},{\"bounds\":{\"lowerRight\":{\"x\":750.0,\"y\":120.0},\"upperLeft\":{\"x\":710.0,\"y\":80.0}},\"resourceId\":\"firstDecision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"firstDecision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00005\"},{\"resourceId\":\"sequenceFlow-000013\"}]},{\"bounds\":{\"lowerRight\":{\"x\":895.0,\"y\":140.0},\"upperLeft\":{\"x\":795.0,\"y\":60.0}},\"resourceId\":\"secenedApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"secenedApprovedTask\",\"name\":\"secenedApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"secened\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00006\"}]},{\"bounds\":{\"lowerRight\":{\"x\":980.0,\"y\":120.0},\"upperLeft\":{\"x\":940.0,\"y\":80.0}},\"resourceId\":\"secenedDecision\",\"childShapes\":[],\"stencil\":{\"id\":\"ExclusiveGateway\"},\"properties\":{\"overrideid\":\"secenedDecision\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-00008\"},{\"resourceId\":\"sequenceFlow-00007\"}]},{\"bounds\":{\"lowerRight\":{\"x\":1120.0,\"y\":140.0},\"upperLeft\":{\"x\":1020.0,\"y\":60.0}},\"resourceId\":\"bossApprovedTask\",\"childShapes\":[],\"stencil\":{\"id\":\"UserTask\"},\"properties\":{\"overrideid\":\"bossApprovedTask\",\"name\":\"bossApprovedTask\",\"usertaskassignment\":{\"assignment\":{\"type\":\"static\",\"candidateGroups\":[{\"value\":\"boss\"}]}},\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"isforcompensation\":false,\"tasklisteners\":{\"taskListeners\":[]},\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"sequenceFlow-000010\"}]},{\"bounds\":{\"lowerRight\":{\"x\":593.0,\"y\":279.0},\"upperLeft\":{\"x\":565.0,\"y\":251.0}},\"resourceId\":\"rejectEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"rejectEnd\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":1193.0,\"y\":114.0},\"upperLeft\":{\"x\":1165.0,\"y\":86.0}},\"resourceId\":\"approveEnd\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"approveEnd\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000011\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"firstApprovedTask\"}],\"target\":{\"resourceId\":\"firstApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-000011\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00004\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"firstDecision\"}],\"target\":{\"resourceId\":\"firstDecision\"},\"properties\":{\"overrideid\":\"sequenceFlow-00004\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00005\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"secenedApprovedTask\"}],\"target\":{\"resourceId\":\"secenedApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-00005\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00008\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-00008\",\"name\":\"faile\",\"conditionsequenceflow\":\"${!approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00006\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"secenedDecision\"}],\"target\":{\"resourceId\":\"secenedDecision\"},\"properties\":{\"overrideid\":\"sequenceFlow-00006\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000010\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"approveEnd\"}],\"target\":{\"resourceId\":\"approveEnd\"},\"properties\":{\"overrideid\":\"sequenceFlow-000010\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00009\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":14.0,\"y\":14.0}],\"outgoing\":[{\"resourceId\":\"rejectEnd\"}],\"target\":{\"resourceId\":\"rejectEnd\"},\"properties\":{\"overrideid\":\"sequenceFlow-00009\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":380.0,\"y\":265.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-90e6eadb-3a9e-4955-bad4-ec337a360fac\",\"name\":\"faile\",\"conditionsequenceflow\":\"${!approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":380.0,\"y\":100.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"externalSystemCall\"}],\"target\":{\"resourceId\":\"externalSystemCall\"},\"properties\":{\"overrideid\":\"sequenceFlow-c1d4f277-e5a7-492a-9768-a9161f18c116\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":40.0},{\"x\":20.0,\"y\":20.0}],\"outgoing\":[{\"resourceId\":\"decision\"}],\"target\":{\"resourceId\":\"decision\"},\"properties\":{\"overrideid\":\"sequenceFlow-5587fdbe-fb00-420d-9711-1b1a81c086db\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-00007\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"bossApprovedTask\"}],\"target\":{\"resourceId\":\"bossApprovedTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-00007\",\"name\":\"success\",\"conditionsequenceflow\":\"${approved}\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000012\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":15.0,\"y\":15.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"approveTask\"}],\"target\":{\"resourceId\":\"approveTask\"},\"properties\":{\"overrideid\":\"sequenceFlow-000012\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"sequenceFlow-000013\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":20.0,\"y\":20.0},{\"x\":50.0,\"y\":40.0}],\"outgoing\":[{\"resourceId\":\"sendRejectionMail\"}],\"target\":{\"resourceId\":\"sendRejectionMail\"},\"properties\":{\"overrideid\":\"sequenceFlow-000013\",\"name\":\"faile\",\"conditionsequenceflow\":\"${approved}\"}}]}', '93caf9ba-8693-11ec-8361-dc41a90b0909', 0, '');

-- ----------------------------
-- Table structure for act_de_model_relation
-- ----------------------------
DROP TABLE IF EXISTS `act_de_model_relation`;
CREATE TABLE `act_de_model_relation`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `parent_model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `model_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `relation_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_relation_parent`(`parent_model_id`) USING BTREE,
  INDEX `fk_relation_child`(`model_id`) USING BTREE,
  CONSTRAINT `fk_relation_child` FOREIGN KEY (`model_id`) REFERENCES `act_de_model` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_relation_parent` FOREIGN KEY (`parent_model_id`) REFERENCES `act_de_model` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_de_model_relation
-- ----------------------------
INSERT INTO `act_de_model_relation` VALUES ('55211c46-8378-11ec-ba42-dc41a90b0909', 'f73c50b4-8375-11ec-ba42-dc41a90b0909', '9ed1f7c5-8377-11ec-ba42-dc41a90b0909', 'form-model');

-- ----------------------------
-- Table structure for act_dmn_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_databasechangelog`;
CREATE TABLE `act_dmn_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_databasechangelog
-- ----------------------------
INSERT INTO `act_dmn_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:18', 1, 'EXECUTED', '8:c8701f1c71018b55029f450b2e9a10a1', 'createTable tableName=ACT_DMN_DEPLOYMENT; createTable tableName=ACT_DMN_DEPLOYMENT_RESOURCE; createTable tableName=ACT_DMN_DECISION_TABLE', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:18', 2, 'EXECUTED', '8:47f94b27feb7df8a30d4e338c7bd5fb8', 'createTable tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 3, 'EXECUTED', '8:ac17eae89fbdccb6e08daf3c7797b579', 'addColumn tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 4, 'EXECUTED', '8:f73aabc4529e7292c2942073d1cff6f9', 'dropColumn columnName=PARENT_DEPLOYMENT_ID_, tableName=ACT_DMN_DECISION_TABLE', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 5, 'EXECUTED', '8:3e03528582dd4eeb4eb41f9b9539140d', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_DMN_DEPLOYMENT; modifyDataType columnName=START_TIME_, tableName=ACT_DMN_HI_DECISION_EXECUTION; modifyDataType columnName=END_TIME_, tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('6', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 6, 'EXECUTED', '8:646c6a061e0b6e8a62e69844ff96abb0', 'createIndex indexName=ACT_IDX_DEC_TBL_UNIQ, tableName=ACT_DMN_DECISION_TABLE', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('7', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 7, 'EXECUTED', '8:215a499ff7ae77685b55355245b8b708', 'dropIndex indexName=ACT_IDX_DEC_TBL_UNIQ, tableName=ACT_DMN_DECISION_TABLE; renameTable newTableName=ACT_DMN_DECISION, oldTableName=ACT_DMN_DECISION_TABLE; createIndex indexName=ACT_IDX_DMN_DEC_UNIQ, tableName=ACT_DMN_DECISION', '', NULL, '3.8.0', NULL, NULL, '3728878899');
INSERT INTO `act_dmn_databasechangelog` VALUES ('8', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-02-01 23:21:19', 8, 'EXECUTED', '8:5355bee389318afed91a11702f2df032', 'addColumn tableName=ACT_DMN_DECISION', '', NULL, '3.8.0', NULL, NULL, '3728878899');

-- ----------------------------
-- Table structure for act_dmn_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_databasechangeloglock`;
CREATE TABLE `act_dmn_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_databasechangeloglock
-- ----------------------------
INSERT INTO `act_dmn_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_dmn_decision
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_decision`;
CREATE TABLE `act_dmn_decision`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `VERSION_` int(0) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DECISION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_DMN_DEC_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_dmn_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_deployment`;
CREATE TABLE `act_dmn_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_dmn_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_deployment_resource`;
CREATE TABLE `act_dmn_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_dmn_hi_decision_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_hi_decision_execution`;
CREATE TABLE `act_dmn_hi_decision_execution`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DECISION_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `INSTANCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `FAILED_` bit(1) NULL DEFAULT b'0',
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `EXECUTION_JSON_` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_evt_log
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log`  (
  `LOG_NR_` bigint(0) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(0) NULL DEFAULT 0,
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_fo_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_databasechangelog`;
CREATE TABLE `act_fo_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_databasechangelog
-- ----------------------------
INSERT INTO `act_fo_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-02-01 23:21:19', 1, 'EXECUTED', '8:033ebf9380889aed7c453927ecc3250d', 'createTable tableName=ACT_FO_FORM_DEPLOYMENT; createTable tableName=ACT_FO_FORM_RESOURCE; createTable tableName=ACT_FO_FORM_DEFINITION; createTable tableName=ACT_FO_FORM_INSTANCE', '', NULL, '3.8.0', NULL, NULL, '3728879280');
INSERT INTO `act_fo_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-02-01 23:21:19', 2, 'EXECUTED', '8:986365ceb40445ce3b27a8e6b40f159b', 'addColumn tableName=ACT_FO_FORM_INSTANCE', '', NULL, '3.8.0', NULL, NULL, '3728879280');
INSERT INTO `act_fo_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-02-01 23:21:19', 3, 'EXECUTED', '8:abf482518ceb09830ef674e52c06bf15', 'dropColumn columnName=PARENT_DEPLOYMENT_ID_, tableName=ACT_FO_FORM_DEFINITION', '', NULL, '3.8.0', NULL, NULL, '3728879280');
INSERT INTO `act_fo_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-02-01 23:21:19', 4, 'EXECUTED', '8:2087829f22a4b2298dbf530681c74854', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_FO_FORM_DEPLOYMENT; modifyDataType columnName=SUBMITTED_DATE_, tableName=ACT_FO_FORM_INSTANCE', '', NULL, '3.8.0', NULL, NULL, '3728879280');
INSERT INTO `act_fo_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-02-01 23:21:19', 5, 'EXECUTED', '8:b4be732b89e5ca028bdd520c6ad4d446', 'createIndex indexName=ACT_IDX_FORM_DEF_UNIQ, tableName=ACT_FO_FORM_DEFINITION', '', NULL, '3.8.0', NULL, NULL, '3728879280');

-- ----------------------------
-- Table structure for act_fo_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_databasechangeloglock`;
CREATE TABLE `act_fo_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_databasechangeloglock
-- ----------------------------
INSERT INTO `act_fo_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_fo_form_definition
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_definition`;
CREATE TABLE `act_fo_form_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `VERSION_` int(0) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_FORM_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_fo_form_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_deployment`;
CREATE TABLE `act_fo_form_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_fo_form_instance
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_instance`;
CREATE TABLE `act_fo_form_instance`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FORM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `TASK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SUBMITTED_DATE_` datetime(3) NULL DEFAULT NULL,
  `SUBMITTED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `FORM_VALUES_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_fo_form_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_resource`;
CREATE TABLE `act_fo_form_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ge_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_bytearray`;
CREATE TABLE `act_ge_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  `GENERATED_` tinyint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_BYTEARR_DEPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_bytearray
-- ----------------------------
INSERT INTO `act_ge_bytearray` VALUES ('4b5cc587-83f5-11ec-98ed-dc41a90b0909', 1, 'bpmn/请假bpmn20.xml', '4b5cc586-83f5-11ec-98ed-dc41a90b0909', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226B65792D3122206E616D653D22E8AFB7E581872220697345786563757461626C653D2274727565223E0A202020203C646F63756D656E746174696F6E3EE8AFB7E58187E6B581E7A88B3C2F646F63756D656E746174696F6E3E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D34313441314338412D413743302D344336422D393644462D38323738313330323642383422206E616D653D22E78FADE995BF2220666C6F7761626C653A61737369676E65653D2279716D6D2220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE58583E6B0945D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE6BBA1E6BBA15D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D33393131423530442D393931412D344633342D424236342D4143363935413338303335422220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D33363942393432352D443446452D343738362D383636382D38433331393441303338324622206E616D653D22E88081E5B8882220666C6F7761626C653A61737369676E65653D22736F6E6778792220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE58588E998B35D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE5AE8B5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D42464538363736352D384337332D344539432D384536442D3446384330343639313546322220736F757263655265663D227369642D34313441314338412D413743302D344336422D393644462D38323738313330323642383422207461726765745265663D227369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D34394332324534362D453634452D344137412D383243342D32333642424341393335343422206E616D653D22E78FADE4B8BBE4BBBB2220666C6F7761626C653A61737369676E65653D2261646D696E2220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D34453744384542372D323932452D344133332D413743452D3439364531303634423036372220736F757263655265663D227369642D33363942393432352D443446452D343738362D383636382D38433331393441303338324622207461726765745265663D227369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434223E3C2F73657175656E6365466C6F773E0A202020203C656E644576656E742069643D227369642D31353834413032392D393932302D343239432D414637312D30424233384439434538383222206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C73657175656E6365466C6F772069643D227369642D46443242443237382D454438432D343737302D394435442D3534383137414631413637322220736F757263655265663D227369642D34394332324534362D453634452D344137412D383243342D32333642424341393335343422207461726765745265663D227369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832223E3C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6B65792D31223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226B65792D31222069643D2242504D4E506C616E655F6B65792D31223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834222069643D2242504D4E53686170655F7369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246222069643D2242504D4E53686170655F7369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223332302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434222069643D2242504D4E53686170655F7369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223436352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832222069643D2242504D4E53686170655F7369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223631302E302220793D223136342E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D33393131423530442D393931412D344633342D424236342D414336393541333830333542222069643D2242504D4E456467655F7369642D33393131423530442D393931412D344633342D424236342D414336393541333830333542223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D42464538363736352D384337332D344539432D384536442D344638433034363931354632222069643D2242504D4E456467655F7369642D42464538363736352D384337332D344539432D384536442D344638433034363931354632223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E393439393939393939393930372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223331392E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46443242443237382D454438432D343737302D394435442D353438313741463141363732222069643D2242504D4E456467655F7369642D46443242443237382D454438432D343737302D394435442D353438313741463141363732223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223536342E39352220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223631302E302220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D34453744384542372D323932452D344133332D413743452D343936453130363442303637222069643D2242504D4E456467655F7369642D34453744384542372D323932452D344133332D413743452D343936453130363442303637223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223431392E39343939393939393939393036372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223436342E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('4b8caf28-83f5-11ec-98ed-dc41a90b0909', 1, 'bpmn/请假key-1.png', '4b5cc586-83f5-11ec-98ed-dc41a90b0909', 0x89504E470D0A1A0A0000000D4948445200000288000000E408060000001BE0D5B200000CB44944415478DAEDDD6D6F54651A07705FF8C20FB12F487693F523EC8B7DE10730595F488A9D519A4A3AEA02629184AC921294081A43A29B6C4C488C860D9B18836C9795D207CBA3645909742564CB2EA2505B8765AB2233D62267EFBBE9B863A138947666CE99DF2FB90274CA43CABFD77D9DFB9C39E79E7B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000048912449EE3D7FFEFC3BC78F1FFF7E707030E9EFEF5775AE818181E4D0A143FF09959721254332244340C3C5A67CF8F0E1A4582C26535353AA4175F9F2E5E4E0C1835F8526FDB00C291952320434543C62D7949BA6394F8523F9B332A46448C910D050F1748EA6D83C151AF3B40C29195232043454BCF644436CAAC69CC89092212543402A1AF3375F8E27E74FBC999C19D83253F1E7F1639AA9C62C433224433204B4E0807875722CF9B86F53727ADF861F55FC587C4D43B5B8CB900CC9900C012D36205E3CF3E79B9A72A52E9DE9D5502DEE32244332244340AB0D88673FD83E6F638EAF69A81677199221199221A0C506C48FFB37CFDB98E36B1AAAC55D86644886640830206ACC1AB30CC9900CC910D0CA03627CB7E07C8D39BEA6A15ADC65488664488680161B10CF1DFDFDBC8D39BEA6A15ADC65488664488680161B10AF8C8D241F1FE8B9F9B44EF8587C4D43B5B8CB900CC9900C012D3620C6FAE4EF6FDFD498E3C734538D5986644886640868C501F1DB6F9373C7FE70F3699DF0B1F89A866A71972119922119025A68408C4F28183DFAFABCD7FEC4D73CC5C0E22E433224433204B4C280188EC8C7CF0D25FFD8FFBB799B72A5E2E7C4CF75146F7197211992211902323A20FED4D1BAA3788D5986644886640868B101B196A3F5DB1DC56BB01677199221199221206303E2429B72A534588BBB0CC9900CC91090B1015169CC32244332244380015169CC32244332A40C8880C6AC31CB900CC9903220021AB3C62C43322443CA8008F34B92E4BE919191C25B6FBD7570DBB66D5FAD5DBB76BAA3A32359B1624592CFE76F3CF9E49353CF3DF7DCF9EDDBB7EF0AF54B03A2D298654886644886D22B97CBFDACADAD6D7558E7DF09752ED4D5B8E6872A87FA24BCB627FCF86CFC3C53528B0E86EFBEFBEEDB2FBCF0C2F4BA75EB9237DE7823397EFC783236369694CBE5248A3F168BC5E4A38F3E4AC200993CFDF4D3379E79E6990B5D5D5DAB0C884A639621199221194A8F471E79E4A150FDA1A66607C25AEA83F0F97953538B088360E78B2FBEF8ED860D1B6686BFEBD7AF27B53A75EA54B271E3C6EB8542E1ECF2E5CB7F6140541AB30CC9900CC950F30A43DEB230E4EDBF83A1F05675384B6B3E37EF1ADEBB77EFDEA1279E7822E9EDEDBDA3C170AEFDFBF7279D9D9DA5ACEC266ACC1AB30CC9900CA9AC0D886D6D6D2BDADBDBFF5B3DECE572B9E4A5975E4A86868692CF3EFB2CF9FAEBAF67D6F56BD7AECD9C453C7AF468B263C78EE4D1471F9D3B245EB59B98D1E1707878F86F6BD6AC4946474793C570E1C285240C9BA555AB56FDD680A82CEE3224433224434D351C3E1806BAEBD583E1AE5DBB662E1DABC5E4E4E4CCE7C7DF573D28863F7395A92A43E2CE611C0E6B0D46ADE29FD7D5D595FAA30A8D5963962119922195950131EE1C560F87DDDDDD33BB850B117F5FBC246DCE906827310BE23587F1B4F262ED1CDE6A27F1B1C71E9B0C81B9DF80A82CEE4A866448861A275E73587D5AB9A7A7E787D3C80B154F3F6FDDBA75EEE9E6FB4D58E93EB57CDF962D5BA6E235874B69DFBE7DA55C2EF7A1015159DC950CC9900C354E180E07AA770EEF7638AC1E12E7EC241E3665A5D89E3D7BFE18FF43BFFBEEBB64A93DF5D4531369DD76D6983566199221195269CF50BC954DF535870B3DAD7CBBD3CDD5D7243AD59CE2DDC3789FC3782B9B7A387DFAF4545A771135668D5986644886540606C4FECAF016DF60B21476EFDE6D1731EDE21352E21B53EEE6763677AAB3B373225EFFD04CDF2CA17EAD316BCC32244332A4B29CA1F8E493CA1B53E22EDF952B5796649D8FEF6E9E730B9C6526AE94898FCFDBB97367524F3B76EC38151FD1D32C5F83AA6DF0FEB6B6B65F69CC1AB30CC9900CA92C6668F6F179339FF7F2CB2F2FF55A5F3D203E6BE24A99F86CE5F8F8BC7A3A76ECD8851096DE66FBA6AAAAF76F7514A6316BCC32244332A4D29CA1D9672BCFBC3E3C3CBCA46B7DBC9976D5BFA5D7C495326BD7AE9D1E1F1FAFEB80383131510C61B9D8C4DF543F1C85557F7369CC1AB30CC9900CA9346728FCFA5CE5E38BFDE694B9E21357AAFE1D174D5C29D3D1D19194CBE5BA0E88F1EF0B612937FB3755F55158DCAED7983566199221195269CE50A852E5D78B756B9BDBDDF2A6EAEF2E9BB85226FEC735C25D3E10BC21A5313757C990922125430BAF7AAFF526AE9459B972E58D7AEF20964AA58994EC209E88CFA97CE08107EE756AC791BB0CC9900CA9B467283EDDC40E223559B366CD54BDAF41FCFCF3CFFFD9E4D720FEA821A7F5DA9FF04FBE65D5FABAC62C43322443F5CE4CDA3295B60CB906919A6DDAB4E95CBDDFC5DCD7D7F7D7267D17F387F10EF3731B729A17F7DB7DECA75EB7B8CB900CC950BD3393B64CA52D43DEC54CCD5E79E5959DF5BE0F624F4FCFBE26BB0FE22D8FD42DEE1677199221193220662943EE8348CD9E7FFEF99FAF5EBDFAFB3A3E49A5DCDEDEFEEF34DE55DDE2AE31CB900CC9900131CD198A4F5209EBEF743D9EA492CFE76F78924ACA757777FFAB5ECF62EEEBEBDB95D6E7325ADC35661992211932207A16B36731B78CC71F7F7CF9FAF5EBA7EBB08B38D9DEDE7E3284336F40B4B85BDC6548860C8806C4860C880F5586B7B88BB8D86F56897F5E58EB6F54DDA83B6FD24AB142A130D2DBDBBBA4D361BCDE311E49FCD435361AB3C5DDE22E4332644034202E9DB0167F5019E0BABBBB17ED9637F1D636EBD7AFBF5EBD7B98D6359FFF876559676767697474744986C3919191BDE1EF180F4712F7A7F56BE416251AB30CC9900CB9CD4D4606C465A12E5706B99E9E9EBB1E12E370B875EBD6EAEB0EAFA679CDA74A3E9FCF150A856BC562715187C34B972E1D696F6FFF34EDDBCC6E50AB31CB900CC990CAC28018B5B5B5ADA8BC61A5B293B8D0D3CDF1F7CDD939746A396B72B95C675757D7D5C5DA498C3B8771380C41DC98F6AF8DC6AC31CB900CC990CACA8038BB93D8513D24C66B12E31B57E2BB906B7DB7727C434AF53587B1B2B0E633CF51453E9FFFEABDF7DEFBE62EDEB832F9EAABAFBE194F2BC70066E1EBA2316BCC32244332A4B2342056D6FC5093D5035E1C14E37D12E3CDB4E3EE60E5F4733C8D1C9F90126F82FDDA6BAF252B57AEFC7ECED35BAE6665CDE736D727842382C142A13071F2E4C9D29DDCE7F0C08103BBC38079225E9C9AA5EB0F34668D598664488654D606C4CA9A5F7DFB9B05D661D71CB6D6A0F8701C143B3A3A8ADBB66D3B73E4C8914FC3D1C378B95C9E99064BA5D2447CB6F2C0C0C05F366FDEFC7E38EA381F43128F20B2F6CE258D5963962119922195C501B17ACD9F1D14A7EF6430CCE29A4F8DE2DDD74368D6CD3EC7F19350E5D960C41F2FC6E72CC647E964F96EE91AB3C62C433224432ACB03A2351F34668D5986644886940111D0983566195232A40C8880C6AC31CB909221254380C6AC31CB909221254380C6AC31CB909221254380C6AC2CEE4A86940C011AB3B2B82B19922119023466657157322443320468CCCAE2AE6448866408D09895C55DC9900CC9106040541677254332244380015169CC32244332244380015169CC32244332244380015169CC32244332244380015169CC32244332244380015169CC32244332A40C8880C6AC31CB900CC9903220021AB3C62C43322443CA800868CC1AB30CC9900C290322A0316BCC32244332A40C8880C6AC31CB909221654004EEDEE0E0A086D83C550E8D795A86940C2919021AEAD0A143E3C56251536C821A1B1BFB5368CC676548C9909221A0A18686867E333C3CFCE5175F7C51D21C1B77C41E9BF2C0C0C0A7A11E962125434A8680860BCDE0C170C478229E5688D79EA8BA57FCBA9F4D735396211992211902000000000000000000000000000000000000000000009AC4FF004732AE6739C0B0980000000049454E44AE426082, 1);
INSERT INTO `act_ge_bytearray` VALUES ('86f1b7d2-870f-11ec-8b65-dc41a90b0909', 1, 'bpmn/报销bpmn20.xml', '86f1b7d1-870f-11ec-8b65-dc41a90b0909', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226B65792D627822206E616D653D22E68AA5E994802220697345786563757461626C653D2274727565223E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322206E616D653D22E4BABAE4BA8B2220666C6F7761626C653A61737369676E65653D22247B68725573657249647D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F6578636C7573697665476174657761793E0A202020203C73657175656E6365466C6F772069643D227369642D38314444314543452D314331362D344539372D413934382D3041344633433044454242362220736F757263655265663D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322207461726765745265663D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922206E616D653D22E680BBE7BB8FE790862220666C6F7761626C653A61737369676E65653D2261646D696E2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422206E616D653D22E8B4A2E58AA12220666C6F7761626C653A61737369676E65653D22247B66696E616E63655573657249647D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D44443843353436332D433146362D344637322D393741312D4237353043323131373645362220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E3C2F73657175656E6365466C6F773E0A202020203C656E644576656E742069643D227369642D43394630304343332D394539462D343439392D424533362D37384337454132453835433622206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C73657175656E6365466C6F772069643D227369642D44463034354545392D423844362D343539332D423144342D4630334630393635423846302220736F757263655265663D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D46393639444233442D384236452D344441342D383036412D4436343130413431333537382220736F757263655265663D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D39463343323144342D303136302D343344312D393432332D39463835354541434545313322206E616D653D22E5B08FE4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793C313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D34303945343536372D423445442D343745382D383741352D32313335413346313038413322206E616D653D22E5A4A7E4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793E313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6B65792D6278223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226B65792D6278222069643D2242504D4E506C616E655F6B65792D6278223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433222069643D2242504D4E53686170655F7369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341222069643D2242504D4E53686170655F7369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223332302E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139222069643D2242504D4E53686170655F7369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223432392E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534222069643D2242504D4E53686170655F7369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223239302E302220793D223331352E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336222069643D2242504D4E53686170655F7369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223436352E302220793D223334312E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630222069643D2242504D4E456467655F7369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223231372E3935303030303030303030303032223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223334312E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738222069643D2242504D4E456467655F7369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338392E39353030303030303030303030352220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223436352E302220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536222069643D2242504D4E456467655F7369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236222069643D2242504D4E456467655F7369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E39343939393939393939393830362220793D223137382E3231363233333736363233333738223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223332302E343133303433343738323630392220793D223137382E34313330343334373832363039223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133222069643D2242504D4E456467655F7369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223335392E353131373438373332373937362220793D223137382E3433313135393432303238393834223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223432382E393939393939393939393838372220793D223137382E31383033323439303937343733223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133222069643D2242504D4E456467655F7369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E34343630323237323732373237352220793D223139372E3439383432343139303830303635223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E31313331373238303435333235352220793D223331352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('872e72b3-870f-11ec-8b65-dc41a90b0909', 1, 'bpmn/报销key-bx.png', '86f1b7d1-870f-11ec-8b65-dc41a90b0909', 0x89504E470D0A1A0A0000000D494844520000021B00000195080600000057753388000019374944415478DAEDDD7D8C5467BD0770548CFA87F19A684C138D35D7788931C69826DAC43F88B9A634C5B4D5E2B22F168284C5DEB62295585F08B54AA43516B5266A526B6CB8AD0D21885C2E85DDE56D4B2105094528CA5EA0085BE82297E2CA9BB43DF7FCCEDDB319F67D7776666786CF2779C2ECCC2CD4F1F9FD9EEF3CE7CC9949930000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000026449224938F1C39B272E7CE9DAFB7B5B5252D2D2D4699476B6B6BB26DDBB6BFA5A3C98C44DDAB7BA839D170DADBDB93AEAEAEE4F2E5CBC6048DD3A74F275BB76E3D9736A03BCC4AD4BDBA879A12EF6C349C8A693C97D3773C07CD4AD4BDBA879A125BA80ABE7246DA74AE9895A87B750F35258E1D2AF68A6A3A895989BA57F7704D369D7FBC7A3239B2EB37C981D607B311B7E33E8D42D341DD1BEA1E8A6E3ADD673B93FD1B16272FAC5B74D588FBE231CD42D341DD1BEA1E8A6A3AC70FFCA15FC3C9C789036B350B4D07756FA87B28AEE91CDCFCD0A04D271ED32C341DD4BDA1EEA1A8A6B3BFE581419B4E3CA659683AA87B43DD83A6A3E980BA57F750B94D27CE421FACE9C4639A85A683BA37D43D14D5743AB6FF7CD0A6138F69169A0EEADE50F75054D339D3B92FD9BF7149FFADD4F4BE784CB3D07450F786BA87A29A4E8CA3BB9FE8D774E23E8D42D341DD1BEA1E8A6F3A972E251DCFFDA2FF566A7A5F3CA659683AA87B43DDC3989B4E5C29F0D0F647073D761B8FB99AA0A683BA37D43D8CBEE9A4EF5C4E766C4AFEF4CCB7076D38F988E7C473BDDBD17450F786BA8711359DE1DED578B7A3E9A0EED5BDBA87A29ACE48DED50CF56E47F3D0748AF42F5E0275AFEEA1C69BCE581B4E3E340F4DA708D3D291F4FC89BA57F750AB4DC7D07426C8BFA7E36C3A96F7FC395D85AA7B750F9A8EA1E98C77D0985EB0C3715AE050F7EA1E341D43D32945D018EE7ED4BDBA074DC7D074466CDA3081227FDC391CEA5EDD537D922479FBBE7DFB9A7FFBDBDF6E5DB66CD9B97BEFBDF7CAECD9B393993367264D4D4D6F7CF5AB5FBDFC9DEF7CE7C8430F3DB4221DFFA6E9189A4EC982C67041C20E87BA57F7545FC858B56AD513DFFFFEF7AF2C58B020F9D5AF7E95ECDCB933E9ECEC4C2E5EBC9884F8B3ABAB2BF9E31FFF98A46124F9DAD7BEF6C6D7BFFEF597E6CD9B3757D331349D713D7432AD44CF47DDAB7B26461A2AE6FCE0073FB8B468D1A22C48BCF6DA6BC948EDDDBB37B9FFFEFB5F6B6E6E3E3863C68C0F6B3A86A65374D0983E86DFEB1638D4BDBAA752773326AF59B366D3FCF9F393B56BD78E2A64F4F5CC33CF2473E6CCB9502BBB1C9A8EA6532541C3211575AFEEA9ECA0B165CB96E7EFB9E79EE4D0A143C97878E9A5979234B85C983B77EE7F683A86A63362C39D0C5AEEBF47D830D43DE32376342268C43918E329FEBE79F3E675D7D7D737693A86A633E280305E8740EC70A87B754F65887334E2D0C978ED680CB4C371E79D779E4D03C7144DC7D074860D0679D078533A66F5FC391A7D7FCF49A3EA5EDD33E1874FDEFEE0830F5E8E73344A69DDBA75171A1B1B77683A86A633A21D88080ABF9EF4FFDF7FF2EB51048EC17E4FE050F7EA9E89B37AF5EAFF8C4F9DFCF39FFF4C4AEDAEBBEE3A55AD8753341D4DA78C416352CFCE4452304612380A83463EEE1CE0108D432AEA5EDD53DE5D8DB88E467CBCB51C5E78E185CBD5BABBA1E9683A253258001828380C1538067AFE63033CDF391CEA5EDD535E7165D03829B4988FB88ED69C39734ECD9C39F3FA4A790DEAEBEB5BD2F1194D47D329F51C1A22684C1B45801828708C34684C724845DDAB7BCA2E2E41FED8638F25E5B47CF9F2BD69D8F846A5BC0671D9F518D17CEAEAEA3EADE9683AA59A43635CF0870B1CA30D1A85FFFE35FB6DB1EA5EDD5346F15D277109F2727AEEB9E75E4A8B7C6DA5359D82B17EA0773C9A8EA653EC1C2AE250C66081E3CD630C1AC31DC2B966C286BA57F794417CA9DAC99327CB1A364E9D3AD59516F6F10A6E3ABDEF780A9B8FA6A3E9143B87C61834860A1C7F29226814FBDF536B6143DDAB7B4A25BEBD35FF52B572897F2F2DEA8B95DE740ADFF1C436ABA6A3E9143B87C6612761A0C0514CD018ED219D9A0F1BEA5EDD53A2829B082328F48A1B9A4E658D6A9B3F5FF8C217F250B0BCC8B27DF3003B1A7FE9B9BF18CBABB12ED5BDB0411598356BD61BE5DED9B870E1C2A92AD9D9D895BEB3993E75EAD4C9D5B89D1A8B504343C3889FBF66CD9AE4231FF988A653C23934A9F84B910FB5B3319A0B7FD9D9A8D2BA3F7CF8F0550128FEBF5FBA74E980CFDDB87163F6F8F9F3E7E3A28A7115E7ACCE6FB9E5962476B5850DCAE69E7BEEB95CEE73365E7EF9E5BF54F8391B7D1788AA3C761B4DE6B39FFDEC808F45C0FCD4A73E958DBC015D77DD75D988DB311E7FFCF1ECEF3871E284A6334E73A86061EF2ED1391B63091CCED9A8B2BA8FFFAF77ECD8919C3D7B36BBBD7DFBF6419FF7C8238F6461236ABEF0FE6F7DEB5BBD3F9F3A752AEB1551F72B56AC486EBCF1C6EC35899FF3FEF0C4134F24EF78C73BD43D63B378F1E28E727F1A65C3860DFF5DA19F46D9515F5F7FDB200B4455868DF7BEF7BD833E1E0DA8A3A323B9E9A69BFA858D952B572653A64CF10E679CE750113B09837DBC75A04FA38C2670F8344A15D5FDFEFDFB9355AB5665751D75FAB6B7BD2DFBF9A31FFD68162A0A9FBB7AF5EADE1D909817DFFCE6377B6FDF7EFBED03F683782C7640F2E7E56F3822704408F9C0073EA0EE199B1FFDE8478F95FB3A1B4B962C595761D7D918EA5D6855868D7817134D229AD150CF8B50F1CE77BEB35FD878CF7BDE33604312368A9B4363DC5118EE3A1AA3BDD268EE8649D7F67536AAAEEED7AF5F9FBCF5AD6FED77FFBBDEF5AEE4D65B6FEDFD79DFBE7DD93C882052B88B11B7E3EF78FAE9A7B3DB9B376FEEF70625EA3FBF1DE126FE7CF2C92763473ABB7DFCF87175CFE87DF7BBDFFDD7BBEFBEFBF5325E41F4624343C3E14ABA82E8485553D8882DD6680CEF7EF7BBAFDA3EED3BE29D4A8CBE61E3E31FFFB8B0511EC3ED2C8CF4825DA30D1CF9A19CDB74C1EAA9FB91868D38BC128123FAC0273FF9C92C34C473E2E7FC39C78E1DCBE649EC809C3B772EABFBF839DE7C3EFAE8A3D9ED783362678371B370E1C2FF29D777A36CD8B061451A34DA359DF29CAF11EF5CE2F6508752F2DB113062E4278BC6EF0D1554349D7133D80EC768AF0C3AD2C091FF7B824615868DF8FF75FEFCF95900C8CFA788FB162C58D0EF904B3C164162F7EEDDD9791DF9F958F921980816F9F3BBBBBBB3BF274E2ACD7B48BE9B113B1BF1781C7655F78CD957BEF29519F7DD77DF9532EC6E9C6D6868D8E35B5FCB7308258EB7E64D63D9B265573DE7E8D1A3C9CD37DF9C8D814E105DBC7871D6BCF2AD5461A32C3B1C7D0F69CC9A34FAEB68F8D6D71AAEFB175F7C311B795D7FEF7BDFCB6E4708E87B2277FEFF7F7B7B7BD613F23716715F1C4619EC4D4A7E182542471E64625723CEED8A5EA1EE294A7373F3BEB56BD7963469C4F921B1AB31CAE3DA9ACE1876350A9BC2DEBD7BB3FBE26CF2C24012A3F0E370CED9A898432AD306080EA3B960D760BF77CD7FF95A35D77D9C2B1101200E8BDC7BEFBDD98EC5A49E4F8BC5C761E3F0CADCB973FBFD5E1C4E29FC54DA5061230EBBC6DFBF75EBD6ECEFCF9F1F3B1BEA9E7111E750CC9933E7C2A143874A1234D209BF26FD374ED6D7D74FD1744A1B34060A09F1CE241E4B83DE55874EF2D011B7A301C531DA7C0B367F07256C4CE8219537F5EC4C8CF6E3AC7D7FAFD8EB7B081B1338CE9C39D3FB11D708044D4D4D573D9EBF9198D473C8343F1F233EA512BB98856F2AE2FE2F7EF18BBDB72358445889DB719D9D387412E124FF286CFEE625BF1D279DAA7B8A924EE0C6E6E6E6F35D5D5DE31A34D2E4FD6C4343C3B16A3D7C520D6123DEF5BCEF7DEFCB4EF61AEE786FDFFF1DF10E269A553CF6F0C30FBBB84FE5058E521CA2A14AEB3E6A3C3F4412E76EA43D3BBB1D3B18431D5A1DEEEF8D733246F37C754F511A1B1BE7CC9B37AF7BBC763862472382465D5DDDFD9A8EA1E9943D705C9317EC52F7EA9E2A900683994D4D4DE77EFFFBDFFFA3889346CFFEF8C73FFE4D1C3A49C76C4DC7D074C6B42351CCA10F874ED4BDBAA7B2C5391C0D0D0D6DCDCDCDA7F6ECD9736134D7D1D8B871E3536958D915278356F3391A9A8EA653418163B43B134E0655F7EA9EAA0A1D7744E8983D7B76D7B265CB0E3CFBECB3C73A3B3B4FE65FDE165FAA16DF75D2DADAFA5F0F3CF0C0FAC6C6C62311326237A35A3F75A2E9683A557E48C5A11375AFEEA94E6988787F7D7DFD823444AC4CC7D1F8D6D69EEF15883F8FC7779DC425C8ABF1CAA09A8EA6534381232E41EECAA0EA5EDD83A663683A633EA4D23DC4A191FC53278286BA57F7A0E9189ACEB8EF703874A2EED53D683A86A653B2C02168A87B750F9A8EA1E9942C702C1734D4BDBA074DC7D0744A655A9CA43DC9C75BD5BDBA074DC7D0744AA5276CA0EED53D683A86A6236CA87B43DD83A6A3E9081BA87B750F9A8EA1E9081BEA5EDD9B95683A86A6236CA87B43DD83A6A3E9081BA87B750F9A8EA1E9081BEADE50F7683A86A6236CA87B43DD83A6A3E9081BA87B750F9A8EA1E9081BEADE50F7A0E9683AC286BA37D43D683A9A8EB081BA57F7A0E9189A8EB0A1EE0D750F9A8EA6236CA0EED53D683A86A6236CA87B750F9A8EA1E9081BEADE50F7304A6D6D6D8ABD72C6C5B4E95C1136840D75AFEEA1A66CDBB6ED6457579782AF80D1D9D9F9BBB4E91C1436840D75AFEEA1A66CDAB4E9D62D5BB6BCFACA2BAF5C50F813F7CE261A4E6B6BEBB174DC216C081BEA5EDD43CD4927FAF43459EF8AADBC387668947DC4EB7E50C31136D4FDE846CC15750F80B081B9028005047305000B08982B005840305700B08060AE006001B18060AE006001C15C01C00282B902001610CC15002C20982B005840305700C00282B9028005047305000B08982B005840305700B08060AE006001017305000B08E60A001610CC1500B08060AE006001C15C01C00282B902001610CC15002C20982B0058403057CC15002C20982B005840305700B08080B9028005047305000B08E60A005840305700B08060AE006001C15C01000B08E60A001610CC15002C20982B006001C15C01C00282B902800504CC15002C20982B005840305700B08080B9028005047305000B08E60A005840305700B08060AE006001C15C01000B08E60A001610CC15002C2060AE006001C15C01C00282B902800504CC15002C20982B005840305700C00282B9028005047305000B08E60A005840305700B08060AE006001C15C01000B08E60A001610CC15002C2060AE006001C15C01C00282B902800504CC15002C20982B005840305700C00282B9028005047305000B08E60A005840305700B08060AE006001017305000B08E60A001610CC15002C2060AE006001C15C01C00282B902001610CC15002C20982B005840305700C00282B9028005047305000B08E68A570180E1168BB5B1600C337679A510360018EB6271433AAE0C1536EAEAEAA67BA51036002866C1583F44D8D83175EAD4C95E25840D008A59306E182C6CD4D7D7DFE61542D800603C168D81CEDDD8655703610380F15A34FA9DBBE15C0D840D00C6551A2E56DBD540D800A09461E3D305E76A4CF38A206C00508AC5233B77C3AE06C20600A55A3C6EB080206C0054A82449261F397264E5CE9D3B5F6F6B6B4B5A5A5A8C328FD6D6D664DBB66D7F4B479319296C00D49C081AEDEDED4957575772F9F2656382C6E9D3A793AD5BB79E4B83C71D66A5B0015053624743D0A898C071B9A5A5E5A059296C00D494387462A1AF9C91868D2B66A5B0015053E29C018B7C45850D0BA1B001706D868D7FBC7A3239B2EB37C981D607B311B7E33E0141D8103600283A6C749FED4CF66F589CBCB06ED15523EE8BC784046143D800A0A8B071FCC01FFA058D7C9C38B056481036840D008A0B1B07373F3468D888C784046143D800A0A8B0B1BFE58141C3463C2624081BC20600C286B081B00150B961233E7D3258D888C784046143D800A0A8B0D1B1FDE783868D784C481036840D008A0A1B673AF725FB372EE97F0825BD2F1E1312840D610380A2C2468CA3BB9FE81736E23E0141D8103600283E6C5CBA94743CF78BFE8750D2FBE2312141D81036001873D8882B841EDAFEE8A0E76CC463AE222A6C5470B0581BE16298B1CB2B05301161E3D2A5E464C7A6E44FCF7C7BD0A0918F784E3CD72E87B0518161E386745C192A6CD4D5D54DF74A0194396C0CB79B619743D8A8B2C0B17E88B0B163EAD4A993BD4A00650E1B23D9CD186A97436810362A707763C0B0515F5F7F9B57086002C2C65883463E840661A30203C740E76EECB2AB01304161C310366A7477E38A733500840D6143D82899345CACB6AB01206C081BC24629C3C6A70BCED598E615011036840DC65D7EEE865D0D006143D8605C343636BEBFAEAEEEEE3460AC4C47473ACEF7EC6E5C4CC7D19E432BDF88E779B500840D6183118B8FB4A6A3251D97477005D17C6C4E9FDFE4D5031036840D06950686EBD3C0F0CC2802C640A37DC68C191FF66A02081BC20657A9ABAB9BD9D0D0F0BF85C1A1B1B131F9E10F7F986CDAB429F9EB5FFF9AFCFDEF7F4FC2F9F3E793CECECE64FBF6EDC9F2E5CB932F7FF9CB7D0347B75D0E006143D8A030684C4FC3C16B852163C58A1549575757321267CF9ECD9E1FBFD7E70AA373BDBA00C286B02168CC2C0C1A0B172ECC7631C6227E6FD1A2457D03871D0E006143D8B856C5391A85874E962C59D27BA864ACE210CBD2A54BFB1E5299E2D5061036848D6B501A345A0B77348A0D1A8581A3CF0E47BB571B40D81036AE31F1F1D6C27334C67AE864A8432A85E770389C02206C081BD75ED868C983409CDC590A4F3DF594DD0D006143D8A8C510918ECF0CF59CB8E2677E5268EC3E9C3973A62461233EA5D2E763B1D7EB1A00C286B051E50A0E5BB4C497A70DF49C9E4B9067CF7BF8E18793528AEB7014848D6FE81A00C286B0512361A360ACEFBBD3D1F35D27D9E35BB66C2969D8880B7F15FCB7ACD53500840D61A3F6C246EF4E471E3A7ABE542DBB7FBC4F0CED2BAE345AF0DF715CD7001036848D1A0D1B853B1DE9B890FF3C5E1F771DEA63B005FFF6455D0340D8A8EA51E417885D93A31C0AFF3D5D03A086C2C6BA75EB92356BD664E3BAEBAE4B56AE5CD9FB587CFA20FDCF4FDADBDBFBFD5E474747F6D8E73EF7B9EC771B1A1A92B973E7DAD9A8FE9D8D5DF11D2853A74E9D1C57F5B4B301206C143D22303CFDF4D3BDB757AD5AD5FB587C2431EEDBB871E380BFF7F9CF7FFEAA9F6FB9E51661A37AC3466FC828788E733600848DF1091B7BF6EC49962D5B96DDFEF39FFF9CDC7CF3CDD96E45048FB8EF431FFA5072F8F0E1AB7EE7C5175FCC3E41103B2131E2BE78772A6C545DD8D81157092D0C1905CFF16914006163FCC2C6FCF9F3B3DB2FBFFC7272F1E2C5DEC762143E3F42C7F1E3C77B7F6E6E6ECE9EB37BF76E27885657D8E8B793D197EB6C00081BE31A36162C5890DD3E76ECD855E764F40D1BF988432B3FF9C94FB2C78F1E3D9ADC75D75DD9ED082DEBD7AFBF2A90081BD529AE209A2EFC57CA7105D1A6A6A6375C4114A086C3C6D6AD5B93C71F7F3CBBBD6FDFBEDEFB9F7FFEF9ECCFCD9B3767F7451089DB7122E94D37DD941D6A89C76FBFFDF6ECF6073FF8C1E4631FFB58F2C8238FF4EE8E081BD5CD77A300081BE312366EBCF1C66C017ECB5BDE929C3B772EBBEF97BFFC65D2DDDD9DDD8E9D8AC2DFC9CFCDD8BB776FF6F8891327FA9D6CEA304ACD848D927FEB6B4343C31BBEF515A086C3461E12E2DC8B385F233E0A9B1F02192C6C14FEDE4F7FFAD3AB7E7EF2C927858D1A938680CD791858B870E1B87D0C3642EB7DF7DDF75AE1AEC650E7900050A5612382C48E1D3B7AC3422C26713B7638F2EB6C0CF4DF1FF77FE94B5FCA168C383934CEDB88FBDADADA848DDA0B1BD7A7E3741E0A962C595274E08879B374E9D2C2F334BAEBEBEBA778B5016A2C6C4440F8C4273ED1FB739C93111F418CDBF1D1D6080FF131D8C2DF89C7E39C8DFDFBF7F7DE1761A330A8081BB5A7AEAE6E667EB268BEC331D6432AF17B7D76341C3E01A8E59D0DDF8DC22876386617068E3887234E1A8D4F938CF45327713268E1391A31D22073BF571740D81036E8DDE148C7D9C2B010A123AEC31117FE8A5D8BFC104B1C2A892B83C66ED8CF7EF6B364D6AC59AFF7B96A695C0E7DB6571540D81036E8BBC3717DE14762C738DA9DA301206C081B0C173AEEE8091D574613326237C3A74E00840D6183118B2B8DA6A16341CF77A91C8D6F6DCDBFBD35BE542DBEEB242E41EECAA000C286B00100C286216C0080B0216C0080B0216C989500081B86B00100C286B00100C286216C00206C18C20600081BC20600081B86B00180B061081B00206C081B00206C18C20600081BC20600081BC20600081B86B00100C286B00100C286216C00206C18C20600081BC20600081B86B001C0B5A5ADADCD225F39E3621A36AE989500D4946DDBB69DECEAEAB2D057C0E8ECECFC5D1A360E9A9500D4944D9B36DDBA65CB96575F79E5950B16FC89DBD188A0D1DADA7A2C1D77989500D49C74819B9EBEA3DE155BF871CE8051F611AFFB414103000000000000000000000000000000000000A002FD1FE254BAD30302D89F0000000049454E44AE426082, 1);
INSERT INTO `act_ge_bytearray` VALUES ('b092050c-8412-11ec-8130-dc41a90b0909', 1, 'bpmn/报销bpmn20.xml', 'b092050b-8412-11ec-8130-dc41a90b0909', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226B65792D627822206E616D653D22E68AA5E994802220697345786563757461626C653D2274727565223E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322206E616D653D22E4BABAE4BA8B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F6578636C7573697665476174657761793E0A202020203C73657175656E6365466C6F772069643D227369642D38314444314543452D314331362D344539372D413934382D3041344633433044454242362220736F757263655265663D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322207461726765745265663D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922206E616D653D22E680BBE7BB8FE790862220666C6F7761626C653A61737369676E65653D2261646D696E2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422206E616D653D22E8B4A2E58AA12220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D44443843353436332D433146362D344637322D393741312D4237353043323131373645362220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E3C2F73657175656E6365466C6F773E0A202020203C656E644576656E742069643D227369642D43394630304343332D394539462D343439392D424533362D37384337454132453835433622206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C73657175656E6365466C6F772069643D227369642D44463034354545392D423844362D343539332D423144342D4630334630393635423846302220736F757263655265663D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D46393639444233442D384236452D344441342D383036412D4436343130413431333537382220736F757263655265663D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D39463343323144342D303136302D343344312D393432332D39463835354541434545313322206E616D653D22E5B08FE4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793C313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D34303945343536372D423445442D343745382D383741352D32313335413346313038413322206E616D653D22E5A4A7E4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793E313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6B65792D6278223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226B65792D6278222069643D2242504D4E506C616E655F6B65792D6278223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433222069643D2242504D4E53686170655F7369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341222069643D2242504D4E53686170655F7369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223332302E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139222069643D2242504D4E53686170655F7369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223432392E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534222069643D2242504D4E53686170655F7369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223239302E302220793D223331352E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336222069643D2242504D4E53686170655F7369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223436352E302220793D223334312E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630222069643D2242504D4E456467655F7369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223231372E3935303030303030303030303032223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223334312E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738222069643D2242504D4E456467655F7369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338392E39353030303030303030303030352220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223436352E302220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536222069643D2242504D4E456467655F7369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236222069643D2242504D4E456467655F7369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E39343939393939393939393830362220793D223137382E3231363233333736363233333738223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223332302E343133303433343738323630392220793D223137382E34313330343334373832363039223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133222069643D2242504D4E456467655F7369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223335392E353131373438373332373937362220793D223137382E3433313135393432303238393834223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223432382E393939393939393939393838372220793D223137382E31383033323439303937343733223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133222069643D2242504D4E456467655F7369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E34343630323237323732373237352220793D223139372E3439383432343139303830303635223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E31313331373238303435333235352220793D223331352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('b0c1797d-8412-11ec-8130-dc41a90b0909', 1, 'bpmn/报销key-bx.png', 'b092050b-8412-11ec-8130-dc41a90b0909', 0x89504E470D0A1A0A0000000D494844520000021B00000195080600000057753388000014474944415478DAEDDDDF6F9475BE07F0EE868BBD3C7FC05E909C4D8E7F00175EEC452FCE0517246B0C643A6D155209558F288224649594A044D0181277938D091BA361E326C6A0DBC3E1477F585AF991D3D5080B21C29E82422D0CCBA98A146A91679F6F4F8733B69D76A6CFCC7466FA7A259FE8B6653193EFE7F37DF7FB3CF34C4303000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C0A288A268D9D0D0D0FBA74E9DFAB1A7A727EAEAEA5215AEEEEEEEA8BFBFFF1F71B55A91E87B7D0F75270C9C8181812893C944E3E3E36A91EAC68D1BD1B163C7BE8D07D06AAB127DAFEFA1AE84DF6C0C9CAA193CE3F16F3CE7AD4AF4BDBE87BA128E50357CF5543C7426AC4AF4BDBE87BA12AE1D6AF6AA1A3A915589BED7F7B02487CEF7DF8C4443836F47E7BA774E56F8F7F03583C2D041DF2B7D0F8987CEADD1E1E8EC91EDD1E9835B7F52E16BE17B8685A183BE57FA1E120D9D2BE7FE3263E064EBEAB94EC3C2D041DF2B7D0FC986CEF98FF7E41D3AE17B8685A183BE57FA1E120D9DB35D3BF20E9DF03DC3C2D041DF2B7D0F868EA103FA5EDF43F50E9D70177ABEA113BE6758183AE87BA5EF21D1D0B978FCF779874EF89E6161E8A0EF95BE874443E7E6F099E8ECD18E9947A9F1D7C2F70C0B43077DAFF43D241A3AA12EFDF5DD1943277CCDA03074D0F74ADF43F2A173F76E74F1C41F661EA5C65F0BDF332C0C1DF4BDD2F7B0E0A1139E1478E1F8EFF25EBB0DDFF3344143077DAFF43D143F74E2DF5C462EF6467F3BFCDBBC03275BE167C2CFFA6DC7D041DF2B7D0F050D9DF97EABF1DB8EA183BED7F7FA1E120D9D427EAB99EBB71DC3C3D049E85FBC04FA5EDF439D0F9D850E9C6C191E864E022BE38AA6FE89BED7F750AF4347193A8BE4DFE31A8D6BEFD43F57E9507DAFEFC1D051864EA983C6AA9C138E1B0287BED7F760E82843A71C4163BEAFA3EFF53D183ACAD029D8CA790245F6FBEEE1D0F7FA9EDA1345D12FCE9C39D3FECE3BEF1CDBBD7BF7B7CF3EFBECC4BA75EBA2A6A6A6A8B5B5F5FE534F3D35FEE28B2F0EEDD9B3677F5CFF66E82843A76C4163BE20E18443DFEB7B6A2F647CF0C107EFBEFCF2CB139B366D8ADE7AEBADE8D4A953D1F0F07074E7CE9D2808FFCC6432D1A79F7E1AC561247AEEB9E7EE3FFFFCF397376CD8B0DED051864E492F9DAC2CD3CFA3EFF53D8B230E156DAFBCF2CADDAD5BB74E06897BF7EE4585FAFCF3CFA36DDBB6DD6B6F6F3FBF66CD9A5F193ACAD0491C34562DE0CFDD1238F4BDBEA75A4F33967DF4D147BD4F3EF964D4D9D95954C898EEF0E1C3515B5BDB58BD9C72183A864E8D040D9754F4BDBEA7BA83465F5FDF7F6FDCB831BA70E142540A972F5F8EE2E032B67EFDFAFF307494A153B0F96E06ADF4FF8FB0A1F43DA5114E3442D008F7609452F8FFDBB061C3AD743ADD6AE82843A7E08050AA4B204E38F4BDBEA73A847B34C2A593529D68CC76C2F1F8E38F8FC681E3214347193AF306836CD0F8595C6BA7FE598CE97FCE4DA3FA5EDFB3E8974F7EB173E7CEF1708F46391D3C7870ACA5A5E5A4A1A30C9D824E204250F863C3FF7DFEC91F8B081CF9FE9CC0A1EFF53D8BE7C081037F0AEF3AF9E1871FA2727BFAE9A7AFD5EAE51443C7D0A960D068983A998872AA90C0911B34B2F5F82C97685C52D1F7FA9ECA9E6A84E76884B7B756C2E9D3A7C76BF574C3D03174CA245F00982D38CC153866FBF97DB3FCBC7B38F4BDBEA7B2C29341C34DA149DEE25AACB6B6B66B4D4D4DCBABE53548A7D35D71FDDAD03174CABD86E6081A2B8B0810B3058E428346834B2AFA5EDF5371E111E4FBF6ED8B2A69EFDEBD9FC761E3856A790DC263D74385E1934AA51E36740C9D72ADA1056EF8F3058E628346EEDFBF643F2D56DFEB7B2A287CD649780479259D3871E272DCE49DD5367472EAD06CBFF1183A864ED23594E05246BEC0F1F305068DF92EE12C99B0A1EFF53D15103E546D6464A4A261E3DAB56B99B8B1AF54F1D079F01B4FEEF031740C9DA46B68814163AEC0F14582A091F4BFA7DEC286BED7F7944BF8F4D6EC87AA554AF8FBE2A6BE53ED4327F7379E70CC6AE8183A49D750094E12660B1C498246B19774EA3E6CE87B7D4F991A6E3114D0E85557864E7555ADAD9F471F7D341B0AF6266CDB9FCF72A2F1C5D4D793D85B8B7DA9EF850D6AC0DAB56BEF57FA64636C6CEC5A8D9C6C0CC6BFD9AC6A6C6C5CE638D5D029C51A6A48FE28F2B94E368A79F097930D7DAFEFA99C8D1B378E57FA9E8DAFBFFEFA8B2ABF6763FA065193D76EF36C48057FDFD029FD1ACAD9D86F95E99E8D85040EF76CD470DF27EDF35A9B03C2468DDABE7DFBC54ABF1BE5C89123FF55A5EF4639994EA71FC9B341D464D898EB6BF37D5FD828FD1A4A709290EFEDADB3BD1BA598C0E1DD2835DEF749FBBCD6E680B051A35E7FFDF57D957ECE464747C7C12A7BCEC65CBF850A1B864EC9D6D0024F14E67B8E46B14F1ACD5AD1B0B49FB351177D2F6C50135E7AE9A57F7DE699677EACE01344EF343737FF4F353D41B450C286A15306F39D2C14FAC0AE620347F652CE23A6606DF7BDB041CDD8BC79F3DF2BF5D928478E1CD91F078D014347D83074E63DE128F6C9A085068EECDF2768081BC20695F3C4134FACD9B265CB44054E37469B9B9B3FF3A9AFC286A133EB09C7F44B1A6B1B8A7F8E864F7D1536840DAA577B7BFB99CECECEB2268D707F4838D528F2BAB6A1236C2C95A133FD6DB1B9C1A1980776E5FB734BFEC3D7840D61834516EEA1686B6B1BBB70E1425982C69933673E8AFF8E91743AFD90A1E3ADAF864EC197547E36753251ECDB59A7FFB9A4CFF71036BCF555DF531AADADAD2DEDEDEDB733994C4983C6D5AB573F696E6EFEB2562F9FD46AD8F0709FBA091CE5B84483BED7F72C9E969696B60D1B36DC2AD5094738D1084123954A6D337494A153F1C0B1241FD8A5EFF53D35200E064DADADADDF7EF8E187DF27B86974F48D37DE783B5C3A896B9DA1A30C9D059D4824B9F4E1D289BED7F754B7700F477373734F7B7BFBB5CF3EFB6CAC98E7681C3D7AF4BD38AC0C869B416BF91E0D43C7D0A9A2C051ECC9849B41F5BDBEA7A642C7EA103AD6AD5B97D9BD7BF7B94F3EF9E4CBE1E1E191EC87B7850F550B9F75D2DDDDFD9F3B76EC38D4D2D23214424638CDA8D5779D183A864E8D5F5271E944DFEB7B6A531C227E994EA737C521E2FDB82E854F6D9DFA5C81F0CF2BE1B34EC223C86BF1C9A0868EA1534781233C82DC9341F5BDBE074347193A0BBEA4726B8E4B23D9779D081AFA5EDF83A1A30C9D929F70B874A2EFF53D183ACAD0295BE01034F4BDBE074347193A650B1C7B050D7DAFEFC1D051864EB9AC0C376937787BABBED7F760E82843A75CA6C206FA5EDF83A1A30C1D6143DF2B7D0F868EA1236CA0EFF53D183ACAD01136F4BDBEB72A317494A1236CE87BA5EFC1D03174840DF4BDBE074347193AC286BE57FA1E4347193AC286BE57FA1E0C1D4347D840DFEB7B307494A1236CE87BA5EFC1D03174840D7DAFF43D183A868EB081BED7F760E8284347D8D0F74ADF83A163E8081BE87B7D0F868E3274840D7DAFEFC1D051868EB0A1EF95BE8722F5F4F468F6EAA93BF1D0991036840D7DAFEFA1AEF4F7F78F6432190D5F05353C3CFCE778E89C1736840D7DAFEFA1AEF4F6F6FEA6AFAFEF9BEBD7AF8F69FCC5FBCD260C9CEEEEEE2FE35A2D6C081BFA5EDF43DD8917FAAA38590F86A3BC70ED5055BCC2EB7EDEC01136F47D7115D68ABE0740D8C05A01C00682B502800D04AC15006C20582B00D840B05600B081D840B05600B08160AD006003C15A01001B08D60A003610AC15006C20582B006003C15A01C00682B502800D04AC15006C20582B00D840B05600B08180B502800D046B05001B08D60A00D840B05600B08160AD006003C15A01001B08D60A003610AC15006C20582BD60A003610AC15006C20582B00D840C05A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610AC1500B08160AD006003C15A01C00602D60A003610AC15006C20582B00D840C05A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610B05600B08160AD006003C15A01C00602D60A003610AC15006C20582B006003C15A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610B05600B08160AD006003C15A01C00602D60A003610AC15006C20582B006003C15A01C00682B502800D046B05006C20582B00D840B05600B08180B502800D046B05001B08D60A003610B05600B08160AD006003C15A01001B08D60A003610AC15006C20582B006003C15A01C00682B502800D046BC5AB00C07C9B4567D830E6A941AF14C206000BDD2C56C4353157D848A552ABBC52081B0024D9300ECD11364E3636362EF32A216C009064C358912F6CA4D3E947BC42081B009462D398EDDE8D41A71A081B00946AD39871EF867B3510360028A9385C1C70AA81B0014039C3C6C339F76AACF48A206C00508ECD63F2DE0DA71A081B00946BF358610341D800A85251142D1B1A1A7AFFD4A9533FF6F4F4445D5D5DAAC2D5DDDD1DF5F7F7FF23AE562B52D800A83B21680C0C0C44994C261A1F1F578B54376EDC888E1D3BF66D1C3C565B95C206405D09271A8246D5048EF1AEAEAEF356A5B0015057C2A5131B7DF5541C3626AC4A6103A0AE847B066CF25515366C84C206C0D20C1BDF7F33120D0DBE1D9DEBDE3959E1DFC3D704046143D8002071D8B8353A1C9D3DB23D3A7D70EB4F2A7C2D7C4F481036840D0012858D2BE7FE32236864EBEAB94E2141D8103600481636CE7FBC276FD808DF1312840D6103804461E36CD78EBC61237C4F481036840D00840D61036103A07AC34678F749BEB011BE2724081BC2060089C2C6C5E3BFCF1B36C2F784046143D8002051D8B8397C263A7BB463E62594F86BE17B4282B0216C0090286C84BAF4D77767848DF0350141D8103600481E36EEDE8D2E9EF8C3CC4B28F1D7C2F784046143D80060C161233C21F4C2F1DFE5BD67237CCF5344858D2A0E169D215CCC53835E2980C5081B77EF4623177BA3BF1DFE6DDEA091ADF033E1679D72081B55183656C4353157D848A552ABBC5200150E1BF39D6638E510366A2C701C9A236C9C6C6C6C5CE65502A870D828E43463AE530EA141D8A8C2D38D59C3463A9D7EC42B04B0086163A141235B4283B051858163B67B37069D6A002C52D850C2469D9E6E4CB8570340D81036848DB289C3C501A71A00C286B0216C94336C3C9C73AFC64AAF0880B0216C5072D97B379C6A00081BC20625D1D2D2F2CB542AF54C1C30DE8FEB625CB7A74E37EEC47569EAD2CA0BE1E7BC5A00C286B041C1C25B5AE3EA8A6BBC80278866EBE3F8E75BBD7A00C286B0415E7160581E0786C345048CD96A60CD9A35BFF26A02081BC2063F914AA59A9A9B9BFF373738B4B4B444AFBEFA6AD4DBDB1B7DF5D557D177DF7D1705B76FDF8E868787A3E3C78F477BF7EE8D1E7BECB1E981E396530E006143D8203768AC8AC3C1BDDC90B17FFFFE2893C94485181D1D9DFCF9F0E7A63D6174BD571740D81036048DA6DCA0B179F3E6C9538C85087F6EEBD6ADD30387130E006143D858AAC23D1AB9974E3A3A3A1E5C2A59A8708965D7AE5DD32FA93CE4D5061036848D25280E1ADDB9271A4983466EE09876C231E0D5061036848D2526BCBD35F71E8D855E3A99EB924AEE3D1C2EA700081BC2C6D20B1B5DD920106EEE2C87F7DE7BCFE90680B0216CD4638888EBD773FD4C78E267F6A6D070FA70F3E6CDB2848DF02E95696F8B5D6E6A00081BC2468DCBB96CD1153E3C6DB69F997A04F9E4CFBDF6DA6B513985E770E4848D174C0D006143D8A893B0915387A69F744C7DD6C9E4F7FBFAFACA1A36C283BF72FE5B3A4D0D006143D8A8BFB0F1E0A4231B3AA63E546DF2EBA5BE3174BAF0A4D19CFF8E2BA60680B0216CD469D8C83DE9886B2CFBBF4BF576D7B9DE069BF377DF313500848D9AAE841F20B624AB1272FF3E530340D870B251BF271B83E133501A1B1B9785A77A3AD9001036CA52F17FF2AC55E8F7858D9A0C1B0F4246CECFB8670340D8285FD898EB6BF37D5FD8A8A9B071323C25343764E4FC8C77A300081BC286B0B1E0B031E324633ACFD9001036840D61A3ACC21344E38D7FA2124F106D6D6DBDEF09A200C286B0B104F96C14006143D81036CA1D36CAFEA9AFCDCDCDF77DEA2B80B0216C2C617108F8381B06366FDE5CB2B7C186B7BB6ED9B2E55EEEA9C65CF79000506761A3C15B5FF9FFB0B13CAE1BD950D0D1D191387084A0B16BD7AEDCFB346EA5D3E987BCDA004B246C78A817D3A552A9A6ECCDA2D9138E855E52097F6EDA8986CB2700C286B0C1E409C7BADCC011EEE108378D86779314FAAE93703368EE3D1AA1E220B3CDAB0B206C081B3C38E1886B34372C84D0119EC3111EFC154E2DB29758C2A592F064D0F0C0AE37DF7C335ABB76ED8FD39E5A1A1E87BECEAB0A206C081B4C3FE1589EFB96D805D6807B3400840D6183F942C7EAA9D031514CC808A719DE7502206C081B142C3C69340E1D9BA63E4BE552F8D4D6ECA7B7860F550B9F75121E41EEC9A000C286B00100C28612360040D810360040D81036AC4A00840D256C0080B0216C0080B0A1840D00840D256C0080B0216C0080B0A1840D00840D256C0080B0216C0080B0A1840D001036840D001036840D00103694B00100C286B00100C286123600103694B00100C286B00100C286123600585A7A7A7A6CF2D55377E2B0316155025057FAFBFB4732998C8DBE0A6A7878F8CF71D8386F550250577A7B7B7FD3D7D7F7CDF5EBD7C76CF88B77A21182467777F79771ADB62A01A83BF106B72AFE8D7A301CE1877B0654C52BBCEEE7050D000000000000000000000000000000000000802AF44F5FC8B304C780B2D30000000049454E44AE426082, 1);
INSERT INTO `act_ge_bytearray` VALUES ('d8b2f847-8381-11ec-b633-dc41a90b0909', 1, 'bpmn/请假bpmn20.xml', 'd8b2f846-8381-11ec-b633-dc41a90b0909', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226B65792D3122206E616D653D22E8AFB7E581872220697345786563757461626C653D2274727565223E0A202020203C646F63756D656E746174696F6E3EE8AFB7E58187E6B581E7A88B3C2F646F63756D656E746174696F6E3E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D34313441314338412D413743302D344336422D393644462D38323738313330323642383422206E616D653D22E78FADE995BF2220666C6F7761626C653A61737369676E65653D2279716D6D2220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE58583E6B0945D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE6BBA1E6BBA15D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D33393131423530442D393931412D344633342D424236342D4143363935413338303335422220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D33363942393432352D443446452D343738362D383636382D38433331393441303338324622206E616D653D22E88081E5B8882220666C6F7761626C653A61737369676E65653D22736F6E6778792220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE58588E998B35D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE5AE8B5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D42464538363736352D384337332D344539432D384536442D3446384330343639313546322220736F757263655265663D227369642D34313441314338412D413743302D344336422D393644462D38323738313330323642383422207461726765745265663D227369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D34394332324534362D453634452D344137412D383243342D32333642424341393335343422206E616D653D22E78FADE4B8BBE4BBBB2220666C6F7761626C653A61737369676E65653D2261646D696E2220666C6F7761626C653A666F726D4B65793D226B65792D73702220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D34453744384542372D323932452D344133332D413743452D3439364531303634423036372220736F757263655265663D227369642D33363942393432352D443446452D343738362D383636382D38433331393441303338324622207461726765745265663D227369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434223E3C2F73657175656E6365466C6F773E0A202020203C656E644576656E742069643D227369642D31353834413032392D393932302D343239432D414637312D30424233384439434538383222206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C73657175656E6365466C6F772069643D227369642D46443242443237382D454438432D343737302D394435442D3534383137414631413637322220736F757263655265663D227369642D34394332324534362D453634452D344137412D383243342D32333642424341393335343422207461726765745265663D227369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832223E3C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6B65792D31223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226B65792D31222069643D2242504D4E506C616E655F6B65792D31223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834222069643D2242504D4E53686170655F7369642D34313441314338412D413743302D344336422D393644462D383237383133303236423834223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246222069643D2242504D4E53686170655F7369642D33363942393432352D443446452D343738362D383636382D384333313934413033383246223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223332302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434222069643D2242504D4E53686170655F7369642D34394332324534362D453634452D344137412D383243342D323336424243413933353434223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223436352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832222069643D2242504D4E53686170655F7369642D31353834413032392D393932302D343239432D414637312D304242333844394345383832223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223631302E302220793D223136342E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D33393131423530442D393931412D344633342D424236342D414336393541333830333542222069643D2242504D4E456467655F7369642D33393131423530442D393931412D344633342D424236342D414336393541333830333542223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D42464538363736352D384337332D344539432D384536442D344638433034363931354632222069643D2242504D4E456467655F7369642D42464538363736352D384337332D344539432D384536442D344638433034363931354632223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E393439393939393939393930372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223331392E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46443242443237382D454438432D343737302D394435442D353438313741463141363732222069643D2242504D4E456467655F7369642D46443242443237382D454438432D343737302D394435442D353438313741463141363732223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223536342E39352220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223631302E302220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D34453744384542372D323932452D344133332D413743452D343936453130363442303637222069643D2242504D4E456467655F7369642D34453744384542372D323932452D344133332D413743452D343936453130363442303637223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223431392E39343939393939393939393036372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223436342E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('d9474908-8381-11ec-b633-dc41a90b0909', 1, 'bpmn/请假key-1.png', 'd8b2f846-8381-11ec-b633-dc41a90b0909', 0x89504E470D0A1A0A0000000D4948445200000288000000E408060000001BE0D5B200000CB44944415478DAEDDD6D6F54651A07705FF8C20FB12F487693F523EC8B7DE10730595F488A9D519A4A3AEA02629184AC921294081A43A29B6C4C488C860D9B18836C9795D207CBA3645909742564CB2EA2505B8765AB2233D62267EFBBE9B863A138947666CE99DF2FB90274CA43CABFD77D9DFB9C39E79E7B000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000048912449EE3D7FFEFC3BC78F1FFF7E707030E9EFEF5775AE818181E4D0A143FF09959721254332244340C3C5A67CF8F0E1A4582C26535353AA4175F9F2E5E4E0C1835F8526FDB00C291952320434543C62D7949BA6394F8523F9B332A46448C910D050F1748EA6D83C151AF3B40C29195232043454BCF644436CAAC69CC89092212543402A1AF3375F8E27E74FBC999C19D83253F1E7F1639AA9C62C433224433204B4E0807875722CF9B86F53727ADF861F55FC587C4D43B5B8CB900CC9900C012D36205E3CF3E79B9A72A52E9DE9D5502DEE32244332244340AB0D88673FD83E6F638EAF69A81677199221199221A0C506C48FFB37CFDB98E36B1AAAC55D86644886640830206ACC1AB30CC9900CC910D0CA03627CB7E07C8D39BEA6A15ADC65488664488680161B10CF1DFDFDBC8D39BEA6A15ADC65488664488680161B10AF8C8D241F1FE8B9F9B44EF8587C4D43B5B8CB900CC9900C012D3620C6FAE4EF6FDFD498E3C734538D5986644886640868C501F1DB6F9373C7FE70F3699DF0B1F89A866A71972119922119025A68408C4F28183DFAFABCD7FEC4D73CC5C0E22E433224433204B4C280188EC8C7CF0D25FFD8FFBB799B72A5E2E7C4CF75146F7197211992211902323A20FED4D1BAA3788D5986644886640868B101B196A3F5DB1DC56BB01677199221199221206303E2429B72A534588BBB0CC9900CC91090B1015169CC32244332244380015169CC32244332A40C8880C6AC31CB900CC9903220021AB3C62C43322443CA8008F34B92E4BE919191C25B6FBD7570DBB66D5FAD5DBB76BAA3A32359B1624592CFE76F3CF9E49353CF3DF7DCF9EDDBB7EF0AF54B03A2D298654886644886D22B97CBFDACADAD6D7558E7DF09752ED4D5B8E6872A87FA24BCB627FCF86CFC3C53528B0E86EFBEFBEEDB2FBCF0C2F4BA75EB9237DE7823397EFC783236369694CBE5248A3F168BC5E4A38F3E4AC200993CFDF4D3379E79E6990B5D5D5DAB0C884A639621199221194A8F471E79E4A150FDA1A66607C25AEA83F0F97953538B088360E78B2FBEF8ED860D1B6686BFEBD7AF27B53A75EA54B271E3C6EB8542E1ECF2E5CB7F6140541AB30CC9900CC950F30A43DEB230E4EDBF83A1F05675384B6B3E37EF1ADEBB77EFDEA1279E7822E9EDEDBDA3C170AEFDFBF7279D9D9DA5ACEC266ACC1AB30CC9900CA9AC0D886D6D6D2BDADBDBFF5B3DECE572B9E4A5975E4A86868692CF3EFB2CF9FAEBAF67D6F56BD7AECD9C453C7AF468B263C78EE4D1471F9D3B245EB59B98D1E1707878F86F6BD6AC4946474793C570E1C285240C9BA555AB56FDD680A82CEE3224433224434D351C3E1806BAEBD583E1AE5DBB662E1DABC5E4E4E4CCE7C7DF573D28863F7395A92A43E2CE611C0E6B0D46ADE29FD7D5D595FAA30A8D5963962119922195950131EE1C560F87DDDDDD33BB850B117F5FBC246DCE906827310BE23587F1B4F262ED1CDE6A27F1B1C71E9B0C81B9DF80A82CEE4A866448861A275E73587D5AB9A7A7E787D3C80B154F3F6FDDBA75EEE9E6FB4D58E93EB57CDF962D5BA6E235874B69DFBE7DA55C2EF7A1015159DC950CC9900C354E180E07AA770EEF7638AC1E12E7EC241E3665A5D89E3D7BFE18FF43BFFBEEBB64A93DF5D4531369DD76D6983566199221195269CF50BC954DF535870B3DAD7CBBD3CDD5D7243AD59CE2DDC3789FC3782B9B7A387DFAF4545A771135668D5986644886540606C4FECAF016DF60B21476EFDE6D1731EDE21352E21B53EEE6763677AAB3B373225EFFD04CDF2CA17EAD316BCC32244332A4B29CA1F8E493CA1B53E22EDF952B5796649D8FEF6E9E730B9C6526AE94898FCFDBB97367524F3B76EC38151FD1D32C5F83AA6DF0FEB6B6B65F69CC1AB30CC9900CA92C6668F6F179339FF7F2CB2F2FF55A5F3D203E6BE24A99F86CE5F8F8BC7A3A76ECD8851096DE66FBA6AAAAF76F7514A6316BCC32244332A4D29CA1D9672BCFBC3E3C3CBCA46B7DBC9976D5BFA5D7C495326BD7AE9D1E1F1FAFEB80383131510C61B9D8C4DF543F1C85557F7369CC1AB30CC9900CA9346728FCFA5CE5E38BFDE694B9E21357AAFE1D174D5C29D3D1D19194CBE5BA0E88F1EF0B612937FB3755F55158DCAED7983566199221195269CE50A852E5D78B756B9BDBDDF2A6EAEF2E9BB85226FEC735C25D3E10BC21A5313757C990922125430BAF7AAFF526AE9459B972E58D7AEF20964AA58994EC209E88CFA97CE08107EE756AC791BB0CC9900CA9B467283EDDC40E223559B366CD54BDAF41FCFCF3CFFFD9E4D720FEA821A7F5DA9FF04FBE65D5FABAC62C43322443F5CE4CDA3295B60CB906919A6DDAB4E95CBDDFC5DCD7D7F7D7267D17F387F10EF3731B729A17F7DB7DECA75EB7B8CB900CC950BD3393B64CA52D43DEC54CCD5E79E5959DF5BE0F624F4FCFBE26BB0FE22D8FD42DEE1677199221193220662943EE8348CD9E7FFEF99FAF5EBDFAFB3A3E49A5DCDEDEFEEF34DE55DDE2AE31CB900CC9900131CD198A4F5209EBEF743D9EA492CFE76F78924ACA757777FFAB5ECF62EEEBEBDB95D6E7325ADC35661992211932207A16B36731B78CC71F7F7CF9FAF5EBA7EBB08B38D9DEDE7E3284336F40B4B85BDC6548860C8806C4860C880F5586B7B88BB8D86F56897F5E58EB6F54DDA83B6FD24AB142A130D2DBDBBBA4D361BCDE311E49FCD435361AB3C5DDE22E4332644034202E9DB0167F5019E0BABBBB17ED9637F1D636EBD7AFBF5EBD7B98D6359FFF876559676767697474744986C3919191BDE1EF180F4712F7A7F56BE416251AB30CC9900CB9CD4D4606C465A12E5706B99E9E9EBB1E12E370B875EBD6EAEB0EAFA679CDA74A3E9FCF150A856BC562715187C34B972E1D696F6FFF34EDDBCC6E50AB31CB900CC990CAC28018B5B5B5ADA8BC61A5B293B8D0D3CDF1F7CDD939746A396B72B95C675757D7D5C5DA498C3B8771380C41DC98F6AF8DC6AC31CB900CC990CACA8038BB93D8513D24C66B12E31B57E2BB906B7DB7727C434AF53587B1B2B0E633CF51453E9FFFEABDF7DEFBE62EDEB832F9EAABAFBE194F2BC70066E1EBA2316BCC32244332A4B2342056D6FC5093D5035E1C14E37D12E3CDB4E3EE60E5F4733C8D1C9F90126F82FDDA6BAF252B57AEFC7ECED35BAE6665CDE736D727842382C142A13071F2E4C9D29DDCE7F0C08103BBC38079225E9C9AA5EB0F34668D598664488654D606C4CA9A5F7DFB9B05D661D71CB6D6A0F8701C143B3A3A8ADBB66D3B73E4C8914FC3D1C378B95C9E99064BA5D2447CB6F2C0C0C05F366FDEFC7E38EA381F43128F20B2F6CE258D5963962119922195C501B17ACD9F1D14A7EF6430CCE29A4F8DE2DDD74368D6CD3EC7F19350E5D960C41F2FC6E72CC647E964F96EE91AB3C62C433224432ACB03A2351F34668D5986644886940111D0983566195232A40C8880C6AC31CB909221254380C6AC31CB909221254380C6AC31CB909221254380C6AC2CEE4A86940C011AB3B2B82B19922119023466657157322443320468CCCAE2AE6448866408D09895C55DC9900CC9106040541677254332244380015169CC32244332244380015169CC32244332244380015169CC32244332244380015169CC32244332244380015169CC32244332A40C8880C6AC31CB900CC9903220021AB3C62C43322443CA800868CC1AB30CC9900C290322A0316BCC32244332A40C8880C6AC31CB909221654004EEDEE0E0A086D83C550E8D795A86940C2919021AEAD0A143E3C56251536C821A1B1BFB5368CC676548C9909221A0A18686867E333C3CFCE5175F7C51D21C1B77C41E9BF2C0C0C0A7A11E962125434A8680860BCDE0C170C478229E5688D79EA8BA57FCBA9F4D735396211992211902000000000000000000000000000000000000000000009AC4FF004732AE6739C0B0980000000049454E44AE426082, 1);
INSERT INTO `act_ge_bytearray` VALUES ('e0690d4a-8425-11ec-b924-dc41a90b0909', 1, 'bpmn/报销bpmn20.xml', 'e0690d49-8425-11ec-b924-dc41a90b0909', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E63652220786D6C6E733A7873643D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612220786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F44492220786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44492220747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F585061746822207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F63657373646566223E0A20203C70726F636573732069643D226B65792D627822206E616D653D22E68AA5E994802220697345786563757461626C653D2274727565223E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322206E616D653D22E4BABAE4BA8B2220666C6F7761626C653A61737369676E65653D22247B68725573657249647D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F6578636C7573697665476174657761793E0A202020203C73657175656E6365466C6F772069643D227369642D38314444314543452D314331362D344539372D413934382D3041344633433044454242362220736F757263655265663D227369642D39354136354438312D384543322D343034362D413144302D34424145373242333336443322207461726765745265663D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E3C2F73657175656E6365466C6F773E0A202020203C757365725461736B2069643D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922206E616D653D22E680BBE7BB8FE790862220666C6F7761626C653A61737369676E65653D2261646D696E2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422206E616D653D22E8B4A2E58AA12220666C6F7761626C653A61737369676E65653D22247B66696E616E63655573657249647D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C73657175656E6365466C6F772069643D227369642D44443843353436332D433146362D344637322D393741312D4237353043323131373645362220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E3C2F73657175656E6365466C6F773E0A202020203C656E644576656E742069643D227369642D43394630304343332D394539462D343439392D424533362D37384337454132453835433622206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C73657175656E6365466C6F772069643D227369642D44463034354545392D423844362D343539332D423144342D4630334630393635423846302220736F757263655265663D227369642D36304435453338352D373141392D344343352D424641452D45333032454646303442413922207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D46393639444233442D384236452D344441342D383036412D4436343130413431333537382220736F757263655265663D227369642D37444541423733312D393134302D343532372D413930352D33463930303231363631353422207461726765745265663D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D39463343323144342D303136302D343344312D393432332D39463835354541434545313322206E616D653D22E5B08FE4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793C313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D34303945343536372D423445442D343745382D383741352D32313335413346313038413322206E616D653D22E5A4A7E4BA8E313030302220736F757263655265663D227369642D43424131443234462D313133352D343039362D394143412D35323846433145433034334122207461726765745265663D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B6D6F6E65793E313030307D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6B65792D6278223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226B65792D6278222069643D2242504D4E506C616E655F6B65792D6278223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433222069643D2242504D4E53686170655F7369642D39354136354438312D384543322D343034362D413144302D344241453732423333364433223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341222069643D2242504D4E53686170655F7369642D43424131443234462D313133352D343039362D394143412D353238464331454330343341223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223332302E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139222069643D2242504D4E53686170655F7369642D36304435453338352D373141392D344343352D424641452D453330324546463034424139223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223432392E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534222069643D2242504D4E53686170655F7369642D37444541423733312D393134302D343532372D413930352D334639303032313636313534223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223239302E302220793D223331352E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336222069643D2242504D4E53686170655F7369642D43394630304343332D394539462D343439392D424533362D373843374541324538354336223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223436352E302220793D223334312E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630222069643D2242504D4E456467655F7369642D44463034354545392D423844362D343539332D423144342D463033463039363542384630223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223231372E3935303030303030303030303032223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223437392E302220793D223334312E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738222069643D2242504D4E456467655F7369642D46393639444233442D384236452D344441342D383036412D443634313041343133353738223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223338392E39353030303030303030303030352220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223436352E302220793D223335352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536222069643D2242504D4E456467655F7369642D44443843353436332D433146362D344637322D393741312D423735304332313137364536223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236222069643D2242504D4E456467655F7369642D38314444314543452D314331362D344539372D413934382D304134463343304445424236223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E39343939393939393939393830362220793D223137382E3231363233333736363233333738223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223332302E343133303433343738323630392220793D223137382E34313330343334373832363039223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133222069643D2242504D4E456467655F7369642D34303945343536372D423445442D343745382D383741352D323133354133463130384133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223335392E353131373438373332373937362220793D223137382E3433313135393432303238393834223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223432382E393939393939393939393838372220793D223137382E31383033323439303937343733223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133222069643D2242504D4E456467655F7369642D39463343323144342D303136302D343344312D393432332D394638353545414345453133223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E34343630323237323732373237352220793D223139372E3439383432343139303830303635223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223334302E31313331373238303435333235352220793D223331352E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('e0980c8b-8425-11ec-b924-dc41a90b0909', 1, 'bpmn/报销key-bx.png', 'e0690d49-8425-11ec-b924-dc41a90b0909', 0x89504E470D0A1A0A0000000D494844520000021B00000195080600000057753388000014474944415478DAEDDDDF6F9475BE07F0EE868BBD3C7FC05E909C4D8E7F00175EEC452FCE0517246B0C643A6D155209558F288224649594A044D0181277938D091BA361E326C6A0DBC3E1477F585AF991D3D5080B21C29E82422D0CCBA98A146A91679F6F4F8733B69D76A6CFCC7466FA7A259FE8B6653193EFE7F37DF7FB3CF34C4303000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000C0A288A268D9D0D0D0FBA74E9DFAB1A7A727EAEAEA5215AEEEEEEEA8BFBFFF1F71B55A91E87B7D0F75270C9C8181812893C944E3E3E36A91EAC68D1BD1B163C7BE8D07D06AAB127DAFEFA1AE84DF6C0C9CAA193CE3F16F3CE7AD4AF4BDBE87BA128E50357CF5543C7426AC4AF4BDBE87BA12AE1D6AF6AA1A3A915589BED7F7B02487CEF7DF8C4443836F47E7BA774E56F8F7F03583C2D041DF2B7D0F8987CEADD1E1E8EC91EDD1E9835B7F52E16BE17B8685A183BE57FA1E120D9D2BE7FE3263E064EBEAB94EC3C2D041DF2B7D0FC986CEF98FF7E41D3AE17B8685A183BE57FA1E120D9DB35D3BF20E9DF03DC3C2D041DF2B7D0F868EA103FA5EDF43F50E9D70177ABEA113BE6758183AE87BA5EF21D1D0B978FCF779874EF89E6161E8A0EF95BE874443E7E6F099E8ECD18E9947A9F1D7C2F70C0B43077DAFF43D241A3AA12EFDF5DD1943277CCDA03074D0F74ADF43F2A173F76E74F1C41F661EA5C65F0BDF332C0C1DF4BDD2F7B0E0A1139E1478E1F8EFF25EBB0DDFF3344143077DAFF43D143F74E2DF5C462EF6467F3BFCDBBC03275BE167C2CFFA6DC7D041DF2B7D0F050D9DF97EABF1DB8EA183BED7F7FA1E120D9D427EAB99EBB71DC3C3D049E85FBC04FA5EDF439D0F9D850E9C6C191E864E022BE38AA6FE89BED7F750AF4347193A8BE4DFE31A8D6BEFD43F57E9507DAFEFC1D051864EA983C6AA9C138E1B0287BED7F760E82843A71C4163BEAFA3EFF53D183ACAD029D8CA790245F6FBEEE1D0F7FA9EDA1345D12FCE9C39D3FECE3BEF1CDBBD7BF7B7CF3EFBECC4BA75EBA2A6A6A6A8B5B5F5FE534F3D35FEE28B2F0EEDD9B3677F5CFF66E82843A76C4163BE20E18443DFEB7B6A2F647CF0C107EFBEFCF2CB139B366D8ADE7AEBADE8D4A953D1F0F07074E7CE9D2808FFCC6432D1A79F7E1AC561247AEEB9E7EE3FFFFCF397376CD8B0DED051864E492F9DAC2CD3CFA3EFF53D8B230E156DAFBCF2CADDAD5BB74E06897BF7EE4585FAFCF3CFA36DDBB6DD6B6F6F3FBF66CD9A5F193ACAD0491C34562DE0CFDD1238F4BDBEA75A4F33967DF4D147BD4F3EF964D4D9D95954C898EEF0E1C3515B5BDB58BD9C72183A864E8D040D9754F4BDBEA7BA83465F5FDF7F6FDCB831BA70E142540A972F5F8EE2E032B67EFDFAFF307494A153B0F96E06ADF4FF8FB0A1F43DA5114E3442D008F7609452F8FFDBB061C3AD743ADD6AE82843A7E08050AA4B204E38F4BDBEA73A847B34C2A593529D68CC76C2F1F8E38F8FC681E3214347193AF306836CD0F8595C6BA7FE598CE97FCE4DA3FA5EDFB3E8974F7EB173E7CEF1708F46391D3C7870ACA5A5E5A4A1A30C9D824E204250F863C3FF7DFEC91F8B081CF9FE9CC0A1EFF53D8BE7C081037F0AEF3AF9E1871FA2727BFAE9A7AFD5EAE51443C7D0A960D068983A998872AA90C0911B34B2F5F82C97685C52D1F7FA9ECA9E6A84E76884B7B756C2E9D3A7C76BF574C3D03174CA245F00982D38CC153866FBF97DB3FCBC7B38F4BDBEA7B2C29341C34DA149DEE25AACB6B6B66B4D4D4DCBABE53548A7D35D71FDDAD03174CABD86E6081A2B8B0810B3058E428346834B2AFA5EDF5371E111E4FBF6ED8B2A69EFDEBD9FC761E3856A790DC263D74385E1934AA51E36740C9D72ADA1056EF8F3058E628346EEDFBF643F2D56DFEB7B2A287CD649780479259D3871E272DCE49DD5367472EAD06CBFF1183A864ED23594E05246BEC0F1F305068DF92EE12C99B0A1EFF53D15103E546D6464A4A261E3DAB56B99B8B1AF54F1D079F01B4FEEF031740C9DA46B68814163AEC0F14582A091F4BFA7DEC286BED7F7944BF8F4D6EC87AA554AF8FBE2A6BE53ED4327F7379E70CC6AE8183A49D750094E12660B1C498246B19774EA3E6CE87B7D4F991A6E3114D0E85557864E7555ADAD9F471F7D341B0AF6266CDB9FCF72A2F1C5D4D793D85B8B7DA9EF850D6AC0DAB56BEF57FA64636C6CEC5A8D9C6C0CC6BFD9AC6A6C6C5CE638D5D029C51A6A48FE28F2B94E368A79F097930D7DAFEFA99C8D1B378E57FA9E8DAFBFFEFA8B2ABF6763FA065193D76EF36C48057FDFD029FD1ACAD9D86F95E99E8D85040EF76CD470DF27EDF35A9B03C2468DDABE7DFBC54ABF1BE5C89123FF55A5EF4639994EA71FC9B341D464D898EB6BF37D5FD828FD1A4A709290EFEDADB3BD1BA598C0E1DD2835DEF749FBBCD6E680B051A35E7FFDF57D957ECE464747C7C12A7BCEC65CBF850A1B864EC9D6D0024F14E67B8E46B14F1ACD5AD1B0B49FB351177D2F6C50135E7AE9A57F7DE699677EACE01344EF343737FF4F353D41B450C286A15306F39D2C14FAC0AE620347F652CE23A6606DF7BDB041CDD8BC79F3DF2BF5D928478E1CD91F078D014347D83074E63DE128F6C9A085068EECDF2768081BC20695F3C4134FACD9B265CB44054E37469B9B9B3FF3A9AFC286A133EB09C7F44B1A6B1B8A7F8E864F7D1536840DAA577B7BFB99CECECEB2268D707F4838D528F2BAB6A1236C2C95A133FD6DB1B9C1A1980776E5FB734BFEC3D7840D61834516EEA1686B6B1BBB70E1425982C69933673E8AFF8E91743AFD90A1E3ADAF864EC197547E36753251ECDB59A7FFB9A4CFF71036BCF555DF531AADADAD2DEDEDEDB733994C4983C6D5AB573F696E6EFEB2562F9FD46AD8F0709FBA091CE5B84483BED7F72C9E969696B60D1B36DC2AD5094738D1084123954A6D337494A153F1C0B1241FD8A5EFF53D35200E064DADADADDF7EF8E187DF27B86974F48D37DE783B5C3A896B9DA1A30C9D059D4824B9F4E1D289BED7F754B7700F477373734F7B7BFBB5CF3EFB6CAC98E7681C3D7AF4BD38AC0C869B416BF91E0D43C7D0A9A2C051ECC9849B41F5BDBEA7A642C7EA103AD6AD5B97D9BD7BF7B94F3EF9E4CBE1E1E191EC87B7850F550B9F75D2DDDDFD9F3B76EC38D4D2D23214424638CDA8D5779D183A864E8D5F5271E944DFEB7B6A531C227E994EA737C521E2FDB82E854F6D9DFA5C81F0CF2BE1B34EC223C86BF1C9A0868EA1534781233C82DC9341F5BDBE074347193A0BBEA4726B8E4B23D9779D081AFA5EDF83A1A30C9D929F70B874A2EFF53D183ACAD0295BE01034F4BDBE074347193A650B1C7B050D7DAFEFC1D051864EB9AC0C376937787BABBED7F760E82843A75CA6C206FA5EDF83A1A30C1D6143DF2B7D0F868EA1236CA0EFF53D183ACAD01136F4BDBEB72A317494A1236CE87BA5EFC1D03174840DF4BDBE074347193AC286BE57FA1E4347193AC286BE57FA1E0C1D4347D840DFEB7B307494A1236CE87BA5EFC1D03174840D7DAFF43D183A868EB081BED7F760E8284347D8D0F74ADF83A163E8081BE87B7D0F868E3274840D7DAFEFC1D051868EB0A1EF95BE8722F5F4F468F6EAA93BF1D0991036840D7DAFEFA1AEF4F7F78F6432190D5F05353C3CFCE778E89C1736840D7DAFEFA1AEF4F6F6FEA6AFAFEF9BEBD7AF8F69FCC5FBCD260C9CEEEEEE2FE35A2D6C081BFA5EDF43DD8917FAAA38590F86A3BC70ED5055BCC2EB7EDEC01136F47D7115D68ABE0740D8C05A01C00682B502800D04AC15006C20582B00D840B05600B081D840B05600B08160AD006003C15A01001B08D60A003610AC15006C20582B006003C15A01C00682B502800D04AC15006C20582B00D840B05600B08180B502800D046B05001B08D60A00D840B05600B08160AD006003C15A01001B08D60A003610AC15006C20582BD60A003610AC15006C20582B00D840C05A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610AC1500B08160AD006003C15A01C00602D60A003610AC15006C20582B00D840C05A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610B05600B08160AD006003C15A01C00602D60A003610AC15006C20582B006003C15A01C00682B502800D046B05006C20582B00D840B05600B08160AD00800D046B05001B08D60A003610B05600B08160AD006003C15A01C00602D60A003610AC15006C20582B006003C15A01C00682B502800D046B05006C20582B00D840B05600B08180B502800D046B05001B08D60A003610B05600B08160AD006003C15A01001B08D60A003610AC15006C20582B006003C15A01C00682B502800D046BC5AB00C07C9B4567D830E6A941AF14C206000BDD2C56C4353157D848A552ABBC52081B0024D9300ECD11364E3636362EF32A216C009064C358912F6CA4D3E947BC42081B009462D398EDDE8D41A71A081B00946AD39871EF867B3510360028A9385C1C70AA81B0014039C3C6C339F76AACF48A206C00508ECD63F2DE0DA71A081B00946BF358610341D800A85251142D1B1A1A7AFFD4A9533FF6F4F4445D5D5DAAC2D5DDDD1DF5F7F7FF23AE562B52D800A83B21680C0C0C44994C261A1F1F578B54376EDC888E1D3BF66D1C3C565B95C206405D09271A8246D5048EF1AEAEAEF356A5B0015057C2A5131B7DF5541C3626AC4A6103A0AE847B066CF25515366C84C206C0D20C1BDF7F33120D0DBE1D9DEBDE3959E1DFC3D704046143D8002071D8B8353A1C9D3DB23D3A7D70EB4F2A7C2D7C4F481036840D0012858D2BE7FE32236864EBEAB94E2141D8103600481636CE7FBC276FD808DF1312840D6103804461E36CD78EBC61237C4F481036840D00840D61036103A07AC34678F749BEB011BE2724081BC2060089C2C6C5E3BFCF1B36C2F784046143D8002051D8B8397C263A7BB463E62594F86BE17B4282B0216C0090286C84BAF4D77767848DF0350141D8103600481E36EEDE8D2E9EF8C3CC4B28F1D7C2F784046143D80060C161233C21F4C2F1DFE5BD67237CCF5344858D2A0E169D215CCC53835E2980C5081B77EF4623177BA3BF1DFE6DDEA091ADF033E1679D72081B55183656C4353157D848A552ABBC5200150E1BF39D6638E510366A2C701C9A236C9C6C6C6C5CE65502A870D828E43463AE530EA141D8A8C2D38D59C3463A9D7EC42B04B0086163A141235B4283B051858163B67B37069D6A002C52D850C2469D9E6E4CB8570340D81036848DB289C3C501A71A00C286B0216C94336C3C9C73AFC64AAF0880B0216C5072D97B379C6A00081BC20625D1D2D2F2CB542AF54C1C30DE8FEB625CB7A74E37EEC47569EAD2CA0BE1E7BC5A00C286B041C1C25B5AE3EA8A6BBC80278866EBE3F8E75BBD7A00C286B0415E7160581E0786C345048CD96A60CD9A35BFF26A02081BC2063F914AA59A9A9B9BFF373738B4B4B444AFBEFA6AD4DBDB1B7DF5D557D177DF7D1705B76FDF8E868787A3E3C78F477BF7EE8D1E7BECB1E981E396530E006143D8203768AC8AC3C1BDDC90B17FFFFE2893C94485181D1D9DFCF9F0E7A63D6174BD571740D81036048DA6DCA0B179F3E6C9538C85087F6EEBD6ADD30387130E006143D858AAC23D1AB9974E3A3A3A1E5C2A59A8708965D7AE5DD32FA93CE4D5061036848D25280E1ADDB9271A4983466EE09876C231E0D5061036848D2526BCBD35F71E8D855E3A99EB924AEE3D1C2EA700081BC2C6D20B1B5DD920106EEE2C87F7DE7BCFE90680B0216CD4638888EBD773FD4C78E267F6A6D070FA70F3E6CDB2848DF02E95696F8B5D6E6A00081BC2468DCBB96CD1153E3C6DB69F997A04F9E4CFBDF6DA6B513985E770E4848D174C0D006143D8A893B0915387A69F744C7DD6C9E4F7FBFAFACA1A36C283BF72FE5B3A4D0D006143D8A8BFB0F1E0A4231B3AA63E546DF2EBA5BE3174BAF0A4D19CFF8E2BA60680B0216CD469D8C83DE9886B2CFBBF4BF576D7B9DE069BF377DF313500848D9AAE841F20B624AB1272FF3E530340D870B251BF271B83E133501A1B1B9785A77A3AD9001036CA52F17FF2AC55E8F7858D9A0C1B0F4246CECFB8670340D8285FD898EB6BF37D5FD8A8A9B071323C25343764E4FC8C77A300081BC286B0B1E0B031E324633ACFD9001036840D61A3ACC21344E38D7FA2124F106D6D6DBDEF09A200C286B0B104F96C14006143D81036CA1D36CAFEA9AFCDCDCDF77DEA2B80B0216C2C617108F8381B06366FDE5CB2B7C186B7BB6ED9B2E55EEEA9C65CF79000506761A3C15B5FF9FFB0B13CAE1BD950D0D1D191387084A0B16BD7AEDCFB346EA5D3E987BCDA004B246C78A817D3A552A9A6ECCDA2D9138E855E52097F6EDA8986CB2700C286B0C1E409C7BADCC011EEE108378D86779314FAAE93703368EE3D1AA1E220B3CDAB0B206C081B3C38E1886B34372C84D0119EC3111EFC154E2DB29758C2A592F064D0F0C0AE37DF7C335ABB76ED8FD39E5A1A1E87BECEAB0A206C081B4C3FE1589EFB96D805D6807B3400840D6183F942C7EAA9D031514CC808A719DE7502206C081B142C3C69340E1D9BA63E4BE552F8D4D6ECA7B7860F550B9F75121E41EEC9A000C286B00100C28612360040D810360040D81036AC4A00840D256C0080B0216C0080B0A1840D00840D256C0080B0216C0080B0A1840D00840D256C0080B0216C0080B0A1840D001036840D001036840D00103694B00100C286B00100C286123600103694B00100C286B00100C286123600585A7A7A7A6CF2D55377E2B0316155025057FAFBFB4732998C8DBE0A6A7878F8CF71D8386F550250577A7B7B7FD3D7D7F7CDF5EBD7C76CF88B77A21182467777F79771ADB62A01A83BF106B72AFE8D7A301CE1877B0654C52BBCEEE7050D000000000000000000000000000000000000802AF44F5FC8B304C780B2D30000000049454E44AE426082, 1);

-- ----------------------------
-- Table structure for act_ge_property
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('batch.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.execution-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.task-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('common.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('entitylink.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('eventsubscription.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('identitylink.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('job.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('next.dbid', '1', 1);
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(6.6.0.0)', 1);
INSERT INTO `act_ge_property` VALUES ('schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('task.schema.version', '6.6.0.0', 1);
INSERT INTO `act_ge_property` VALUES ('variable.schema.version', '6.6.0.0', 1);

-- ----------------------------
-- Table structure for act_hi_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `TRANSACTION_ORDER_` int(0) NULL DEFAULT NULL,
  `DURATION_` bigint(0) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_actinst
-- ----------------------------
INSERT INTO `act_hi_actinst` VALUES ('10c29f94-83f1-11ec-88f2-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-02 14:26:33.319', '2022-02-02 14:26:33.329', 1, 10, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('10c51095-83f1-11ec-88f2-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-3911B50D-991A-4F34-BB64-AC695A38035B', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 14:26:33.335', '2022-02-02 14:26:33.335', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('10c51096-83f1-11ec-88f2-dc41a90b0909', 2, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '10cf97e7-83f1-11ec-88f2-dc41a90b0909', NULL, '班长', 'userTask', 'yqmm', '2022-02-02 14:26:33.335', '2022-02-02 17:02:42.798', 3, 9369463, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('12cedad3-8427-11ec-9b73-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-02 20:53:09.578', '2022-02-02 20:53:09.582', 1, 4, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('12cf9e24-8427-11ec-9b73-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 20:53:09.583', '2022-02-02 20:53:09.583', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('12cf9e25-8427-11ec-9b73-dc41a90b0909', 2, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '12d25d46-8427-11ec-9b73-dc41a90b0909', NULL, '人事', 'userTask', 'songxy', '2022-02-02 20:53:09.583', '2022-02-02 21:09:19.715', 3, 970132, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('1f45bbfb-83f6-11ec-9379-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-02 15:02:45.150', '2022-02-02 15:02:45.154', 1, 4, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('1f46cd6c-83f6-11ec-9379-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-3911B50D-991A-4F34-BB64-AC695A38035B', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 15:02:45.157', '2022-02-02 15:02:45.157', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('1f46cd6d-83f6-11ec-9379-dc41a90b0909', 2, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '1f493e6e-83f6-11ec-9379-dc41a90b0909', NULL, '班长', 'userTask', 'yqmm', '2022-02-02 15:02:45.157', '2022-02-02 15:47:21.821', 3, 2676664, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('454aca96-8722-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-F969DB3D-8B6E-4DA4-806A-D6410A413578', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('454aca97-8722-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '结束', 'endEvent', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('550ec6d2-8429-11ec-8070-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-81DD1ECE-1C16-4E97-A948-0A4F3C0DEBB6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 21:09:19.720', '2022-02-02 21:09:19.720', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('550f14f3-8429-11ec-8070-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-02-02 21:09:19.722', '2022-02-02 21:09:19.746', 2, 24, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('5512be74-8429-11ec-8070-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-9F3C21D4-0160-43D1-9423-9F855EACEE13', NULL, NULL, '小于1000', 'sequenceFlow', NULL, '2022-02-02 21:09:19.746', '2022-02-02 21:09:19.746', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('5512e585-8429-11ec-8070-dc41a90b0909', 2, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-7DEAB731-9140-4527-A905-3F9002166154', '55130c96-8429-11ec-8070-dc41a90b0909', NULL, '财务', 'userTask', 'yqmm', '2022-02-02 21:09:19.747', '2022-02-02 21:31:29.201', 4, 1329454, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('5ab2143d-83fc-11ec-95f0-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 15:47:21.826', '2022-02-02 15:47:21.826', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('5ab23b4e-83fc-11ec-95f0-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', '5ab4100f-83fc-11ec-95f0-dc41a90b0909', NULL, '老师', 'userTask', 'songxy', '2022-02-02 15:47:21.827', NULL, 2, NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('641c7695-8721-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('641c7696-8721-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('641c7697-8721-11ec-8ba2-dc41a90b0909', 2, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '641c7698-8721-11ec-8ba2-dc41a90b0909', NULL, '人事', 'userTask', 'songxy', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('6d7defb1-842c-11ec-9f4a-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-F969DB3D-8B6E-4DA4-806A-D6410A413578', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 21:31:29.204', '2022-02-02 21:31:29.204', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('6d7e16c2-842c-11ec-9f4a-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '结束', 'endEvent', NULL, '2022-02-02 21:31:29.205', '2022-02-02 21:31:29.208', 2, 3, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7303c7a3-8725-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-81DD1ECE-1C16-4E97-A948-0A4F3C0DEBB6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7303c7a4-8725-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7303eeb5-8725-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-409E4567-B4ED-47E8-87A5-2135A3F108A3', NULL, NULL, '大于1000', 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7303eeb6-8725-11ec-8ba2-dc41a90b0909', 2, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-60D5E385-71A9-4CC5-BFAE-E302EFF04BA9', '73043cd7-8725-11ec-8ba2-dc41a90b0909', NULL, '总经理', 'userTask', 'admin', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 4, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('86de2fcb-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-06 13:42:08.532', '2022-02-06 13:42:08.532', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('86de56dc-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 13:42:08.532', '2022-02-06 13:42:08.532', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('86de56dd-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '86e1641e-870f-11ec-8b65-dc41a90b0909', NULL, '人事', 'userTask', 'songxy', '2022-02-06 13:42:08.532', NULL, 3, NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a8e173eb-8725-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-DF045EE9-B8D6-4593-B1D4-F03F0965B8F0', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a8e19afc-8725-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '结束', 'endEvent', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('ce5e462b-8724-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('ce5e462c-8724-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('ce5e462d-8724-11ec-8ba2-dc41a90b0909', 2, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', 'ce5e462e-8724-11ec-8ba2-dc41a90b0909', NULL, '人事', 'userTask', 'songxy', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e1684546-8406-11ec-b6f5-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 17:02:42.802', '2022-02-02 17:02:42.802', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e1686c57-8406-11ec-b6f5-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', 'e16b2b78-8406-11ec-b6f5-dc41a90b0909', NULL, '老师', 'userTask', 'songxy', '2022-02-02 17:02:42.803', NULL, 2, NULL, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e22be2fe-8721-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-81DD1ECE-1C16-4E97-A948-0A4F3C0DEBB6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e22be2ff-8721-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-CBA1D24F-1135-4096-9ACA-528FC1EC043A', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e22e04e0-8721-11ec-8ba2-dc41a90b0909', 1, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-9F3C21D4-0160-43D1-9423-9F855EACEE13', NULL, NULL, '小于1000', 'sequenceFlow', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('e22e04e1-8721-11ec-8ba2-dc41a90b0909', 2, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', 'sid-7DEAB731-9140-4527-A905-3F9002166154', 'e22e04e2-8721-11ec-8ba2-dc41a90b0909', NULL, '财务', 'userTask', 'yqmm', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 4, 0, NULL, '');

-- ----------------------------
-- Table structure for act_hi_attachment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_comment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_comment
-- ----------------------------
INSERT INTO `act_hi_comment` VALUES ('4548f5d5-8722-11ec-8ba2-dc41a90b0909', 'event', '2022-02-06 15:15:01.249', NULL, 'e22e04e2-8721-11ec-8ba2-dc41a90b0909', NULL, 'AddUserLink', 'yqmm_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('55079adf-8429-11ec-8070-dc41a90b0909', 'event', '2022-02-02 21:09:19.673', NULL, '12d25d46-8427-11ec-9b73-dc41a90b0909', NULL, 'AddUserLink', 'songxy_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('5aadf58c-83fc-11ec-95f0-dc41a90b0909', 'event', '2022-02-02 15:47:21.799', NULL, '1f493e6e-83f6-11ec-9379-dc41a90b0909', NULL, 'AddUserLink', 'yqmm_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('6d790db0-842c-11ec-9f4a-dc41a90b0909', 'event', '2022-02-02 21:31:29.172', NULL, '55130c96-8429-11ec-8070-dc41a90b0909', NULL, 'AddUserLink', 'yqmm_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('7301a4c1-8725-11ec-8ba2-dc41a90b0909', 'event', '2022-02-06 15:15:01.249', NULL, 'ce5e462e-8724-11ec-8ba2-dc41a90b0909', NULL, 'AddUserLink', 'songxy_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('a8e03b6a-8725-11ec-8ba2-dc41a90b0909', 'event', '2022-02-06 15:15:01.249', NULL, '73043cd7-8725-11ec-8ba2-dc41a90b0909', NULL, 'AddUserLink', 'admin_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('e162c704-8406-11ec-b6f5-dc41a90b0909', 'event', '2022-02-02 17:02:42.766', NULL, '10cf97e7-83f1-11ec-88f2-dc41a90b0909', NULL, 'AddUserLink', 'yqmm_|_assignee', NULL);
INSERT INTO `act_hi_comment` VALUES ('e2221efb-8721-11ec-8ba2-dc41a90b0909', 'event', '2022-02-06 15:15:01.249', NULL, '641c7698-8721-11ec-8ba2-dc41a90b0909', NULL, 'AddUserLink', 'songxy_|_assignee', NULL);

-- ----------------------------
-- Table structure for act_hi_detail
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(0) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_entitylink`;
CREATE TABLE `act_hi_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_ROOT_SCOPE`(`ROOT_SCOPE_ID_`, `ROOT_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_identitylink
-- ----------------------------
INSERT INTO `act_hi_identitylink` VALUES ('10c22a62-83f1-11ec-88f2-dc41a90b0909', NULL, 'starter', 'admin', NULL, '2022-02-02 14:26:33.318', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('10d00d18-83f1-11ec-88f2-dc41a90b0909', NULL, 'assignee', 'yqmm', '10cf97e7-83f1-11ec-88f2-dc41a90b0909', '2022-02-02 14:26:33.407', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('10d03429-83f1-11ec-88f2-dc41a90b0909', NULL, 'participant', 'yqmm', NULL, '2022-02-02 14:26:33.408', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('12d2f987-8427-11ec-9b73-dc41a90b0909', NULL, 'assignee', 'songxy', '12d25d46-8427-11ec-9b73-dc41a90b0909', '2022-02-02 20:53:09.605', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('12d32098-8427-11ec-9b73-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-02 20:53:09.606', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('1f49657f-83f6-11ec-9379-dc41a90b0909', NULL, 'assignee', 'yqmm', '1f493e6e-83f6-11ec-9379-dc41a90b0909', '2022-02-02 15:02:45.174', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('1f498c90-83f6-11ec-9379-dc41a90b0909', NULL, 'participant', 'yqmm', NULL, '2022-02-02 15:02:45.175', '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('551333a7-8429-11ec-8070-dc41a90b0909', NULL, 'assignee', 'yqmm', '55130c96-8429-11ec-8070-dc41a90b0909', '2022-02-02 21:09:19.749', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('551381c8-8429-11ec-8070-dc41a90b0909', NULL, 'participant', 'yqmm', NULL, '2022-02-02 21:09:19.751', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('5ab43720-83fc-11ec-95f0-dc41a90b0909', NULL, 'assignee', 'songxy', '5ab4100f-83fc-11ec-95f0-dc41a90b0909', '2022-02-02 15:47:21.840', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('5ab4d361-83fc-11ec-95f0-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-02 15:47:21.844', '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('641c7699-8721-11ec-8ba2-dc41a90b0909', NULL, 'assignee', 'songxy', '641c7698-8721-11ec-8ba2-dc41a90b0909', '2022-02-06 15:15:01.249', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('641cc4ba-8721-11ec-8ba2-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-06 15:15:01.249', '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('73043cd8-8725-11ec-8ba2-dc41a90b0909', NULL, 'assignee', 'admin', '73043cd7-8725-11ec-8ba2-dc41a90b0909', '2022-02-06 15:15:01.249', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('73043cd9-8725-11ec-8ba2-dc41a90b0909', NULL, 'participant', 'admin', NULL, '2022-02-06 15:15:01.249', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('86e2005f-870f-11ec-8b65-dc41a90b0909', NULL, 'assignee', 'songxy', '86e1641e-870f-11ec-8b65-dc41a90b0909', '2022-02-06 13:42:08.532', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('86e20060-870f-11ec-8b65-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-06 13:42:08.532', '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('ce5e462f-8724-11ec-8ba2-dc41a90b0909', NULL, 'assignee', 'songxy', 'ce5e462e-8724-11ec-8ba2-dc41a90b0909', '2022-02-06 15:15:01.249', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('ce5e6d40-8724-11ec-8ba2-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-06 15:15:01.249', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('e16b5289-8406-11ec-b6f5-dc41a90b0909', NULL, 'assignee', 'songxy', 'e16b2b78-8406-11ec-b6f5-dc41a90b0909', '2022-02-02 17:02:42.822', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('e16bc7ba-8406-11ec-b6f5-dc41a90b0909', NULL, 'participant', 'songxy', NULL, '2022-02-02 17:02:42.825', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('e22e2bf3-8721-11ec-8ba2-dc41a90b0909', NULL, 'assignee', 'yqmm', 'e22e04e2-8721-11ec-8ba2-dc41a90b0909', '2022-02-06 15:15:01.249', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('e22e2bf4-8721-11ec-8ba2-dc41a90b0909', NULL, 'participant', 'yqmm', NULL, '2022-02-06 15:15:01.249', '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_hi_procinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(0) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `PROC_INST_ID_`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_procinst
-- ----------------------------
INSERT INTO `act_hi_procinst` VALUES ('10c1dc41-83f1-11ec-88f2-dc41a90b0909', 1, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '2022-02-02 14:26:33.314', NULL, NULL, 'admin', 'startEvent1', NULL, NULL, NULL, '', '请假 - February 2nd 2022', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('12ce8cb0-8427-11ec-9b73-dc41a90b0909', 2, '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '2022-02-02 20:53:09.576', '2022-02-02 21:31:29.243', 2299667, NULL, 'startEvent1', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('1f4594e9-83f6-11ec-9379-dc41a90b0909', 1, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '2022-02-02 15:02:45.149', NULL, NULL, NULL, 'startEvent1', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('641c0162-8721-11ec-8ba2-dc41a90b0909', 2, '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 'startEvent1', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('86dde1a8-870f-11ec-8b65-dc41a90b0909', 1, '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '2022-02-06 13:42:08.532', NULL, NULL, NULL, 'startEvent1', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 2, 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', NULL, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 'startEvent1', 'sid-C9F00CC3-9E9F-4499-BE36-78C7EA2E85C6', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_hi_taskinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(0) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(0) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_taskinst
-- ----------------------------
INSERT INTO `act_hi_taskinst` VALUES ('10cf97e7-83f1-11ec-88f2-dc41a90b0909', 3, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', NULL, 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '班长', NULL, NULL, NULL, 'yqmm', '2022-02-02 14:26:33.335', '2022-02-02 17:02:42.761', '2022-02-02 17:02:42.792', 9369457, NULL, 50, NULL, 'key-sp', NULL, '', '2022-02-02 17:02:42.792');
INSERT INTO `act_hi_taskinst` VALUES ('12d25d46-8427-11ec-9b73-dc41a90b0909', 3, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '人事', NULL, NULL, NULL, 'songxy', '2022-02-02 20:53:09.583', '2022-02-02 21:09:19.668', '2022-02-02 21:09:19.707', 970124, NULL, 50, NULL, NULL, NULL, '', '2022-02-02 21:09:19.707');
INSERT INTO `act_hi_taskinst` VALUES ('1f493e6e-83f6-11ec-9379-dc41a90b0909', 3, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', NULL, 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '班长', NULL, NULL, NULL, 'yqmm', '2022-02-02 15:02:45.157', '2022-02-02 15:47:21.794', '2022-02-02 15:47:21.816', 2676659, NULL, 50, NULL, 'key-sp', NULL, '', '2022-02-02 15:47:21.816');
INSERT INTO `act_hi_taskinst` VALUES ('55130c96-8429-11ec-8070-dc41a90b0909', 3, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, 'sid-7DEAB731-9140-4527-A905-3F9002166154', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12cedad2-8427-11ec-9b73-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '财务', NULL, NULL, NULL, 'yqmm', '2022-02-02 21:09:19.747', '2022-02-02 21:31:29.167', '2022-02-02 21:31:29.194', 1329447, NULL, 50, NULL, NULL, NULL, '', '2022-02-02 21:31:29.194');
INSERT INTO `act_hi_taskinst` VALUES ('5ab4100f-83fc-11ec-95f0-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', NULL, 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, 'songxy', '2022-02-02 15:47:21.827', NULL, NULL, NULL, NULL, 50, NULL, 'key-sp', NULL, '', '2022-02-02 15:47:21.840');
INSERT INTO `act_hi_taskinst` VALUES ('641c7698-8721-11ec-8ba2-dc41a90b0909', 3, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', NULL, 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '人事', NULL, NULL, NULL, 'songxy', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 50, NULL, NULL, NULL, '', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_taskinst` VALUES ('73043cd7-8725-11ec-8ba2-dc41a90b0909', 3, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', NULL, 'sid-60D5E385-71A9-4CC5-BFAE-E302EFF04BA9', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '总经理', NULL, NULL, NULL, 'admin', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 50, NULL, NULL, NULL, '', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_taskinst` VALUES ('86e1641e-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '人事', NULL, NULL, NULL, 'songxy', '2022-02-06 13:42:08.532', NULL, NULL, NULL, NULL, 50, NULL, NULL, NULL, '', '2022-02-06 13:42:08.532');
INSERT INTO `act_hi_taskinst` VALUES ('ce5e462e-8724-11ec-8ba2-dc41a90b0909', 3, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', NULL, 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e462a-8724-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '人事', NULL, NULL, NULL, 'songxy', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 50, NULL, NULL, NULL, '', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_taskinst` VALUES ('e16b2b78-8406-11ec-b6f5-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', NULL, 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, 'songxy', '2022-02-02 17:02:42.803', NULL, NULL, NULL, NULL, 50, NULL, 'key-sp', NULL, '', '2022-02-02 17:02:42.822');
INSERT INTO `act_hi_taskinst` VALUES ('e22e04e2-8721-11ec-8ba2-dc41a90b0909', 3, 'key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', NULL, 'sid-7DEAB731-9140-4527-A905-3F9002166154', '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c7694-8721-11ec-8ba2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, '财务', NULL, NULL, NULL, 'yqmm', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249', 0, NULL, 50, NULL, NULL, NULL, '', '2022-02-06 15:15:01.249');

-- ----------------------------
-- Table structure for act_hi_tsk_log
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_tsk_log`;
CREATE TABLE `act_hi_tsk_log`  (
  `ID_` bigint(0) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_hi_varinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(0) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_`, `VAR_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_EXE`(`EXECUTION_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_varinst
-- ----------------------------
INSERT INTO `act_hi_varinst` VALUES ('12ceb3c1-8427-11ec-9b73-dc41a90b0909', 0, '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, 'hrUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'songxy', NULL, '2022-02-02 20:53:09.577', '2022-02-02 20:53:09.577');
INSERT INTO `act_hi_varinst` VALUES ('550c7ce0-8429-11ec-8070-dc41a90b0909', 0, '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, 'money', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '100', NULL, '2022-02-02 21:09:19.707', '2022-02-02 21:09:19.707');
INSERT INTO `act_hi_varinst` VALUES ('550ccb01-8429-11ec-8070-dc41a90b0909', 0, '12ce8cb0-8427-11ec-9b73-dc41a90b0909', '12ce8cb0-8427-11ec-9b73-dc41a90b0909', NULL, 'financeUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'yqmm', NULL, '2022-02-02 21:09:19.707', '2022-02-02 21:09:19.707');
INSERT INTO `act_hi_varinst` VALUES ('641c2873-8721-11ec-8ba2-dc41a90b0909', 0, '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, 'hrUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'songxy', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_varinst` VALUES ('73035272-8725-11ec-8ba2-dc41a90b0909', 0, 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', NULL, 'money', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '10000', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_varinst` VALUES ('86de08b9-870f-11ec-8b65-dc41a90b0909', 0, '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, 'hrUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'songxy', NULL, '2022-02-06 13:42:08.532', '2022-02-06 13:42:08.532');
INSERT INTO `act_hi_varinst` VALUES ('ce5e1f19-8724-11ec-8ba2-dc41a90b0909', 0, 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', 'ce5e1f18-8724-11ec-8ba2-dc41a90b0909', NULL, 'hrUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'songxy', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_varinst` VALUES ('e1667085-8406-11ec-b6f5-dc41a90b0909', 0, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, 'outcome', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '通过', NULL, '2022-02-02 17:02:42.791', '2022-02-02 17:02:42.791');
INSERT INTO `act_hi_varinst` VALUES ('e22b1fac-8721-11ec-8ba2-dc41a90b0909', 0, '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, 'financeUserId', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'yqmm', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249');
INSERT INTO `act_hi_varinst` VALUES ('e22b1fad-8721-11ec-8ba2-dc41a90b0909', 0, '641c0162-8721-11ec-8ba2-dc41a90b0909', '641c0162-8721-11ec-8ba2-dc41a90b0909', NULL, 'money', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '100', NULL, '2022-02-06 15:15:01.249', '2022-02-06 15:15:01.249');

-- ----------------------------
-- Table structure for act_id_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_id_bytearray`;
CREATE TABLE `act_id_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_group
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_info
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_membership
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership`  (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`) USING BTREE,
  INDEX `ACT_FK_MEMB_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_id_priv
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv`;
CREATE TABLE `act_id_priv`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PRIV_NAME`(`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_priv
-- ----------------------------
INSERT INTO `act_id_priv` VALUES ('9e6c4a56-8372-11ec-ba42-dc41a90b0909', 'access-admin');
INSERT INTO `act_id_priv` VALUES ('9e6a7594-8372-11ec-ba42-dc41a90b0909', 'access-idm');
INSERT INTO `act_id_priv` VALUES ('9e6d82d8-8372-11ec-ba42-dc41a90b0909', 'access-modeler');
INSERT INTO `act_id_priv` VALUES ('9e701aec-8372-11ec-ba42-dc41a90b0909', 'access-rest-api');
INSERT INTO `act_id_priv` VALUES ('9e6e6d3a-8372-11ec-ba42-dc41a90b0909', 'access-task');

-- ----------------------------
-- Table structure for act_id_priv_mapping
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv_mapping`;
CREATE TABLE `act_id_priv_mapping`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PRIV_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_PRIV_MAPPING`(`PRIV_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `act_id_priv` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_priv_mapping
-- ----------------------------
INSERT INTO `act_id_priv_mapping` VALUES ('9e6b8705-8372-11ec-ba42-dc41a90b0909', '9e6a7594-8372-11ec-ba42-dc41a90b0909', 'admin', NULL);
INSERT INTO `act_id_priv_mapping` VALUES ('9e6d0da7-8372-11ec-ba42-dc41a90b0909', '9e6c4a56-8372-11ec-ba42-dc41a90b0909', 'admin', NULL);
INSERT INTO `act_id_priv_mapping` VALUES ('9e6df809-8372-11ec-ba42-dc41a90b0909', '9e6d82d8-8372-11ec-ba42-dc41a90b0909', 'admin', NULL);
INSERT INTO `act_id_priv_mapping` VALUES ('9e6f308b-8372-11ec-ba42-dc41a90b0909', '9e6e6d3a-8372-11ec-ba42-dc41a90b0909', 'admin', NULL);
INSERT INTO `act_id_priv_mapping` VALUES ('9e70b72d-8372-11ec-ba42-dc41a90b0909', '9e701aec-8372-11ec-ba42-dc41a90b0909', 'admin', NULL);

-- ----------------------------
-- Table structure for act_id_property
-- ----------------------------
DROP TABLE IF EXISTS `act_id_property`;
CREATE TABLE `act_id_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_property
-- ----------------------------
INSERT INTO `act_id_property` VALUES ('schema.version', '6.6.0.0', 1);

-- ----------------------------
-- Table structure for act_id_token
-- ----------------------------
DROP TABLE IF EXISTS `act_id_token`;
CREATE TABLE `act_id_token`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NULL DEFAULT NULL,
  `IP_ADDRESS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_AGENT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_token
-- ----------------------------
INSERT INTO `act_id_token` VALUES ('kr9OJbeIXtUk8wLn1uQ+YQ==', 1, 'X7zvs4A+F5S8B169bU8tLw==', '2022-02-02 14:30:51.497', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36', 'admin', NULL);

-- ----------------------------
-- Table structure for act_id_user
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_user
-- ----------------------------
INSERT INTO `act_id_user` VALUES ('admin', 1, 'Test', 'Administrator', NULL, 'test-admin@example-domain.tld', 'test', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('songxy', 2, '先阳', '宋', NULL, NULL, 'test', NULL, NULL);
INSERT INTO `act_id_user` VALUES ('yqmm', 2, '元气', '满满', NULL, NULL, 'test', NULL, NULL);

-- ----------------------------
-- Table structure for act_procdef_info
-- ----------------------------
DROP TABLE IF EXISTS `act_procdef_info`;
CREATE TABLE `act_procdef_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_INFO_JSON_BA`(`INFO_JSON_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_re_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_deployment
-- ----------------------------
INSERT INTO `act_re_deployment` VALUES ('4b5cc586-83f5-11ec-98ed-dc41a90b0909', '学生请假流程', NULL, 'key-1-qj', '', '2022-02-02 14:56:49.624', NULL, NULL, '4b5cc586-83f5-11ec-98ed-dc41a90b0909', NULL);
INSERT INTO `act_re_deployment` VALUES ('86f1b7d1-870f-11ec-8b65-dc41a90b0909', 'OA报销流程', NULL, NULL, '', '2022-02-06 13:42:08.532', NULL, NULL, '86f1b7d1-870f-11ec-8b65-dc41a90b0909', NULL);
INSERT INTO `act_re_deployment` VALUES ('b092050b-8412-11ec-8130-dc41a90b0909', '报销流程', NULL, NULL, '', '2022-02-02 18:27:14.828', NULL, NULL, 'b092050b-8412-11ec-8130-dc41a90b0909', NULL);
INSERT INTO `act_re_deployment` VALUES ('d8b2f846-8381-11ec-b633-dc41a90b0909', '学生请假流程', NULL, NULL, '', '2022-02-02 01:10:25.127', NULL, NULL, 'd8b2f846-8381-11ec-b633-dc41a90b0909', NULL);
INSERT INTO `act_re_deployment` VALUES ('e0690d49-8425-11ec-b924-dc41a90b0909', 'OA报销流程', NULL, NULL, '', '2022-02-02 20:44:35.528', NULL, NULL, 'e0690d49-8425-11ec-b924-dc41a90b0909', NULL);

-- ----------------------------
-- Table structure for act_re_model
-- ----------------------------
DROP TABLE IF EXISTS `act_re_model`;
CREATE TABLE `act_re_model`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(0) NULL DEFAULT NULL,
  `META_INFO_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE`(`EDITOR_SOURCE_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE_EXTRA`(`EDITOR_SOURCE_EXTRA_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_DEPLOYMENT`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_re_procdef
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int(0) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(0) NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(0) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(0) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_VERSION_` int(0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PROCDEF`(`KEY_`, `VERSION_`, `DERIVED_VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_procdef
-- ----------------------------
INSERT INTO `act_re_procdef` VALUES ('key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', 1, 'http://www.flowable.org/processdef', '请假', 'key-1', 1, 'd8b2f846-8381-11ec-b633-dc41a90b0909', 'bpmn/请假bpmn20.xml', 'bpmn/请假key-1.png', '请假流程', 0, 1, 1, '', NULL, NULL, NULL, 0);
INSERT INTO `act_re_procdef` VALUES ('key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', 1, 'http://www.flowable.org/processdef', '请假', 'key-1', 2, '4b5cc586-83f5-11ec-98ed-dc41a90b0909', 'bpmn/请假bpmn20.xml', 'bpmn/请假key-1.png', '请假流程', 0, 1, 1, '', NULL, NULL, NULL, 0);
INSERT INTO `act_re_procdef` VALUES ('key-bx:1:b0c1c79e-8412-11ec-8130-dc41a90b0909', 1, 'http://www.flowable.org/processdef', '报销', 'key-bx', 1, 'b092050b-8412-11ec-8130-dc41a90b0909', 'bpmn/报销bpmn20.xml', 'bpmn/报销key-bx.png', NULL, 0, 1, 1, '', NULL, NULL, NULL, 0);
INSERT INTO `act_re_procdef` VALUES ('key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', 1, 'http://www.flowable.org/processdef', '报销', 'key-bx', 2, 'e0690d49-8425-11ec-b924-dc41a90b0909', 'bpmn/报销bpmn20.xml', 'bpmn/报销key-bx.png', NULL, 0, 1, 1, '', NULL, NULL, NULL, 0);
INSERT INTO `act_re_procdef` VALUES ('key-bx:3:872e72b4-870f-11ec-8b65-dc41a90b0909', 1, 'http://www.flowable.org/processdef', '报销', 'key-bx', 3, '86f1b7d1-870f-11ec-8b65-dc41a90b0909', 'bpmn/报销bpmn20.xml', 'bpmn/报销key-bx.png', NULL, 0, 1, 1, '', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for act_ru_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_actinst`;
CREATE TABLE `act_ru_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(0) NULL DEFAULT NULL,
  `TRANSACTION_ORDER_` int(0) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC_ACT`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC_ACT`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_actinst
-- ----------------------------
INSERT INTO `act_ru_actinst` VALUES ('10c29f94-83f1-11ec-88f2-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-02 14:26:33.319', '2022-02-02 14:26:33.329', 10, 1, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('10c51095-83f1-11ec-88f2-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-3911B50D-991A-4F34-BB64-AC695A38035B', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 14:26:33.335', '2022-02-02 14:26:33.335', 0, 2, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('10c51096-83f1-11ec-88f2-dc41a90b0909', 2, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '10cf97e7-83f1-11ec-88f2-dc41a90b0909', NULL, '班长', 'userTask', 'yqmm', '2022-02-02 14:26:33.335', '2022-02-02 17:02:42.798', 9369463, 3, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('1f45bbfb-83f6-11ec-9379-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-02 15:02:45.150', '2022-02-02 15:02:45.154', 4, 1, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('1f46cd6c-83f6-11ec-9379-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-3911B50D-991A-4F34-BB64-AC695A38035B', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 15:02:45.157', '2022-02-02 15:02:45.157', 0, 2, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('1f46cd6d-83f6-11ec-9379-dc41a90b0909', 2, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-414A1C8A-A7C0-4C6B-96DF-827813026B84', '1f493e6e-83f6-11ec-9379-dc41a90b0909', NULL, '班长', 'userTask', 'yqmm', '2022-02-02 15:02:45.157', '2022-02-02 15:47:21.821', 2676664, 3, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('5ab2143d-83fc-11ec-95f0-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 15:47:21.826', '2022-02-02 15:47:21.826', 0, 1, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('5ab23b4e-83fc-11ec-95f0-dc41a90b0909', 1, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', '1f45bbfa-83f6-11ec-9379-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', '5ab4100f-83fc-11ec-95f0-dc41a90b0909', NULL, '老师', 'userTask', 'songxy', '2022-02-02 15:47:21.827', NULL, NULL, 2, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('86de2fcb-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-02-06 13:42:08.532', '2022-02-06 13:42:08.532', 0, 1, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('86de56dc-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'sid-DD8C5463-C1F6-4F72-97A1-B750C21176E6', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-06 13:42:08.532', '2022-02-06 13:42:08.532', 0, 2, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('86de56dd-870f-11ec-8b65-dc41a90b0909', 1, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86de2fca-870f-11ec-8b65-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', '86e1641e-870f-11ec-8b65-dc41a90b0909', NULL, '人事', 'userTask', 'songxy', '2022-02-06 13:42:08.532', NULL, NULL, 3, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('e1684546-8406-11ec-b6f5-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-BFE86765-8C73-4E9C-8E6D-4F8C046915F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-02-02 17:02:42.802', '2022-02-02 17:02:42.802', 0, 1, NULL, '');
INSERT INTO `act_ru_actinst` VALUES ('e1686c57-8406-11ec-b6f5-dc41a90b0909', 1, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c29f93-83f1-11ec-88f2-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', 'e16b2b78-8406-11ec-b6f5-dc41a90b0909', NULL, '老师', 'userTask', 'songxy', '2022-02-02 17:02:42.803', NULL, NULL, 2, NULL, '');

-- ----------------------------
-- Table structure for act_ru_deadletter_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_deadletter_job`;
CREATE TABLE `act_ru_deadletter_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_entitylink`;
CREATE TABLE `act_ru_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_ROOT_SCOPE`(`ROOT_SCOPE_ID_`, `ROOT_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_event_subscr
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_event_subscr`;
CREATE TABLE `act_ru_event_subscr`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONFIGURATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EVENT_SUBSCR_CONFIG_`(`CONFIGURATION_`) USING BTREE,
  INDEX `ACT_FK_EVENT_EXEC`(`EXECUTION_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_ACTIVE_` tinyint(0) NULL DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(0) NULL DEFAULT NULL,
  `IS_SCOPE_` tinyint(0) NULL DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(0) NULL DEFAULT NULL,
  `IS_MI_ROOT_` tinyint(0) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(0) NULL DEFAULT NULL,
  `CACHED_ENT_STATE_` int(0) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(0) NULL DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int(0) NULL DEFAULT NULL,
  `TASK_COUNT_` int(0) NULL DEFAULT NULL,
  `JOB_COUNT_` int(0) NULL DEFAULT NULL,
  `TIMER_JOB_COUNT_` int(0) NULL DEFAULT NULL,
  `SUSP_JOB_COUNT_` int(0) NULL DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int(0) NULL DEFAULT NULL,
  `EXTERNAL_WORKER_JOB_COUNT_` int(0) NULL DEFAULT NULL,
  `VAR_COUNT_` int(0) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(0) NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_`) USING BTREE,
  INDEX `ACT_IDC_EXEC_ROOT`(`ROOT_PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_SUPER`(`SUPER_EXEC_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_execution
-- ----------------------------
INSERT INTO `act_ru_execution` VALUES ('10c1dc41-83f1-11ec-88f2-dc41a90b0909', 1, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, 1, 0, 1, 0, 0, 1, NULL, '', '请假 - February 2nd 2022', 'startEvent1', '2022-02-02 14:26:33.314', 'admin', NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('10c29f93-83f1-11ec-88f2-dc41a90b0909', 2, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2022-02-02 14:26:33.319', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('1f4594e9-83f6-11ec-9379-dc41a90b0909', 1, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, NULL, 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', NULL, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startEvent1', '2022-02-02 15:02:45.149', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('1f45bbfa-83f6-11ec-9379-dc41a90b0909', 2, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, '1f4594e9-83f6-11ec-9379-dc41a90b0909', 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', NULL, '1f4594e9-83f6-11ec-9379-dc41a90b0909', 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2022-02-02 15:02:45.150', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('86dde1a8-870f-11ec-8b65-dc41a90b0909', 1, '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, NULL, 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, 1, 0, 1, 0, 0, 1, NULL, '', NULL, 'startEvent1', '2022-02-06 13:42:08.532', NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_execution` VALUES ('86de2fca-870f-11ec-8b65-dc41a90b0909', 1, '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, '86dde1a8-870f-11ec-8b65-dc41a90b0909', 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, '86dde1a8-870f-11ec-8b65-dc41a90b0909', 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', 1, 0, 0, 0, 0, 1, NULL, '', NULL, NULL, '2022-02-06 13:42:08.532', NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_ru_external_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_external_job`;
CREATE TABLE `act_ru_external_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(0) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_history_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_history_job`;
CREATE TABLE `act_ru_history_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(0) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_identitylink`;
CREATE TABLE `act_ru_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_GROUP`(`GROUP_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ATHRZ_PROCEDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_TSKASS_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_IDL_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_identitylink
-- ----------------------------
INSERT INTO `act_ru_identitylink` VALUES ('10c22a62-83f1-11ec-88f2-dc41a90b0909', 1, NULL, 'starter', 'admin', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_identitylink` VALUES ('10d03429-83f1-11ec-88f2-dc41a90b0909', 1, NULL, 'participant', 'yqmm', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_identitylink` VALUES ('1f498c90-83f6-11ec-9379-dc41a90b0909', 1, NULL, 'participant', 'yqmm', NULL, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_identitylink` VALUES ('5ab4d361-83fc-11ec-95f0-dc41a90b0909', 1, NULL, 'participant', 'songxy', NULL, '1f4594e9-83f6-11ec-9379-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_identitylink` VALUES ('86e20060-870f-11ec-8b65-dc41a90b0909', 1, NULL, 'participant', 'songxy', NULL, '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_ru_identitylink` VALUES ('e16bc7ba-8406-11ec-b6f5-dc41a90b0909', 1, NULL, 'participant', 'songxy', NULL, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_ru_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_job`;
CREATE TABLE `act_ru_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(0) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_suspended_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_suspended_job`;
CREATE TABLE `act_ru_suspended_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(0) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_task
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(0) NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(0) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(0) NULL DEFAULT NULL,
  `VAR_COUNT_` int(0) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(0) NULL DEFAULT NULL,
  `SUB_TASK_COUNT_` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TASK_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_task
-- ----------------------------
INSERT INTO `act_ru_task` VALUES ('5ab4100f-83fc-11ec-95f0-dc41a90b0909', 1, '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '1f4594e9-83f6-11ec-9379-dc41a90b0909', 'key-1:2:4b8d2459-83f5-11ec-98ed-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', NULL, 'songxy', NULL, 50, '2022-02-02 15:47:21.827', NULL, NULL, 1, '', 'key-sp', NULL, 1, 0, 0, 0);
INSERT INTO `act_ru_task` VALUES ('86e1641e-870f-11ec-8b65-dc41a90b0909', 1, '86de2fca-870f-11ec-8b65-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', 'key-bx:2:e0985aac-8425-11ec-b924-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, NULL, '人事', NULL, NULL, 'sid-95A65D81-8EC2-4046-A1D0-4BAE72B336D3', NULL, 'songxy', NULL, 50, '2022-02-06 13:42:08.532', NULL, NULL, 1, '', NULL, NULL, 1, 0, 0, 0);
INSERT INTO `act_ru_task` VALUES ('e16b2b78-8406-11ec-b6f5-dc41a90b0909', 1, '10c29f93-83f1-11ec-88f2-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', 'key-1:1:d947be39-8381-11ec-b633-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, 'sid-369B9425-D4FE-4786-8668-8C3194A0382F', NULL, 'songxy', NULL, 50, '2022-02-02 17:02:42.803', NULL, NULL, 1, '', 'key-sp', NULL, 1, 0, 0, 0);

-- ----------------------------
-- Table structure for act_ru_timer_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_timer_job`;
CREATE TABLE `act_ru_timer_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(0) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for act_ru_variable
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_variable`;
CREATE TABLE `act_ru_variable`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(0) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_VAR_BYTEARRAY`(`BYTEARRAY_ID_`) USING BTREE,
  INDEX `ACT_IDX_VARIABLE_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_variable
-- ----------------------------
INSERT INTO `act_ru_variable` VALUES ('86de08b9-870f-11ec-8b65-dc41a90b0909', 1, 'string', 'hrUserId', '86dde1a8-870f-11ec-8b65-dc41a90b0909', '86dde1a8-870f-11ec-8b65-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'songxy', NULL);
INSERT INTO `act_ru_variable` VALUES ('e1667085-8406-11ec-b6f5-dc41a90b0909', 1, 'string', 'outcome', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', '10c1dc41-83f1-11ec-88f2-dc41a90b0909', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '通过', NULL);

-- ----------------------------
-- Table structure for flw_channel_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_channel_definition`;
CREATE TABLE `flw_channel_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `VERSION_` int(0) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_CHANNEL_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ev_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangelog`;
CREATE TABLE `flw_ev_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(0) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangelog
-- ----------------------------
INSERT INTO `flw_ev_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml', '2022-02-01 23:21:18', 1, 'EXECUTED', '8:1b0c48c9cf7945be799d868a2626d687', 'createTable tableName=FLW_EVENT_DEPLOYMENT; createTable tableName=FLW_EVENT_RESOURCE; createTable tableName=FLW_EVENT_DEFINITION; createIndex indexName=ACT_IDX_EVENT_DEF_UNIQ, tableName=FLW_EVENT_DEFINITION; createTable tableName=FLW_CHANNEL_DEFIN...', '', NULL, '3.8.0', NULL, NULL, '3728878431');

-- ----------------------------
-- Table structure for flw_ev_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangeloglock`;
CREATE TABLE `flw_ev_databasechangeloglock`  (
  `ID` int(0) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangeloglock
-- ----------------------------
INSERT INTO `flw_ev_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for flw_event_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_definition`;
CREATE TABLE `flw_event_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `VERSION_` int(0) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_EVENT_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_event_deployment
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_deployment`;
CREATE TABLE `flw_event_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_event_resource
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_resource`;
CREATE TABLE `flw_event_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ru_batch
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch`;
CREATE TABLE `flw_ru_batch`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BATCH_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for flw_ru_batch_part
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch_part`;
CREATE TABLE `flw_ru_batch_part`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(0) NULL DEFAULT NULL,
  `BATCH_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESULT_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `FLW_IDX_BATCH_PART`(`BATCH_ID_`) USING BTREE,
  CONSTRAINT `FLW_FK_BATCH_PART_PARENT` FOREIGN KEY (`BATCH_ID_`) REFERENCES `flw_ru_batch` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
