package com.song.flowable;

import com.song.flowable.entity.DoneEntity;
import com.song.flowable.mapper.FlowableMapper;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.engine.*;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormDeployment;
import org.flowable.form.api.FormInfo;
import org.flowable.form.api.FormRepositoryService;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.image.ProcessDiagramGenerator;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

@SpringBootTest
@Slf4j
class YqmmFlowableApplicationTests {
    /**
     * 部署
     */
    @Autowired
    private RepositoryService repositoryService;
    
    /**
     * 启动
     */
    @Autowired
    private RuntimeService runtimeService;
    
    /**
     * 查询任务
     */
    @Autowired
    private TaskService taskService;
    
    @Autowired
    private FlowableMapper flowableMapper;
    
    @Autowired
    private ProcessEngine processEngine;
    
    @Autowired
    private HttpServletResponse httpServletResponse;
    /**
     * 表单部署service
     */
    //@Autowired
    //private FormRepositoryService formRepositoryService;
// 根据类型注入
    @Autowired
    private FormService formService;

    @Test
    void contextLoads() {
    }
    
    /**
     * 部署流程
     */
    @Test
    void createDeployment() {

        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
                //.addClasspathResource("bpmn/定时器边界事件1.bpmn20.xml").name("定时器边界事件1");
                .addClasspathResource("bpmn/邮件服务1.bpmn20.xml").name("邮件服务1");
        Deployment deploy = deploymentBuilder.deploy();
        System.out.println("流程部署成功");
        System.out.println(deploy.getId());
        System.out.println(deploy.getName());
        System.out.println(deploy.getKey());
        System.out.println(deploy.getParentDeploymentId());
    }



    @Test
    void getTask() {

        Task task = taskService.createTaskQuery().processInstanceId("f86da6cb-8790-11ee-b556-dc41a90b0909").singleResult();
        System.out.println(task.getId());
        System.out.println(task.getName());
        System.out.println(task.getAssignee());
    }

    /**
     * 部署流程以及表单
     */
    //@Test
    //void deploymentFrom() {
    //    // 部署流程id
    //    DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
    //            .addClasspathResource("bpmn/外置表单.bpmn20.xml").name("表单请假");
    //    Deployment deploy = deploymentBuilder.deploy();
    //    System.out.println("流程部署成功");
    //    System.out.println(deploy.getId());
    //    System.out.println(deploy.getName());
    //    System.out.println(deploy.getKey());
    //    System.out.println(deploy.getParentDeploymentId());
    //
    //    // 部署表单 挂载 挂载部署流程的deploy.getId()
    //    FormDeployment formDeployment = formRepositoryService.createDeployment()
    //            // 表单位置
    //            .addClasspathResource("form/steveCodeForm.form")
    //            // 给部署的表单起个名字
    //            .name("外置表单form")
    //            // 流程部署id
    //            .parentDeploymentId(deploy.getId())
    //            .deploy();
    //    System.out.println(formDeployment.getId());
    //    System.out.println(formDeployment.getName());
    //    System.out.println(formDeployment.getParentDeploymentId());
    //}


    /**
     * 启动流程时填写表单数据
     */
    @Test
    void statusFlowableForm() {
        // cd52c349-3ba1-11ed-a424-dc41a90b0909 from_id
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId("cd03b8e5-3ba1-11ed-a424-dc41a90b0909").singleResult();

        Map<String, Object> map = new HashMap<>();
        map.put("reason", "回家生孩子");
        map.put("startTime", new Date().toString());
        map.put("endTime", new Date().toString());
        map.put("days","30");
        map.put("startUserId", "songxy");
        /**
         * 参数：
         * processDefinitionId – 流程定义的 id，不能为空。
         * 结果——开始表单的表单结果，可以为空。
         * variables - 要传递的变量，可以为 null
         * processInstanceName – 要启动的流程实例的名称，可以为空。
         */
        //formService.submitStartFormData(processDefinition.getId(),map);
        runtimeService.startProcessInstanceWithForm(processDefinition.getId(),"外置表单流程结果",map,"又是请假-外置表单");
    }

    /**
     * 获取表单提交的数据
     */
    @Test
    void formData() {
        TaskFormData taskFormData = formService.getTaskFormData("00453d3c-3b55-11ed-aa82-dc41a90b0909");
        System.out.println(taskFormData);
        FormInfo taskFormModel = taskService.getTaskFormModel("00453d3c-3b55-11ed-aa82-dc41a90b0909");
        SimpleFormModel formModel = (SimpleFormModel) taskFormModel.getFormModel();
        System.out.println(formModel.getFields());
    }
    /**
     * 启动流程
     */
    @Test
    void statusFlowable() {
        final String processId = "key-bx";
        Map<String, Object> map = new HashMap<>();
        map.put("hrUserId", "songxy");
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processId, map);
        
        System.out.println(processInstance.getId());
    }
    
    /**
     * 查询部署的流程定义
     */
    @Test
    void findProcessDefinition() {
        //不分页
        List<ProcessDefinition> list = repositoryService
                .createProcessDefinitionQuery()
                .processDefinitionKey("key-1")
                .list();
        list.forEach(processDefinition -> {
            System.out.println(processDefinition);
        });
        System.out.println("_________________");
        //分页
        List<ProcessDefinition> pages = repositoryService.createProcessDefinitionQuery().processDefinitionKey("key-1")
                .listPage(0, 10);
        pages.forEach(processDefinition -> {
            System.out.println(processDefinition);
        });
    }
    
    /**
     * 查询指定流程所有启动的实例列表
     */
    @Test
    void findExecutions() {
        //不分页
        List<Execution> executions = runtimeService.createExecutionQuery().processDefinitionKey("key-1").list();
        executions.forEach(execution -> {
            System.out.println(execution);
        });
        System.out.println("-----------");
        //分页
        List<Execution> executionPages = runtimeService.createExecutionQuery().processDefinitionKey("key-1")
                .listPage(0, 10);
        executionPages.forEach(execution -> {
            System.out.println(execution);
        });
    }
    
    /**
     * 查询当用户待办
     */
    @Test
    void findUserList() {
        String userId = "songxy";
        //不分页
        List<Task> list = taskService.createTaskQuery().taskAssignee(userId).orderByTaskCreateTime().desc().list();
        list.forEach(task -> {
            System.out.println(task);
        });
        //分页
        List<Task> pages = taskService.createTaskQuery().taskAssignee(userId).orderByTaskCreateTime().desc()
                .listPage(0, 10);
        
    }
    
    /**
     * 审批任务  通过或者驳回
     */
    @Test
    void agreeOrRefuse() {
        Map<String, Object> map = new HashMap<>();
        map.put("financeUserId", "yqmm");
        map.put("money", "100");
        //        map.put("outcome", "驳回");
        //任务id
        String taskId = "55130c96-8429-11ec-8070-dc41a90b0909";
        String userId = "yqmm";
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        //领取任务
        taskService.claim(task.getId(), userId);
        // 完成
        taskService.complete(task.getId(),map);
    }
    
    /**
     * 已办列表
     */
    @Test
    void done() {
        String userId = "songxy";
        List<DoneEntity> list = flowableMapper.doneByUserId(userId);
        list.forEach(doneEntity -> {
            System.out.println(doneEntity);
        });
    }
    
    /**
     * 生成流程图
     *
     *
     */
    @Test
    void genProcessDiagram() throws Exception {
        String processId = "e16b2b78-8406-11ec-b6f5-dc41a90b0909";
        ProcessInstance pi = runtimeService.createProcessInstanceQuery().processInstanceId(processId).singleResult();
        
        //流程走完的不显示图
        if (pi == null) {
            return;
        }
        Task task = taskService.createTaskQuery().processInstanceId(pi.getId()).singleResult();
        //使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
        String InstanceId = task.getProcessInstanceId();
        List<Execution> executions = runtimeService.createExecutionQuery().processInstanceId(InstanceId).list();
        
        //得到正在执行的Activity的Id
        List<String> activityIds = new ArrayList<>();
        List<String> flows = new ArrayList<>();
        for (Execution exe : executions) {
            List<String> ids = runtimeService.getActiveActivityIds(exe.getId());
            activityIds.addAll(ids);
        }
        
        //获取流程图
        BpmnModel bpmnModel = repositoryService.getBpmnModel(pi.getProcessDefinitionId());
        ProcessEngineConfiguration engconf = processEngine.getProcessEngineConfiguration();
        ProcessDiagramGenerator diagramGenerator = engconf.getProcessDiagramGenerator();
        InputStream in = diagramGenerator.generateDiagram(bpmnModel, "png", activityIds, flows,
                engconf.getActivityFontName(), engconf.getLabelFontName(), engconf.getAnnotationFontName(),
                engconf.getClassLoader(), 1, true);
        OutputStream out = null;
        byte[] buf = new byte[1024];
        int legth = 0;
        try {
            out = httpServletResponse.getOutputStream();
            while ((legth = in.read(buf)) != -1) {
                out.write(buf, 0, legth);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
    
    @Test
    void reject() {
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId("1f4594e9-83f6-11ec-9379-dc41a90b0909")
//                .moveActivityIdsToSingleActivityId("5ab4100f-83fc-11ec-95f0-dc41a90b0909","333")
                .changeState();
    }
}
