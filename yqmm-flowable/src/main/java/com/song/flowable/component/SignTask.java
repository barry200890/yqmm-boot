/*******************************************************************************
 * Package: com.song.flowable.component
 * Type:    SignTask
 * Date:    2022-09-16 22:04
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.component;


import org.flowable.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 功能描述：
 * @author Songxianyang
 * @date 2022-09-16 22:04
 */
@Component("signTask")
public class SignTask {
    // 日志
    private static final Logger log= LoggerFactory.getLogger(SignTask.class);

    public boolean signTaskTo(DelegateExecution execution){
        log.info("任务会签总数："+execution.getVariable("nrOfInstances"));
        log.info("待会签任务数量："+execution.getVariable("nrOfActiveInstances"));
        log.info("已完成会签任务数量："+execution.getVariable("nrOfCompletedInstances"));
        //同意人数
        Integer agree = (Integer) execution.getVariable("agree");
        /*
        ==1 会签
        >= 0.5 就是或签 也就是有一半得人去审批即可（50%）
         */
        if(agree/(Integer) execution.getVariable("nrOfInstances")==1){
            log.info("同意人数："+agree);
            //有网关得时候去去设置
            //execution.setVariable("key","value");
            return  true;
        }else{
            return  false;
        }
    }
}
