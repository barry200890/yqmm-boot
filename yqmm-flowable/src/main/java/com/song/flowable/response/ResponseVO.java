/*******************************************************************************
 * Package: com.song.common.response
 * Type:    ResponseVO
 * Date:    2022-10-04 17:35
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.response;

import com.song.common.code.ResultCode;
import lombok.*;

import java.io.Serializable;

/**
 * 功能描述： 对外响应的Vo
 *
 * @author Songxianyang
 * @date 2022-10-04 17:35
 */
@Data
public class ResponseVO<T> implements Serializable {
    /**
     * 响应码
     */
    private Integer code;
    /**
     * 响应的信息
     */
    private String msg;
    /**
     * 具体的请数据
     */
    private T data;

    public ResponseVO() {
    }

    public ResponseVO(T data) {
        this.data = data;
    }

    public ResponseVO(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResponseVO<T> error(Integer code, String msg) {
        return new ResponseVO<T>(code, msg, null);
    }
    public static <T> ResponseVO<T> error() {
        return new ResponseVO<T>(ResultCode.FAIL.getCode(), ResultCode.FAIL.getMsg(), null);
    }

    public static <T> ResponseVO<T> success() {
        return new ResponseVO<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), null);
    }

    public static <T> ResponseVO<T> error(String msg) {
        return new ResponseVO<T>(ResultCode.FAIL.getCode(), msg, null);
    }

    public static <T> ResponseVO<T> success(T data) {
        return new ResponseVO<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), data);
    }

    public static <T> ResponseVO<T> success(String msg) {
        return new ResponseVO<T>(ResultCode.SUCCESS.getCode(), msg, null);
    }

    public static <T> ResponseVO<T> success(String msg, T data) {
        return new ResponseVO<T>(ResultCode.SUCCESS.getCode(), msg, data);
    }
}
