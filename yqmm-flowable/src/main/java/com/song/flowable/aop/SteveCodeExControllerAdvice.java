/*******************************************************************************
 * Package: com.song.flowable.aop
 * Type:    ExControllerAdvice
 * Date:    2022-10-20 22:12
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.aop;

import com.song.common.code.ResultCode;
import com.song.flowable.exception.ServiceException;
import com.song.flowable.response.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-10-20 22:12
 */
@RestControllerAdvice
@Slf4j
public class SteveCodeExControllerAdvice {

    @ExceptionHandler({ServiceException.class})
    public ResponseVO<Object> serviceException(HttpServletRequest request, ServiceException ex) {
        this.commonProcess(request, ex);
        return ResponseVO.error(ex.getCode(), ex.getMsg());
    }
    private void commonProcess(HttpServletRequest request, Exception ex) {
        log.error("Request url : " + request.getRequestURI(), ex);
    }

    /**-------- 通用异常处理方法 --------**/
    @ExceptionHandler(Exception.class)
    public ResponseVO error(Exception e) {
        e.printStackTrace();
        // 通用异常结果
        return ResponseVO.error();
    }

    /**-------- 指定异常处理方法 --------**/
    @ExceptionHandler(NullPointerException.class)
    public ResponseVO error(NullPointerException e) {
        e.printStackTrace();
        return ResponseVO.error(ResultCode.NullPointerException.getCode(), ResultCode.NullPointerException.getMsg());
    }
}
