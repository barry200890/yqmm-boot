/*******************************************************************************
 * Package: com.song.common.web.advice
 * Type:    SteveCodeResponseBodyAdvice
 * Date:    2022-10-19 21:56
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.song.common.code.ResultCode;
import com.song.common.web.ResponseInfoSkin;
import com.song.flowable.response.ResponseVO;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 功能描述： 请求的前置处理
 *
 * @author Songxianyang
 * @date 2022-10-19 21:56
 */
@ControllerAdvice
public class SteveCodeResponseBodyAdvice implements ResponseBodyAdvice<Object> , InitializingBean{
    @Resource
    private ApplicationContext applicationContext;
    private boolean hasAnotherResponseBodyAdvice;

    /**
     * 获取注解信息
     * @param methodParameter
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        if (this.hasAnotherResponseBodyAdvice) {
            return false;
        }
        ResponseInfoSkin methodAnnotation = (ResponseInfoSkin)methodParameter.getMethodAnnotation(ResponseInfoSkin.class);
        if (methodAnnotation != null) {
            return true;
        } else {
            ResponseInfoSkin classAnnotation = (ResponseInfoSkin)methodParameter.getExecutable().getDeclaringClass().getAnnotation(ResponseInfoSkin.class);
            return classAnnotation != null;
        }
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        if (body instanceof String) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                // 将数据包装在JsonResponse里返回
                return objectMapper.writeValueAsString(ResponseVO.success(body));
            } catch (JsonProcessingException e) {
                throw new RuntimeException("返回String类型错误");
            }
        }
        if (Objects.isNull(body)) {
            ResponseVO.success();
        }
        if (body instanceof ResponseVO) {
            return body;
        } else {
          return  ResponseVO.success(body);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.hasAnotherResponseBodyAdvice = this.applicationContext.getBeansOfType(ResponseBodyAdvice.class).values().size() > 1;
    }
}
