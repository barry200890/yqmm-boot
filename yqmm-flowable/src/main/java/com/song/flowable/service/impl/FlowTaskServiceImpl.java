/*******************************************************************************
 * Package: com.song.flowable.service.impl
 * Type:    FlowTaskServiceImpl
 * Date:    2023-11-28 22:55
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service.impl;

import com.song.flowable.service.FlowTaskService;
import com.song.flowable.vo.ReturnTaskListVO;
import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.constants.BpmnXMLConstants;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.FlowNode;
import org.flowable.bpmn.model.Process;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.impl.util.ExecutionGraphUtil;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:55
 */
@Service
public class FlowTaskServiceImpl implements FlowTaskService {
    @Resource
    private TaskService taskService;
    @Resource
    private RepositoryService repositoryService;
    @Resource
    private HistoryService historyService;
    /**
     * 更新任务的负责人(转办或者转派负责人)
     *
     * @param taskId
     * @param targetUserId
     * @return
     */
    @Override
    public Boolean updateTaskAssignee(String taskId, String targetUserId) {
        taskService.setAssignee(taskId, targetUserId);
        return true;
    }

    /**
     * 查询可退回的节点列表
     * https://blog.csdn.net/suprezheng/article/details/122627461
     * @param taskId
     * @return
     */
    @Override
    public List<ReturnTaskListVO> getReturnTaskList(String taskId) {
        // 初始化返回结果列表
        List<ReturnTaskListVO> result = new ArrayList<>(16);
        if (StringUtils.isBlank(taskId)) {
            return result;
        }
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) {
            return result;
        }
        // 任务定义key 等于 当前任务节点id
        String taskDefinitionKey = task.getTaskDefinitionKey();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
        Process mainProcess = bpmnModel.getMainProcess();
        // 当前节点
        FlowNode currentFlowElement = (FlowNode) mainProcess.getFlowElement(taskDefinitionKey, true);
        // 查询历史节点实例
        List<HistoricActivityInstance> activityInstanceList = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .finished()
                .orderByHistoricActivityInstanceEndTime().asc().list();
        List<String> activityIdList = activityInstanceList.stream()
                .filter(activityInstance ->
                        BpmnXMLConstants.ELEMENT_TASK_USER.equals(activityInstance.getActivityType()) || BpmnXMLConstants.ELEMENT_EVENT_START.equals(activityInstance.getActivityType()))
                .map(HistoricActivityInstance::getActivityId)
                .filter(activityId -> !taskDefinitionKey.equals(activityId))
                .distinct()
                .collect(Collectors.toList());
        for (String activityId : activityIdList) {
            // 回退到主流程的节点
            FlowNode toBackFlowElement = (FlowNode) mainProcess.getFlowElement(activityId, true);
            // 判断 【工具类判断是否可以从源节点 到 目标节点】
            Set<String> set = new HashSet<>();
            if (toBackFlowElement != null && ExecutionGraphUtil.isReachable(mainProcess, toBackFlowElement, currentFlowElement, set)) {
                ReturnTaskListVO workFlowNodeDTO = new ReturnTaskListVO();
                workFlowNodeDTO.setNodeId(activityId);
                workFlowNodeDTO.setNodeName(toBackFlowElement.getName());
                result.add(workFlowNodeDTO);
            }
        }
        return result;
    }
}
