/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    FlowTaskService
 * Date:    2023-11-28 22:53
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.song.flowable.vo.ReturnTaskListVO;

import java.util.List;

/**
 * 功能描述：流程任务
 *
 * @author Songxianyang
 * @date 2023-11-28 22:53
 */
public interface FlowTaskService {
    /**
     * 更新任务的负责人(转办或者转派负责人)
     * @param taskId
     * @param targetUserId
     * @return
     */
    Boolean updateTaskAssignee(String taskId, String targetUserId);

    /**
     * 查询可退回的节点列表
     * @param taskId
     * @return
     */
    List<ReturnTaskListVO> getReturnTaskList(String taskId);
}
