/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    ProcessInstanceService
 * Date:    2023-11-28 22:19
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:19
 */
public interface ProcessInstanceService {
    /**
     * 获得指定流程实例
     * @param id
     * @return ProcessDefinition
     */
    HistoricProcessInstance getProcessInstance(String id);

}
