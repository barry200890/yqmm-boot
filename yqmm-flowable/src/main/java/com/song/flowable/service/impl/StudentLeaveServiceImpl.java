/*******************************************************************************
 * Package: com.song.flowable.service.impl
 * Type:    StudentLeaveServiceImpl
 * Date:    2022-05-16 21:09
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service.impl;

import com.song.flowable.dto.StartFlowableDTO;
import com.song.flowable.entity.StudentLeaveDO;
import com.song.flowable.mapper.IStudentLeaveMapper;
import com.song.flowable.service.FlowableService;
import com.song.flowable.service.StudentLeaveService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-16 21:09
 */
@Service
public class StudentLeaveServiceImpl implements StudentLeaveService {
    @Resource
    private FlowableService flowableService;
    
    @Resource
    private IStudentLeaveMapper iStudentLeaveMapper;
    
    @Override
    @Transactional
    public String leaveStudent(StudentLeaveDO leaveDO) {
        //先插入 业务表
        iStudentLeaveMapper.insertIgnoreNull(leaveDO);
        StartFlowableDTO startFlowableDTO = new StartFlowableDTO();
        startFlowableDTO.setProcessDefinitionKey("student-key");
        HashMap<String, Object> map = new HashMap<>(4);
        map.put("name",leaveDO.getName());
        map.put("time",leaveDO.getTime());
        map.put("days",leaveDO.getDays());
        map.put("reason",leaveDO.getReason());
        map.put("monitorId", leaveDO.getUserId());
        startFlowableDTO.setMap(map);
        return flowableService.startFlowable(startFlowableDTO);
    }
}
