/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    ModelService
 * Date:    2023-11-27 20:59
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.song.flowable.dto.ModelRespDTO;
import com.song.flowable.dto.ModelPageReqDTO;
import com.song.flowable.vo.ModelVO;
import org.flowable.engine.repository.Model;

import java.util.List;

/**
 * 功能描述：Flowable流程模型
 *
 * @author Songxianyang
 * @date 2023-11-27 20:59
 */

public interface ModelService {
    /**
     * 获得流程模型分页
     *
     * @param dto 分页查询
     * @return Model
     */
    List<ModelVO> getModelPage(ModelPageReqDTO dto);

    /**
     * 删除模型
     *
     * @param id 编号
     */
    Boolean deleteModel(String id);

    /**
     * 修改模型部署的流程定义状态
     *
     * @param id 编号 （1激活 2停止、挂起、暂停）
     * @param state 状态
     */
    String  updateModelState(String id, Integer state);
    /**
     * 获得流程模块
     *
     * @param id 编号
     * @return ModelRespDTO
     */
    ModelVO getModel(String id);
}
