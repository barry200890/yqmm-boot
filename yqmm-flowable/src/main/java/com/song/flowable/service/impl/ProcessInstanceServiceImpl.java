/*******************************************************************************
 * Package: com.song.flowable.service.impl
 * Type:    ProcessInstanceServiceImpl
 * Date:    2023-11-28 22:24
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service.impl;

import com.song.flowable.service.ProcessInstanceService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:24
 */
@Service
public class ProcessInstanceServiceImpl implements ProcessInstanceService {

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private HistoryService historyService;

    /**
     * 获得指定流程实例
     *
     * @param id
     * @return processInstanceId
     */
    @Override
    public HistoricProcessInstance getProcessInstance(String processInstanceId) {
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
       return historicProcessInstance;
    }
}
