/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    StudentLeaveService
 * Date:    2022-05-16 21:08
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.song.flowable.entity.StudentLeaveDO;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-16 21:08
 */
public interface StudentLeaveService {
    /**
     * 学生请假
     */
    String leaveStudent(StudentLeaveDO leaveDO);
}
