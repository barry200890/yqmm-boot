/*******************************************************************************
 * Package: com.song.flowable.service
 * Type:    FlowableService
 * Date:    2022-02-05 22:06
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.service;

import com.song.flowable.dto.*;

import com.song.flowable.entity.DoneEntity;
import com.song.flowable.vo.QueryHistoryActivityVO;
import com.song.flowable.vo.ReturnTaskVo;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.flowable.task.api.Task;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-05 22:06
 */
public interface FlowableService {
    /**
     * 查看流程图
     * @param httpServletResponse 相应
     * @param processId  流程实例id
     */
    void genProcessDiagram(HttpServletResponse httpServletResponse, String processId) throws Exception;
    
    /**
     * 流程部署
     * @param dto 流程部署对象
     * @return
     */
    String createDeployment(DeploymentDTO dto);
    
    /**
     * 流程部署 列表
     * @param dto
     * @return
     */
    List<ProcessDefinition> getProcessDefinitionList(DeploymentDTO dto);
    
    /**
     * 启动流程
     * @param dto  启动流程参数
     * @return
     */
    String startFlowable(StartFlowableDTO dto);
    
    /**
     * 查询所有启动的流程列表
     * @param dto
     * @return
     */
    List<Execution> executions(StartFlowableDTO dto);
    
    /**
     * 用户待办
     * @param dto
     * @return
     */
    List<ReturnTaskVo> todoList(TodoDTO dto);
    /**
     * 用户已办
     * @param dto
     * @return
     */
    List<DoneEntity> doneList(DoneDTO dto);
    
    /**
     * 通过或者拒绝
     * @param dto
     * @return
     */
    String acceptOrReject(AcceptOrRejectDTO dto);
    
    /**
     * 获取指定用户组流程任务列表
     * @param group
     * @return List<Task>
     */
    List<Task> tasks(String group);
    
    /**
     *查看历史流程记录
     * @param processInstanceId
     * @return List
     */
    List<QueryHistoryActivityVO> historicActivityInstances(String processInstanceId);
    /**
     * 驳回流程
     * @param targetTaskKey
     * @param taskId
     * @return Task
     */
    String  currentTask(String taskId, String targetTaskKey);
    
    /**
     *终止流程
     * @param processInstanceId
     * @param reason 原因
     * @return string
     */
    String deleteProcessInstanceById(String processInstanceId,String reason);
    /**
     * 批量删除流程实例
     * @param dto
     * @return
     */
    String batchDeleteProcessInstanceById(BatchDeleteProcessInstanceDTO dto);
    /**
     *挂起流程实例
     * @param processInstanceId
     * @return string
     */
    String handUpProcessInstance(String processInstanceId);
    /**
     *（唤醒）被挂起的流程实例
     * @param processInstanceId
     * @return string
     */
    String activateProcessInstance(String processInstanceId);
     /**
     *判断传入流程实例在运行中是否存在
      * @param processInstanceId
      * @return 布尔
     */
     Boolean isExistProcIntRunning(String processInstanceId);
     /**
     *我发起的流程实例列表
      * @param userId
      * @return list
     */
     List<HistoricProcessInstance> getMyStartProcint(String userId);

    /**
     * 多实例加签
     * @param addMultiInstance
     * @return
     */
     String addMultiInstanceExecution(AddMultiInstanceDTO addMultiInstance);

    /**
     * 多实例减签
     * @param executionId 具体减签 主键id
     * @return
     */
    String deleteMultiInstanceExecution(String executionId);

    /**
     * 查询会签节点的审批人任务
     * @param dto
     * @return
     */
    List<ReturnTaskVo> signTaskUsers(SignTaskUserDTO dto);

    String flowReturn(FlowReturnDTO dto);
}
