package com.song.flowable.service;


import com.song.flowable.dto.ProcessDefinitionPageDTO;
import com.song.flowable.vo.ProcessDefinitionVO;
import org.flowable.engine.repository.ProcessDefinition;

import java.util.List;

/**
 * 功能描述：流程定义
 *
 * @author Songxianyang
 * @date 2022-02-05 22:18
 */
public interface BpmProcessDefinitionService {

    /**
     * 获得流程定义分页
     *
     * @param dto 分页入参
     * @return 流程定义 Page
     */
    List<ProcessDefinitionVO> getProcessDefinitionPage(ProcessDefinitionPageDTO dto);


    /**
     * 获得流程定义对应的 BPMN XML
     *
     * @param id 流程定义编号
     * @return BPMN XML
     */
    String getXML(String id);
}
