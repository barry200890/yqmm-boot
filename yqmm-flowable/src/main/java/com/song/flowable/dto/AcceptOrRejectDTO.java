/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    AcceptOrreject
 * Date:    2022-02-05 23:49
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 功能描述：接受或者拒绝
 *
 * @author Songxianyang
 * @date 2022-02-05 23:49
 */
@Data
@ApiModel("接受拒绝入参")
public class AcceptOrRejectDTO {
    /**
     * 任务id
     */
    @ApiModelProperty("任务id")
    private String taskId;
    
    /**
     * 审批人id
     */
    @ApiModelProperty("审批人id")
    private String userId;
    
    /**
     * 流程中的变量 会签时也是这个变量
     */
    @ApiModelProperty("流程中的变量")
    private Map<String, Object> map;

    /**
     * 审批意见
     */
    @ApiModelProperty("审批意见")
    private String  message;
    /**
     * 流程实例id
     */
    @ApiModelProperty("流程实例id")
    private String  processInstanceId;

}
