/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    BatchDeleteProcessInstanceDTO
 * Date:    2023-11-23 9:41
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

/**
 * 功能描述：批量删除流程实例
 *
 * @author Songxianyang
 * @date 2023-11-23 9:41
 */
@Data
@ApiModel("批量删除流程实例入参")
public class BatchDeleteProcessInstanceDTO {
    @ApiModelProperty("流程实例id集合")
    @NonNull
    private List<String> processInstanceIds;
    @ApiModelProperty("删除原因")
    @NonNull
    private String reason;
}
