/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    SignTaskUserDTO
 * Date:    2022-09-18 16:17
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

/**
 * 功能描述：会签节点审批入参
 *
 * @author Songxianyang
 * @date 2022-09-18 16:17
 */
@ApiModel("加签对象入参")
@Data
@NoArgsConstructor
public class SignTaskUserDTO implements Serializable {
    @ApiModelProperty("流程实例id")
    @NonNull
    private String processInstanceId;
    @ApiModelProperty("节点id")
    private String nodeId;
    @ApiModelProperty("节点名称")
    private String nodeName;
}
