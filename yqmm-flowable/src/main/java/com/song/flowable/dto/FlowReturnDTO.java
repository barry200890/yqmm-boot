/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    FlowReturn
 * Date:    2023-11-19 15:55
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述： 流程退回
 *
 * @author Songxianyang
 * @date 2023-11-19 15:55
 */
@ApiModel("流程退回")
@Data
public class FlowReturnDTO {

    @ApiModelProperty("当前节点id")
    private String currentUserTaskId;


    @ApiModelProperty("目标节点id")
    private String targetUserTaskId;


    @ApiModelProperty("流程实例id")
    private String processInstanceId;
}
