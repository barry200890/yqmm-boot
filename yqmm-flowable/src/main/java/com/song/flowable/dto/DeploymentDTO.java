/*******************************************************************************
 * Package: com.song.flowable.entity
 * Type:    DeploymentDTO
 * Date:    2022-02-05 23:01
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 功能描述：部署对象
 *
 * @author Songxianyang
 * @date 2022-02-05 23:01
 */

@ApiModel("部署入参")
@Data
public class DeploymentDTO {
    /**
     * 部署的流程名称
     */
    private String deploymentFlowName;
    
    /**
     * resource文件路径
     */
    private String resourceBpmnPath;

    /**
     * resource文件路径
     */
    private String resourceFromPath;
    
    /**
     * 查询部署的流程列表-流程key
     */
    private String processDefinitionKey;
    
    /**
     * 那一页开始
     */
    private Integer page;
    
    /**
     * 每页显示条数
     */
    private Integer pageSize;
}
