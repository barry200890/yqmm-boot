/*******************************************************************************
 * Package: com.song.flowable.dto
 * Type:    AddMultiInstance
 * Date:    2022-09-18 9:44
 *
 *
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

import java.util.List;


/**
 * 功能描述： 加签对象
 *
 * @author Songxianyang
 * @date 2022-09-18 9:44
 */
@Data
@ApiModel("加签对象入参")
public class AddMultiInstanceDTO {
    @ApiModelProperty("流程实例id")
    @NonNull
    private String processInstanceId;

    @ApiModelProperty("当前会签流程节点id")
    @NonNull
    private String activityId;

    @ApiModelProperty("支持一次加签多个人")
    @NonNull
    private List<String> addLabelUserId;
}
