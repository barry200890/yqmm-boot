/*******************************************************************************
 * Package: com.song.flowable.convert
 * Type:    ModelConvert
 * Date:    2023-11-29 17:31
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.convert;

import com.song.flowable.entity.ActDeModelDO;
import com.song.flowable.vo.ModelVO;
import com.song.flowable.vo.ReturnTaskVo;
import org.flowable.task.api.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-29 17:31
 */
@Mapper
public interface ModelConvert {

    ModelConvert INSTANCE = Mappers.getMapper(ModelConvert.class);
    @Mappings({
            @Mapping(source = "modelKey", target = "key"),
            @Mapping(source = "modelType", target = "category"),
            @Mapping(source = "created", target = "createTime"),
            @Mapping(source = "lastUpdated", target = "lastUpdateTime"),
    })
    List<ModelVO> toModelVOList(List<ActDeModelDO> source);


    ModelVO toModelVO(ActDeModelDO source)  ;


}
