/*******************************************************************************
 * Package: com.song.flowable.utils
 * Type:    TaskConvert
 * Date:    2022-02-06 15:04
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.convert;

import com.song.flowable.vo.ReturnTaskVo;

import org.flowable.task.api.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-06 15:04
 */
@Mapper
public interface TaskConvert {
    TaskConvert INSTANCE = Mappers.getMapper(TaskConvert.class);
    
    @Mappings({
            @Mapping(source = "id", target = "taskId"),
    })
    ReturnTaskVo toConvertTaskVo(Task source)  ;
    
    List<ReturnTaskVo> toConvertTaskVoList(List<Task> source);
}
