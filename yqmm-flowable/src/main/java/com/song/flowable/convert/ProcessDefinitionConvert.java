/*******************************************************************************
 * Package: com.song.flowable.convert
 * Type:    ProcessDefinitionConvert
 * Date:    2023-11-29 16:51
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.convert;

import com.song.flowable.vo.ProcessDefinitionVO;
import org.flowable.engine.repository.ProcessDefinition;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-29 16:51
 */
@Mapper
public interface ProcessDefinitionConvert {
    ProcessDefinitionConvert INSTANCE = Mappers.getMapper(ProcessDefinitionConvert.class);
    List<ProcessDefinitionVO> toConvertProcessDefinitionList(List<ProcessDefinition> source);
}
