/*******************************************************************************
 * Package: com.song.common.exception
 * Type:    ServiceException
 * Date:    2022-10-20 18:09
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.exception;

import com.song.common.code.ResultCode;
import lombok.Data;

/**
 * 功能描述： 业务层异常抽象
 *
 * @author Songxianyang
 * @date 2022-10-20 18:09
 */
@Data
public abstract class ServiceException extends RuntimeException{
    /**
     * 错误码
     */
    private Integer code;
    /**
     * 错误信息
     */
    private String msg;


    public ServiceException(String message) {
        this.msg=message;
        this.code = ResultCode.FAIL.getCode();
    }
}
