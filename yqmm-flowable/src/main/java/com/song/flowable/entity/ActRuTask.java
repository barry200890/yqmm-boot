/*******************************************************************************
 * Package: com.song.flowable.entity
 * Type:    ActRuTaskDO
 * Date:    2022-09-18 11:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：
 *bug
 * @author Songxianyang
 * @date 2022-09-18 11:44
 */
@Data
@TableName("ACT_RU_TASK")
public class ActRuTask {
    @TableId("id_")
    private String id;
    @TableField("rev_")
    private Integer rev;
    private String executionId_;
    private String procInstId_;
    private String procDefId_;
    private String taskDefId_;
    private String scopeId_;
    private String subScopeId_;
    private String scopeType_;
    private String scopeDefinitionId_;
    private String propagatedStageInstId_;
    private String name_;
    private String parentTaskId_;
    private String description_;
    @TableField("TASK_DEF_KEY_")
    private String taskDefKey;
    private String owner_;
    private String assignee_;
    private String delegation_;
    private Integer priority_;
    private Date createTime_;
    private Date dueDate_;
    private String category_;
    private Integer suspensionState_;
    private String tenantId_;
    private String formKey_;
    private Date claimTime_;
    private Byte isCountEnabled_;
    private Integer varCount_;
    private Integer idLinkCount_;
    private Integer subTaskCount_;
}
