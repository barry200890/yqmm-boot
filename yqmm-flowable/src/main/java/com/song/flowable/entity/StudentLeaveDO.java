package com.song.flowable.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@ApiModel(value = "StudentLeaveDO", description = "do")
public class StudentLeaveDO {
	private Integer id;
	/** 请假人 */
	@ApiModelProperty(value = "请假人", name = "name")
	private String name;
	/** 请假时间 */
	@ApiModelProperty(value = "请假时间", name = "time")
	private Date time;
	/** 天数 */
	@ApiModelProperty(value = "天数", name = "days")
	private Long days;
	/** 请假原因 */
	@ApiModelProperty(value = "请假原因", name = "reason")
	private String reason;

	public Map<String, ArrayList<String>> getListMap() {
		return listMap;
	}

	public void setListMap(Map<String, ArrayList<String>> listMap) {
		this.listMap = listMap;
	}

	@ApiModelProperty(value = "测试用例", name = "listMap")
	private Map<String, ArrayList<String>> listMap;

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@ApiModelProperty(value = "下一步审核人id", name = "userId")
	private String userId;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getTime() {
		return time;
	}
	
	public void setTime(Date time) {
		this.time = time;
	}
	
	public Long getDays() {
		return days;
	}
	
	public void setDays(Long days) {
		this.days = days;
	}
	
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
}