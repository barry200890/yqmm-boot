/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ReturnTaskVo
 * Date:    2022-02-06 15:02
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述：Task居然不是个实体，是一个接口。我们需要封装起来
 *
 * @author Songxianyang
 * @date 2022-02-06 15:02
 */
@ApiModel("返回Task")
@Data
public class ReturnTaskVo {
    @ApiModelProperty("任务id")
    private String taskId;

    @ApiModelProperty("父任务id")
    private String parentTaskId;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("启动时流程实例id")
    private String processInstanceId;

    @ApiModelProperty("运行时实例id")
    private String executionId;

    @ApiModelProperty("节点id")
    private String nodeId;

    @ApiModelProperty("审批人")
    private String assignee;
}
