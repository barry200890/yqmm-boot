/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    ModelVO
 * Date:    2023-11-29 17:26
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述：流程模型VO
 * ModelConvert
 *
 * @author Songxianyang
 * @date 2023-11-29 17:26
 */
@ApiModel("流程模型VO")
@Data
public class ModelVO {
    private String id;
    private String name;
    private String key;
    private String category;
    private Date createTime;
    private Date lastUpdateTime;
    private Integer version;
    private String metaInfo;
    private String deploymentId;
    private String tenantId;
    /**
     * 查询当前xml
     */
    private String flowXml;
}
