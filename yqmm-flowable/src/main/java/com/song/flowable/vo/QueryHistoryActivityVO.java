/*******************************************************************************
 * Package: com.song.flowable.vo
 * Type:    QueryHistoryActivityVO
 * Date:    2023-11-26 22:36
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Flowable工作流之查询历史流程信息：https://blog.csdn.net/weixin_38192427/article/details/127574163
 * 功能描述：查询历史活动信息
 * ACT_HI_ACTINST
 * flowable审批意见：https://blog.csdn.net/sinat_33151213/article/details/127477591
 *
 * @author Songxianyang
 * @date 2023-11-26 22:36
 */
@ApiModel("查询历史活动信息")
@Data
public class QueryHistoryActivityVO {
    /**
     * 历史活动信息id
     */
    @ApiModelProperty("历史活动信息id")

    private String historicActivityInstanceId;
    /**
     * 流程定义ID
     */
    @ApiModelProperty("流程定义ID")

    private String processDefinitionId;

    /**
     * 流程开始时间
     */
    @ApiModelProperty("流程开始时间")

    private Date startTime;

    /**
     * 流程结束时间
     */
    @ApiModelProperty("流程结束时间")

    private Date endTime;
    /**
     * 审批人
     */
    @ApiModelProperty("审批人")

    private String assignee;

    /**
     * 审批意见
     */
    @ApiModelProperty("审批意见")

    private String fullMessage;

}
