/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    TaskWeb
 * Date:    2023-11-28 22:38
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.flowable.service.FlowTaskService;
import com.song.flowable.vo.ReturnTaskListVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-28 22:38
 */
@Api(tags = "流程任务")
@RestController
@RequestMapping("/flow/task")
public class TaskWeb {
    @Resource
    private FlowTaskService flowTaskService;
    @GetMapping("/update-assignee")
    @ApiOperation("更新任务的负责人(转办或者转派负责人)")
    // taskId：任务id  targetUserId:转办目标人
    public Boolean updateTaskAssignee(@RequestParam("taskId") String taskId,@RequestParam("targetUserId") String targetUserId) {
        return flowTaskService.updateTaskAssignee(taskId, targetUserId);

    }

    @GetMapping("/get-return-task-list")
    @ApiOperation("查询可退回的节点列表-待优化")
    // taskId:当前任务ID
    // todo 待优化
    public List<ReturnTaskListVO> getReturnTaskList(@RequestParam("taskId") String taskId) {
        return flowTaskService.getReturnTaskList(taskId);

    }
}
