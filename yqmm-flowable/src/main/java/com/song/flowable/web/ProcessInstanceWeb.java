/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ProcessInstanceWeb
 * Date:    2023-11-28 22:13
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.flowable.service.ProcessInstanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 功能描述：流程实例
 *
 * @author Songxianyang
 * @date 2023-11-28 22:13
 */
@Api(tags = "流程实例")
@RestController
@RequestMapping("/flow/instance")
public class ProcessInstanceWeb {
    @Resource
    private ProcessInstanceService processInstanceService;
    @GetMapping("/get")
    @ApiOperation("获得指定流程实例")
    // id:流程实例的编号
    public HistoricProcessInstance getProcessInstance(@RequestParam("id") String id) {
        return processInstanceService.getProcessInstance(id);
    }
    //取消流程实例或者撤回发起的流程
}
