/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ModelWeb
 * Date:    2023-11-27 20:52
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.flowable.dto.ModelRespDTO;
import com.song.flowable.service.ModelService;
import com.song.flowable.dto.ModelPageReqDTO;
import com.song.flowable.vo.ModelUpdateStateVO;
import com.song.flowable.vo.ModelVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 功能描述：流程模型
 *
 * @author Songxianyang
 * @date 2023-11-27 20:52
 */
@Api(tags = "流程模型-暂时剔除")
@RestController
@RequestMapping("/flow/model")
public class ModelWeb {
    @Resource
    private ModelService modelService;

    @PostMapping("/page")
    @ApiOperation("列表分页")
    public List<ModelVO> getModelPage(@RequestBody ModelPageReqDTO dto) {
        return modelService.getModelPage(dto);
    }

    @GetMapping("/get")
    @ApiOperation("获得模型")
    public ModelVO getModel(@RequestParam("id") String id) {
        ModelVO model = modelService.getModel(id);
        return model;
    }

    @PostMapping("/update-state")
    @ApiOperation("修改模型部署的流程定义状态")
    public String updateModelState(@RequestBody ModelUpdateStateVO vo) {
        return modelService.updateModelState(vo.getId(), vo.getState());
    }

    @DeleteMapping("/delete")
    @ApiOperation( "删除模型")
    public Boolean deleteModel(@RequestParam("id") String id) {
        return modelService.deleteModel(id);
    }

}
