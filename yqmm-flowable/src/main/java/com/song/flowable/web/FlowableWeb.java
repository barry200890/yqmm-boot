/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    FlowableWeb
 * Date:    2022-02-05 22:22
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.common.web.ResponseInfoSkin;
import com.song.flowable.dto.*;
import com.song.flowable.entity.DoneEntity;
import com.song.flowable.service.FlowableService;
import com.song.flowable.convert.TaskConvert;
import com.song.flowable.vo.QueryHistoryActivityVO;
import com.song.flowable.vo.ReturnTaskVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.Execution;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-05 22:22
 */
@Api(tags = "工作流接口")
@RestController
@RequestMapping("flow")
@Slf4j
public class FlowableWeb {
    @Resource
    private FlowableService flowableService;
    
    @GetMapping("get-flow-chart")
    @ApiOperation("流程图")
    public void getFlowChart( HttpServletResponse response, String processId) throws Exception {
        flowableService.genProcessDiagram(response,processId);
    }
    
    /**
     * 流程部署
     * @param dto 流程部署对象
     * @return
     */
    @ApiOperation("流程部署")
    @PostMapping("create-deployment")
    public String createDeployment(@RequestBody DeploymentDTO dto) {
        return flowableService.createDeployment(dto);
    }


    /**
     * 部署流程以及绑定表单、部署表单
     */


    /**
     * 流程部署 列表
     * @param dto
     * @return
     */
    @ApiOperation("流程部署 列表")
    @PostMapping("get-process-definition-list")
    List<ProcessDefinition> getProcessDefinitionList(@RequestBody DeploymentDTO dto) {
        return flowableService.getProcessDefinitionList(dto);
    }
    
    /**
     * 启动流程
     * @param dto  启动流程参数
     * @return
     */
    @ApiOperation("启动流程")
    @PostMapping("start-flowable")
    String startFlowable(@RequestBody StartFlowableDTO dto){
        return flowableService.startFlowable(dto);
    }
    
    /**
     * 查询所有启动的流程列表
     * @param dto
     * @return
     */
    @ApiOperation("查询所有启动的流程列表")
    @PostMapping("executions")
    List<Execution> executions(@RequestBody StartFlowableDTO dto) {
        return flowableService.executions(dto);
    }
    
    
    /**
     * 用户待办
     * @param dto
     * @return ,produces = {"application/json;charset=utf-8"}
     */
    @ApiOperation("用户待办")
    @PostMapping("todo-list")
    @ResponseInfoSkin
    List<ReturnTaskVo> todoList(@RequestBody TodoDTO dto) {
        return flowableService.todoList(dto);
    }
    /**
     * 用户已办
     * @param dto
     * @return
     */
    @ApiOperation("用户已办")
    @PostMapping("done-list")
    List<DoneEntity> doneList(@RequestBody DoneDTO dto) {
        return flowableService.doneList(dto);
    }
    
    /**
     * 通过或者拒绝
     * @param dto
     * @return
     */
    @ApiOperation("通过或者拒绝")
    @PostMapping("accept-or-reject")
    String acceptOrReject(@RequestBody AcceptOrRejectDTO dto) {
        return flowableService.acceptOrReject(dto);
    }
    
    /**
     * 获取指定用户组流程任务列表
     * @param group
     * @return List<Task>
     */
    @ApiOperation("获取指定用户组流程任务列表")
    @GetMapping("tasks-group/{group}")
    List<ReturnTaskVo>  tasks(@PathVariable("group") String group) {
        return TaskConvert.INSTANCE.toConvertTaskVoList(flowableService.tasks(group));
    }
    
    /**
     *查看历史流程记录
     * @param processInstanceId
     * @return List
     */
    @ApiOperation("根据流程实例id查询当前历史流程记录")
    @GetMapping("historic-activity-instances/{processInstanceId}")
    List<QueryHistoryActivityVO> historicActivityInstances(@PathVariable String processInstanceId) {
        return flowableService.historicActivityInstances(processInstanceId);
    }
    /**
     * 驳回流程
     * @param targetTaskKey
     * @param taskId
     * @return Task
     */
    @ApiOperation("驳回流程")
    @GetMapping("current-task/{taskId}/{targetTaskKey}")
    String  currentTask(@PathVariable String taskId, @PathVariable String targetTaskKey){
        return flowableService.currentTask(taskId, targetTaskKey);
    }
    
    /**
     *终止流程
     * @param processInstanceId
     * @param reason 原因
     * @return string
     */
    @ApiOperation("终止流程")
    @GetMapping("delete-process-instance-by-id/{processInstanceId}/{reason}")
    String deleteProcessInstanceById(@PathVariable String processInstanceId,@PathVariable String reason) {
        return flowableService.deleteProcessInstanceById(processInstanceId, reason);
    }

    /**
     *批量删除流程实例
     * @param dto
     * @return string
     */
    @ApiOperation("批量删除流程实例")
    @PostMapping("batch-delete-process-instance-by-id")
    String batchDeleteProcessInstanceById(@RequestBody BatchDeleteProcessInstanceDTO dto) {
        return flowableService.batchDeleteProcessInstanceById(dto);
    }

    /**
     *挂起流程实例
     * @param processInstanceId
     * @return string
     */
    @ApiOperation("挂起流程实例")
    @GetMapping("hand-up-process-instance/{processInstanceId}")
    String handUpProcessInstance(@PathVariable String processInstanceId){
        return flowableService.handUpProcessInstance(processInstanceId);
    }
    /**
     *（唤醒）被挂起的流程实例
     * @param processInstanceId
     * @return string
     */
    @ApiOperation("（唤醒）被挂起的流程实例")
    @GetMapping("activate-process-instance/{processInstanceId}")
    String activateProcessInstance(@PathVariable String processInstanceId){
        return flowableService.activateProcessInstance(processInstanceId);
    }
    /**
     *判断传入流程实例在运行中是否存在
     * @param processInstanceId
     * @return 布尔
     */
    @ApiOperation("判断传入流程实例在运行中是否存在")
    @GetMapping("is-exist-proc-int-running/{processInstanceId}")
    Boolean isExistProcIntRunning(@PathVariable String processInstanceId) {
        return flowableService.isExistProcIntRunning(processInstanceId);
    }
    /**
     *我发起的流程实例列表
     * @param userId
     * @return list
     */
    @ApiOperation("我发起的流程实例列表")
    @GetMapping("get-my-start-procint/{userId}")
    List<HistoricProcessInstance> getMyStartProcint(@PathVariable String userId) {
        return flowableService.getMyStartProcint(userId);
    }

    /**
     * 多实例加签
     * @param addMultiInstance
     * @return
     */
    @ApiOperation("多实例加签")
    @PostMapping("add-multi-instance")
    String addMultiInstanceExecution(@RequestBody AddMultiInstanceDTO addMultiInstance) {
        return flowableService.addMultiInstanceExecution(addMultiInstance);
    }
    /**
     *多实例减签
     * @param executionId
     * @return String
     */
    @ApiOperation("多实例减签")
    @GetMapping("delete-multi-instance/{executionId}")
    @ApiImplicitParam(value = "运行时实例id",name = "executionId")
    String deleteMultiInstanceExecution(@PathVariable String executionId) {
        return flowableService.deleteMultiInstanceExecution(executionId);
    }

    /**
     * 查询会签节点的审批人任务
     */
    @ApiOperation("查询会签节点的审批人任务")
    @PostMapping("sign-task-users")
    List<ReturnTaskVo> signTaskUsers(@RequestBody SignTaskUserDTO dto) {
        return flowableService.signTaskUsers(dto);
    }

    @ApiOperation("退回")
    // 例如：在节点4直接退到节点1
    @PostMapping("flow-return")
    String  flowReturn(@RequestBody FlowReturnDTO dto) {
        return flowableService.flowReturn(dto);
    }

}
