/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    StudentLeaveWeb
 * Date:    2022-05-16 21:31
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.common.web.ResponseInfoSkin;
import com.song.flowable.entity.StudentLeaveDO;

import com.song.flowable.exception.FlowableException;
import com.song.flowable.service.StudentLeaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-05-16 21:31
 */
@Api(tags = "学生")
@RestController
@RequestMapping("student")
@Slf4j
@ResponseInfoSkin
public class StudentLeaveWeb {
    @Resource
    private StudentLeaveService studentLeaveService;
    @ApiOperation("流程部署")
    @PostMapping("leave-student")
    String leaveStudent(@RequestBody StudentLeaveDO leaveDO) {
       return studentLeaveService.leaveStudent(leaveDO);
    }
    @ApiOperation("返回id")
    @GetMapping("get-uuid")
    String getUuid() {
        throw new FlowableException("返回id");
        //return UUID.randomUUID().toString().replace("-","");
    }
}
