/*******************************************************************************
 * Package: com.song.flowable.web
 * Type:    ProcessDefinitionWeb
 * Date:    2023-11-27 22:13
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.web;

import com.song.flowable.dto.ProcessDefinitionPageDTO;
import com.song.flowable.service.BpmProcessDefinitionService;
import com.song.flowable.vo.ProcessDefinitionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.flowable.engine.repository.ProcessDefinition;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-11-27 22:13
 */
@Api(tags = "流程定义")
@RestController
@RequestMapping("/flow/definition")
public class ProcessDefinitionWeb {
    @Resource
    private BpmProcessDefinitionService bpmDefinitionService;

    @PostMapping("/page")
    @ApiOperation("列表分页")
    public List<ProcessDefinitionVO> getProcessDefinitionPage(
           @RequestBody ProcessDefinitionPageDTO dto) {
        return bpmDefinitionService.getProcessDefinitionPage(dto);
    }

    @GetMapping ("/get-xml")
    @ApiOperation("获得流程定义的xml")
    // 流程定义id
    public String getXML(@RequestParam("id") String id) {
        String bpmnXML = bpmDefinitionService.getXML(id);
        return bpmnXML;
    }
}
