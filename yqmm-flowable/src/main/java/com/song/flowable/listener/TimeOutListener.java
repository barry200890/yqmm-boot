/*******************************************************************************
 * Package: com.song.flowable.listener
 * Type:    TimeOutListener
 * Date:    2023-11-20 21:30
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.listener;

import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

/**
 * 功能描述：  任务监听器
 *
 * @author Songxianyang
 * @date 2023-11-20 21:30
 */

public class TimeOutListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {

    }
}
