/*******************************************************************************
 * Package: com.song.flowable.listener
 * Type:    ProcessDueTimeListener
 * Date:    2023-11-20 15:37
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.listener;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.flowable.task.api.Task;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 功能描述：ExecutionListener 执行监听器
 *
 * @author Songxianyang
 * @date 2023-11-20 15:37
 */
@Component
@Slf4j
public class ProcessDueTimeListener implements ExecutionListener, ApplicationContextAware {

    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ProcessDueTimeListener.applicationContext = applicationContext;
    }

    private TaskService taskService;

    private TaskService getBean(){
        if(taskService==null){
            taskService=applicationContext.getBean(TaskService.class);
        }
        return taskService;
    }




    @Override
    public void notify(DelegateExecution execution) {
        //System.out.println("--------> is-run <--------");
        //Task task = taskService.createTaskQuery().processInstanceId(execution.getProcessInstanceId()).singleResult();
        //log.info("获取当前任务id--------》{}", task.getId());
        ////taskService.claim(task.getId(),"admin");
        //taskService.complete(task.getId());

        log.info("任务超时自动执行=====start");
        String taskId = execution.getCurrentActivityId();
        Task task = getBean().createTaskQuery().processInstanceId(execution.getProcessInstanceId()).singleResult();
        if (task != null && task.getDueDate() != null && task.getDueDate().compareTo(new Date()) < 0) {
            taskService.complete(taskId);
        }
        log.info("任务超时自动执行=====end");
        //UpdateWrapper<ActRuTimerJobDO> updateWrapper = new UpdateWrapper<>();
        //LambdaUpdateWrapper<ActRuTimerJobDO> lambda = updateWrapper.lambda();
        //lambda.eq(ActRuTimerJobDO::getProcessInstanceId_,execution.getProcessInstanceId());
        //
        //ActRuTimerJobDO actRuTimerJobDO = new ActRuTimerJobDO();
        //
        //LocalDateTime now = LocalDateTime.now();
        //LocalDateTime plusDate = now.plusMinutes(3);
        //
        //Instant instant = now.atZone(ZoneId.systemDefault()).toInstant();
        //Date date = Date.from(instant);
        //
        //Instant instant1 = plusDate.atZone(ZoneId.systemDefault()).toInstant();
        //Date date1 = Date.from(instant1);
        //
        //actRuTimerJobDO.setCreateTime_(date);
        //actRuTimerJobDO.setDuedate(date1);
        //getActRuTimerJobMapper().update(actRuTimerJobDO,lambda);
    }

}
