package com.song.flowable;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.song.flowable.mapper")
public class YqmmFlowableApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(YqmmFlowableApplication.class, args);
    }
    
}
