/*******************************************************************************
 * Package: com.song.flowable.util
 * Type:    MyIdGenerator
 * Date:    2023-11-26 22:02
 *
 * Copyright (c) 2023 LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.util;

import org.flowable.common.engine.impl.cfg.IdGenerator;

import java.util.UUID;

/**
 * 功能描述：id获取器
 *Flowable自定义Id生成器：https://blog.csdn.net/vbirdbest/article/details/134416336
 * @author Songxianyang
 * @date 2023-11-26 22:02
 */
public class MyIdGenerator implements IdGenerator {
    @Override
    public String getNextId() {

        return UUID.randomUUID().toString().replace("-","");
    }
}
