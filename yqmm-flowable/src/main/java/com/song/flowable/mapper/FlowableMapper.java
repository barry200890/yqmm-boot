/*******************************************************************************
 * Package: com.song.flowable.mapper
 * Type:    Flowable
 * Date:    2022-02-02 17:27
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.mapper;

import com.song.flowable.entity.DoneEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-02-02 17:27
 */
@Mapper
public interface FlowableMapper {
    /**
     * 根据已办列表查待办列表
     * @param userId
     * @return
     */
    List<DoneEntity> doneByUserId(@Param("userId") String userId);
    
    
}
