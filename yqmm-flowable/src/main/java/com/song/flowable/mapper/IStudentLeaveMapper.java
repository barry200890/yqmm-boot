package com.song.flowable.mapper;

import java.util.List;

import com.song.flowable.entity.StudentLeaveDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author SongXianYang
 */
@Mapper
public interface IStudentLeaveMapper {

	/**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
	List<StudentLeaveDO> listAll();


	/**
     * 根据主键查询
     *
     * @param id 主键
     * @return 返回记录，没有返回null
     */
	StudentLeaveDO getById(Integer id);
	
	/**
     * 新增，插入所有字段
     *
     * @param studentLeave 新增的记录
     * @return 返回影响行数
     */
	int insert(StudentLeaveDO studentLeave);
	
	/**
     * 新增，忽略null字段
     *
     * @param studentLeave 新增的记录
     * @return 返回影响行数
     */
	int insertIgnoreNull(StudentLeaveDO studentLeave);
	
	/**
     * 修改，修改所有字段
     *
     * @param studentLeave 修改的记录
     * @return 返回影响行数
     */
	int update(StudentLeaveDO studentLeave);
	
	/**
     * 修改，忽略null字段
     *
     * @param studentLeave 修改的记录
     * @return 返回影响行数
     */
	int updateIgnoreNull(StudentLeaveDO studentLeave);
	
	/**
     * 删除记录
     *
     * @param studentLeave 待删除的记录
     * @return 返回影响行数
     */
	int delete(StudentLeaveDO studentLeave);
	
}