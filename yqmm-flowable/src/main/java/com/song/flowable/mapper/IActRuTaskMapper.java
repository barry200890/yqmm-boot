/*******************************************************************************
 * Package: com.song.flowable.mapper
 * Type:    IActRuTaskMapper
 * Date:    2022-09-18 11:44
 *
 * 
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.song.flowable.entity.ActRuTask;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2022-09-18 11:44
 */

public interface IActRuTaskMapper extends BaseMapper<ActRuTask> {

}
