# 流程退回
### 需求：从节点4直接退回到节点1（节点3和节点2可以任意退回）
# 代码
## web

```java
    @ApiOperation("doc/退回")
// 例如：在节点4直接退到节点1
@PostMapping("flow-return")
    String flowReturn(@RequestBody FlowReturnDTO dto){
        return flowableService.flowReturn(dto);
        }
```
## dto

```java
@ApiModel("流程退回")
@Data
public class FlowReturnDTO {

    @ApiModelProperty("当前节点id")
    private String currentUserTaskId;


    @ApiModelProperty("目标节点id")
    private String targetUserTaskId;


    @ApiModelProperty("流程实例id")
    private String processInstanceId;
}
```
## 实现
```java
    String flowReturn(FlowReturnDTO dto);


@Override
public String flowReturn(FlowReturnDTO dto) {

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
// 当前的任务定义key，即ACT_RU_TASK表的TASK_DEF_KEY_字段
        List<String> currentActivityIds = new ArrayList<>();
        currentActivityIds.add(dto.getCurrentUserTaskId());
        // 需要回退的目标节点的任务定义key，即ACT_RU_TASK表的TASK_DEF_KEY_字段
        String newActivityId = dto.getTargetUserTaskId();
        // 回退操作
        runtimeService.createChangeActivityStateBuilder()
        // 流程实例id
        .processInstanceId(dto.getProcessInstanceId())
        .moveActivityIdsToSingleActivityId(currentActivityIds, newActivityId)
        .changeState();
        return "流程退回成功！";
        }

```
# 所需流程图
```xml
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.flowable.org/processdef">
  <process id="yqmm-qj-key-th" name="又是请假-退回" isExecutable="true">
    <documentation>又是请假</documentation>
    <startEvent id="startEvent1" flowable:formFieldValidation="true"></startEvent>
    <userTask id="id1" name="员工" flowable:assignee="${startUserId}" flowable:formFieldValidation="true">
      <extensionElements>
        <modeler:initiator-can-complete xmlns:modeler="http://flowable.org/modeler"><![CDATA[false]]></modeler:initiator-can-complete>
      </extensionElements>
    </userTask>
    <sequenceFlow id="sid-F922625A-5942-4D2F-AD43-8C70819679D6" sourceRef="startEvent1" targetRef="id1"></sequenceFlow>
    <userTask id="id2" name="组长" flowable:assignee="${startUserId1}" flowable:formFieldValidation="true">
      <extensionElements>
        <modeler:initiator-can-complete xmlns:modeler="http://flowable.org/modeler"><![CDATA[false]]></modeler:initiator-can-complete>
      </extensionElements>
    </userTask>
    <sequenceFlow id="sid-4EE5F1BD-689A-4FCB-A87E-0436FF297C32" sourceRef="id1" targetRef="id2"></sequenceFlow>
    <userTask id="id3" name="人事" flowable:assignee="${startUserId2}" flowable:formFieldValidation="true">
      <extensionElements>
        <modeler:initiator-can-complete xmlns:modeler="http://flowable.org/modeler"><![CDATA[false]]></modeler:initiator-can-complete>
      </extensionElements>
    </userTask>
    <sequenceFlow id="sid-CF92D6A0-CB2E-43EF-8269-5C6BA03B4F1E" sourceRef="id2" targetRef="id3"></sequenceFlow>
    <userTask id="id4" name="项目经理" flowable:assignee="${startUserId3}" flowable:formFieldValidation="true">
      <extensionElements>
        <modeler:initiator-can-complete xmlns:modeler="http://flowable.org/modeler"><![CDATA[false]]></modeler:initiator-can-complete>
      </extensionElements>
    </userTask>
    <sequenceFlow id="sid-02FDC71C-5567-4C8A-A543-E5E9DD3E443A" sourceRef="id3" targetRef="id4"></sequenceFlow>
    <userTask id="id5" name="CEO" flowable:assignee="${startUserId4}" flowable:formFieldValidation="true">
      <extensionElements>
        <modeler:initiator-can-complete xmlns:modeler="http://flowable.org/modeler"><![CDATA[false]]></modeler:initiator-can-complete>
      </extensionElements>
    </userTask>
    <sequenceFlow id="sid-9F09870D-02CA-4D14-8AC7-E779F081EC7B" sourceRef="id4" targetRef="id5"></sequenceFlow>
    <endEvent id="sid-6F19D151-9234-4ACD-BF6A-AE2FB9CAE7E7"></endEvent>
    <sequenceFlow id="sid-A52F2B42-83DE-491A-AFE3-40F9130A884C" sourceRef="id5" targetRef="sid-6F19D151-9234-4ACD-BF6A-AE2FB9CAE7E7"></sequenceFlow>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_yqmm-qj-key-th">
    <bpmndi:BPMNPlane bpmnElement="yqmm-qj-key-th" id="BPMNPlane_yqmm-qj-key-th">
      <bpmndi:BPMNShape bpmnElement="startEvent1" id="BPMNShape_startEvent1">
        <omgdc:Bounds height="30.0" width="30.0" x="100.0" y="163.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="id1" id="BPMNShape_id1">
        <omgdc:Bounds height="80.0" width="99.99999999999997" x="174.99999739229682" y="137.99999794363978"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="id2" id="BPMNShape_id2">
        <omgdc:Bounds height="80.0" width="100.0" x="319.999990463257" y="137.99999794363978"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="id3" id="BPMNShape_id3">
        <omgdc:Bounds height="80.0" width="100.0" x="464.99999307096016" y="137.99999794363978"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="id4" id="BPMNShape_id4">
        <omgdc:Bounds height="80.0" width="100.0" x="609.9999909102918" y="137.99999794363978"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="id5" id="BPMNShape_id5">
        <omgdc:Bounds height="80.0" width="100.0" x="754.9999887496234" y="137.99999794363978"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-6F19D151-9234-4ACD-BF6A-AE2FB9CAE7E7" id="BPMNShape_sid-6F19D151-9234-4ACD-BF6A-AE2FB9CAE7E7">
        <omgdc:Bounds height="28.0" width="28.0" x="900.0" y="164.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="sid-4EE5F1BD-689A-4FCB-A87E-0436FF297C32" id="BPMNEdge_sid-4EE5F1BD-689A-4FCB-A87E-0436FF297C32">
        <omgdi:waypoint x="274.94999739229684" y="177.99999794363978"></omgdi:waypoint>
        <omgdi:waypoint x="319.9999904632017" y="177.99999794363978"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-9F09870D-02CA-4D14-8AC7-E779F081EC7B" id="BPMNEdge_sid-9F09870D-02CA-4D14-8AC7-E779F081EC7B">
        <omgdi:waypoint x="709.9499909102673" y="177.99999794363978"></omgdi:waypoint>
        <omgdi:waypoint x="754.9999887496234" y="177.99999794363978"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-02FDC71C-5567-4C8A-A543-E5E9DD3E443A" id="BPMNEdge_sid-02FDC71C-5567-4C8A-A543-E5E9DD3E443A">
        <omgdi:waypoint x="564.9499930709356" y="177.99999794363978"></omgdi:waypoint>
        <omgdi:waypoint x="609.9999909102918" y="177.99999794363978"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-A52F2B42-83DE-491A-AFE3-40F9130A884C" id="BPMNEdge_sid-A52F2B42-83DE-491A-AFE3-40F9130A884C">
        <omgdi:waypoint x="854.9499887495535" y="177.99999888598092"></omgdi:waypoint>
        <omgdi:waypoint x="900.0" y="177.99999973588038"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-F922625A-5942-4D2F-AD43-8C70819679D6" id="BPMNEdge_sid-F922625A-5942-4D2F-AD43-8C70819679D6">
        <omgdi:waypoint x="129.94999849008173" y="177.99999971958724"></omgdi:waypoint>
        <omgdi:waypoint x="174.99999739229682" y="177.9999988774143"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-CF92D6A0-CB2E-43EF-8269-5C6BA03B4F1E" id="BPMNEdge_sid-CF92D6A0-CB2E-43EF-8269-5C6BA03B4F1E">
        <omgdi:waypoint x="419.949990463257" y="177.99999794363978"></omgdi:waypoint>
        <omgdi:waypoint x="464.99999307093583" y="177.99999794363978"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
```
# 流程部署
```java
 @Test
    void createDeployment() {

        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
                .addClasspathResource("bpmn/请假-退回.bpmn20.xml").name("请假退回");
        Deployment deploy = deploymentBuilder.deploy();
        System.out.println("流程部署成功");
        System.out.println(deploy.getId());
        System.out.println(deploy.getName());
        System.out.println(deploy.getKey());
        System.out.println(deploy.getParentDeploymentId());
    }
```
## 启动流程or发起流程
```json
/flow/start-flowable
{
	"map": {
"startUserId":"songxy"
},
	"processDefinitionKey": "yqmm-qj-key-th"
}
```
# 查询代办
```json
{
	"page": 0,
	"pageSize": 1000,
	"userId": "admin"
}
/flow/todo-list
```
# 通过或者拒绝
```json
{
    "map":{
        "startUserId3":"admin"
    },
    "taskId":"75b56a5a-86b5-11ee-9e6b-dc41a90b0909",
    "userId":"admin"
}
/flow/accept-or-reject
```

# 退回
```json

{
	"currentUserTaskId": "id4",
	"processInstanceId": "0ebd7525-86af-11ee-98a8-dc41a90b0909",
	"targetUserTaskId": "id1"
}
```

# 截图
![img.png](img.png)
![img_1.png](img_1.png)
