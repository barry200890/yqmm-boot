/*******************************************************************************
 * Package: com.song.rabbit.mq.web
 * Type:    MqController
 * Date:    2023-05-14 18:52
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.web;

import com.song.rabbit.mq.entity.Cat;
import com.song.rabbit.mq.entity.Dog;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

/**
 * 功能描述：
 *
 * @author Songxianyang
 * @date 2023-05-14 18:52
 */
@RestController
@RequestMapping("mq-web")
public class MqController {
    @Resource
    private RabbitTemplate rabbitTemplate;
    @GetMapping("send")
    public String send() {
        Cat cat = new Cat(1,"大猫",new Date());
        Cat cat2 = new Cat(2,"大猫2",new Date());
        // 发送消息
        // String exchange, String routingKey, Object message, CorrelationData 确定这一条消息
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange","song.java",cat,new CorrelationData(UUID.randomUUID().toString()));
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange","song.java",cat2,new CorrelationData(UUID.randomUUID().toString()));
        Dog dog = new Dog(1,"大狗",new Date());
        rabbitTemplate.convertAndSend("yqmm.rabbit.mq.java.exchange","song.java",dog,new CorrelationData(UUID.randomUUID().toString()));
        return "成功";
    }
}
