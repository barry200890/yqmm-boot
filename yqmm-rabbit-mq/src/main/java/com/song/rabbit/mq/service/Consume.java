/*******************************************************************************
 * Package: com.song.rabbit.mq.service
 * Type:    Consume
 * Date:    2023-05-14 19:04
 *
 * Copyright (c) 2023 HUANENG GUICHENG TRUST CORP.,LTD All Rights Reserved.
 *
 * You may not use this file except in compliance with the License.
 *******************************************************************************/
package com.song.rabbit.mq.service;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.song.rabbit.mq.entity.Cat;
import com.song.rabbit.mq.entity.Dog;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 功能描述：
 * 默认为自动确认：后果服务宕机后所有消息自动确认签收
 * 解决方法：手动确认可以保证消息不丢失
 * @author Songxianyang
 * @date 2023-05-14 19:04
 */

@Service
@RabbitListener(queues = {"yqmm.rabbit.mq.java.queue"})
public class Consume {


    /**
     * 消费者只消费：Cat类型的数据。
     * @param cat
     * @param channel
     * @param message 获取 ：The server reported 0 messages remaining
     */
    @RabbitHandler
    public void testConsume(Cat cat, Channel channel, Message message) throws IOException {
        // 第一个 ：来接受具体的消息类型 ：在消息头上
        System.out.println("我的大猫===》"+cat);
        System.out.println(message.getMessageProperties());
        // 获取序列标签
        MessageProperties messageProperties = message.getMessageProperties();

        // 手动消息确认
        // 标签号，批量处理
        channel.basicAck(messageProperties.getDeliveryTag(),false);
    }
    @RabbitHandler
    public void testConsume(Dog dog, Channel channel, Message message) {
        //这个没有消息确认
        System.out.println("我的大狗===》"+dog);
    }
}
